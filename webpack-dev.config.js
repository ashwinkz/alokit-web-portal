const path = require('path');
const Dotenv = require('dotenv-webpack');
const { merge } = require('webpack-merge');
const commonConfig = require('./webpack-base.config.js');

const devConfig = {
  mode: 'development',
  module: {
    rules: [
      {
        test: /\.(pdf|jpg|jpeg|png|gif|svg|ico|mp3)$/,
        use: [{ loader: 'url-loader' }],
      },
    ],
  },
  plugins: [
    new Dotenv({
      path: path.resolve(__dirname, '.env.development'),
    }),
  ],
  devtool: 'inline-source-map',
  devServer: {
    contentBase: path.resolve(__dirname, 'build'),
    compress: true,
    historyApiFallback: true,
  },
};

module.exports = commonConfig.map((conf) => merge(conf, devConfig));
