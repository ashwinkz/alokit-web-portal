/* eslint-disable no-extend-native */
/* eslint-disable prefer-reflect */
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { merge } = require('webpack-merge');
const ESLintWebpackPlugin = require('eslint-webpack-plugin');

const commonConfig = {
  entry: path.join(__dirname, './src', 'index.mjs'),
  output: {
    path: path.join(__dirname, './build'),
    publicPath: '/',
  },
  resolve: {
    modules: [
      path.resolve(__dirname, './src'),
      path.resolve(__dirname, './node_modules'),
      path.resolve(__dirname, './images'),
    ],
    extensions: ['.mjs', '.js', '.jsx'],
    alias: {
      Images: path.resolve(__dirname, './images'),
      Containers: path.resolve(__dirname, './src/containers'),
      Components: path.resolve(__dirname, './src/components'),
      Actions: path.resolve(__dirname, './src/actions'),
      Reducers: path.resolve(__dirname, './src/reducers'),
      Utils: path.resolve(__dirname, './src/utils'),
      GraphQL: path.resolve(__dirname, './src/graphql'),
    },
  },
  module: {
    rules: [
      {
        test: /\.(css|scss)$/,
        use: [
          'style-loader', // creates style nodes from JS strings
          'css-loader', // translates CSS into CommonJS
          'sass-loader', // compiles Sass to CSS, using Node Sass by default
        ],
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack', 'file-loader'],
      },
      {
        test: /\.m?js/,
        resolve: {
          fullySpecified: false,
        },
      },
    ],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: path.resolve('./index.html'),
      inject: false,
      title: 'Alokit Education Leadership Private Ltd',
    }),
    new ESLintWebpackPlugin(),
  ],
};

const es5Config = merge(commonConfig, {
  name: 'es5Build',
  output: {
    filename: 'main.es5.js',
  },
  module: {
    rules: [
      {
        // this is so that we can compile any React,
        // ES6 and above into normal ES5 syntax
        test: /\.(m?js|jsx)$/,
        resolve: {
          fullySpecified: false,
        },
        // we do not want anything from node_modules to be compiled
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  '@babel/preset-env',
                  {
                    modules: false, // do not transform ES2015 module syntax into CommonJS modules - requirement to take advantage of tree-shaking https://webpack.js.org/guides/tree-shaking/#conclusion
                    useBuiltIns: false,
                    targets: {
                      browsers: [
                        '>1%', // browsers with more than 1% market share
                        'last 2 versions',
                        'Firefox ESR',
                      ],
                    },
                  },
                ],
              ],
            },
          },
        ],
      },
    ],
  },
});

const es6Config = merge(commonConfig, {
  name: 'es6Build',
  output: {
    filename: 'main.mjs',
  },
  module: {
    rules: [
      {
        // this is so that we can compile any React,
        // ES6 and above into normal ES5 syntax
        test: /\.(m?js|jsx)$/,
        resolve: {
          fullySpecified: false,
        },
        // we do not want anything from node_modules to be compiled
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  '@babel/preset-env',
                  {
                    modules: false, // do not transform ES2015 module syntax into CommonJS modules - requirement to take advantage of tree-shaking https://webpack.js.org/guides/tree-shaking/#conclusion
                    useBuiltIns: false,
                    targets: {
                      browsers: [
                        'Chrome >= 60',
                        'Safari >= 10.1',
                        'iOS > 10.3',
                        'Firefox >= 54',
                        'Edge >= 15',
                      ],
                    },
                  },
                ],
              ],
            },
          },
        ],
      },
    ],
  },
});

module.exports = [es5Config, es6Config];
