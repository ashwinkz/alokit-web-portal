# india-ffg-2020-alokit-education-leadership-private-ltd

Web application for Alokit Education Leadership Private Ltd. The application frontend is developed in [React] and the backend is a GraphQL API with AWS DynamoDB deployed using AWS AppSync.

### Getting started

Check the technical guide for the project.

# What's Included?

## Dependencies for React

**React**, **ReactDOM** and **PropTypes**

## Material Design

The project is configured to use [MATERIAL - UI] framework (v4.11.x). Use [MATERIAL - UI] React Components for UI development.

## State Management and Routing

The project is configured to use [React Redux] for state management in the UI. Also included is the [Redux Thunk] middleware (to better handle asynchronous network calls within actions) and [React Router] (for handling routing).

## Code Formatter

[Prettier] is an opinionated code formatter. It removes all original styling and ensures that all outputted code conforms to a consistent style.

Prettier is already configured for the project. You can identify and fix formatting of code files as follows:

- From the command line `$ npm run format`
- Use [Prettier] with an IDE or editor, check out [Prettier - Editor Integration]
- The repo is configured with _husky_ and _lint-staged_ to automatically identify and fix formatting of code files staged for a commit

* Files with the extension _.js, .mjs, .jsx, .json, .html, .md, .css, .less, .scss_ are scanned and fixed
* The same configuration holds whether running [Prettier] from the command line or IDE or git commit hooks

## Loaders & Compilers

The project is configured to use [Babel] for compiling code and transpile ES2015+ code to ES5. The configuration supports loading React code for compilation.

## Linting

The project is configured to use [ESLint].

## Build Configuration

The project is configured to use [Webpack] for development and production UI builds.

## Code Splitting and Lazy Loading

Code splitting can be implemented using **_webpack, @babel/plugin-syntax-dynamic-import, and loadable-components_**. **_loadable-components_** is a library for loading components with dynamic imports. It handles all sorts of edge cases automatically and makes code splitting simple!

The required package and plugin configuration is already available in the project. Check the [loadable components guide] on how to use dynamic imports to implement code splitting for React components.

## cross-fetch

Universal WHATWG Fetch API for Node, Browsers and React Native. The API supports [async/await].

#### es6-promise

ES6 Promise compatible polyfill for environments that don't support Promises.

## References

[How to set up & deploy your React app from scratch using Webpack and Babel]

[webpack Tutorial: How to Set Up webpack 5 From Scratch]

[Deploying ES2015+ Code in Production Today]

[React: Code Splitting and Lazy Loading]

[//]: # 'Reference Links'
[prettier]: https://prettier.io/docs/en/index.html
[prettier - editor integration]: https://prettier.io/docs/en/editors.html
[material - ui]: https://material-ui.com/
[webpack]: https://webpack.js.org/
[babel]: https://babeljs.io/
[react]: https://reactjs.org/
[react redux]: https://github.com/reduxjs/react-redux
[async/await]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function
[react router]: https://github.com/ReactTraining/react-router
[redux thunk]: https://github.com/gaearon/redux-thunk
[eslint]: https://eslint.org/
[husky]: https://github.com/typicode/husky
[lint-staged]: https://github.com/okonet/lint-staged

[Deploying ES2015+ Code in Production Today]: <[https://philipwalton.com/articles/deploying-es2015-code-in-production-today/]>
[How to set up & deploy your React app from scratch using Webpack and Babel]: <https://www.freecodecamp.org/news/how-to-set-up-deploy-your-react-app-from-scratch-using-webpack-and-babel-a669891033d4/>
[Loadable Components]: <https://www.smooth-code.com/open-source/loadable-components/>
[React: Code Splitting and Lazy Loading]: <https://reacttraining.com/react-router/web/guides/code-splitting>
[loadable components guide]: <https://www.smooth-code.com/open-source/loadable-components/docs/getting-started/>
[webpack Tutorial: How to Set Up webpack 5 From Scratch]: <https://www.taniarascia.com/how-to-use-webpack/>
