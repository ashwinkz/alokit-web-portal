export const cognitoConfig = {
  Auth: {
    region: process.env.AWS_REGION,
    userPoolId: process.env.AUTH_USER_POOL,
    userPoolWebClientId: process.env.AUTH_WEBAPP_CLIENT_ID,
    authenticationFlowType: 'USER_PASSWORD_AUTH',
  },
  aws_appsync_graphqlEndpoint: process.env.APPSYNC_GRAPHQL_API,
  aws_appsync_region: process.env.AWS_REGION,
  aws_appsync_authenticationType: 'AMAZON_COGNITO_USER_POOLS',
};

export default cognitoConfig;
