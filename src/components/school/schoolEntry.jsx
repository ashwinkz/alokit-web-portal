import React from 'react';
import {
  Button,
  CircularProgress,
  Divider,
  InputAdornment,
  MenuItem,
  Toolbar,
  Typography,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {
  ValidatorForm,
  TextValidator,
  SelectValidator,
} from 'react-material-ui-form-validator';
import PropTypes from 'prop-types';

import {
  MOBILE_NUMBER_PREFIX,
  validatePhoneNumber,
  validateName,
} from 'Utils/appShell';
import { CREDENTIALS_DELIVERY_OPTIONS } from 'Utils/constants';

const useStyles = makeStyles((theme) => ({
  paper: {
    height: '50%',
    //overflowY: 'scroll',
    position: 'relative',
    width: 800,
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2, 4, 3),
    justify: 'center',
  },
}));

export default function SchoolEntry(props) {
  const classes = useStyles();
  const [editdata, setEditdata] = React.useState({
    id: props.data.id,
    schoolName: props.data.schoolName,
    branch: props.data.branch,
    adminFirstName: props.data.adminFirstName,
    adminLastName: props.data.adminLastName,
    adminGender: props.data.adminGender,
    adminEmail: props.data.adminEmail,
    address: props.data.address,
    adminMobile: props.data.adminMobile,
  });
  let formLabel = 'Add School';
  if (props.viewOnlyMode) formLabel = 'View School';
  if (props.editMode) formLabel = 'Edit School';
  const [submitted, setSubmitted] = React.useState(false);
  const [selectedDeliveryMedium, setSelectedDeliveryMedium] = React.useState(
    CREDENTIALS_DELIVERY_OPTIONS.EMAIL
  );
  const handleValueChange = (name) => (e) => {
    const value = e.target.value;
    setEditdata({ ...editdata, [name]: value });
  };

  const handleDeliveryMediumChange = (e) => {
    setSelectedDeliveryMedium(e.target.value);
  };

  const onSubmitHandle = () => {
    setSubmitted(true);
    props.onSubmit(editdata, selectedDeliveryMedium);
  };

  ValidatorForm.addValidationRule('isPhoneNumber', (value) =>
    validatePhoneNumber(value)
  );
  ValidatorForm.addValidationRule('isValidName', (value) =>
    validateName(value)
  );

  const onCancel = () => {
    props.onCancel(true);
  };

  return (
    <div className={classes.paper}>
      <ValidatorForm onSubmit={onSubmitHandle}>
        <Typography variant="h5" align="center">
          {formLabel}
        </Typography>
        <Divider className="contentMarginTop" />
        <div className="popupFormContent">
          <TextValidator
            label="School Name"
            onChange={handleValueChange('schoolName')}
            name="schoolName"
            value={editdata.schoolName}
            validators={['required']}
            errorMessages={['this field is required']}
            variant="outlined"
            fullWidth
            disabled={props.viewOnlyMode}
          />
          <br />
          <TextValidator
            label="Branch"
            onChange={handleValueChange('branch')}
            name="branch"
            value={editdata.branch}
            validators={['required']}
            errorMessages={['this field is required']}
            variant="outlined"
            fullWidth
            disabled={props.viewOnlyMode}
          />
          <br />
          <TextValidator
            label="Address"
            onChange={handleValueChange('address')}
            name="address"
            value={editdata.address}
            validators={['required']}
            errorMessages={['this field is required']}
            variant="outlined"
            fullWidth
            disabled={props.viewOnlyMode}
          />
          <br />
          <TextValidator
            label="Admin - First Name"
            onChange={handleValueChange('adminFirstName')}
            disabled={props.viewOnlyMode || props.editMode}
            name="first_name"
            value={editdata.adminFirstName}
            validators={['required', 'isValidName']}
            errorMessages={[
              'this field is required',
              'This is not a valid name',
            ]}
            variant="outlined"
            fullWidth
          />
          <br />
          <TextValidator
            label="Admin - Last Name"
            onChange={handleValueChange('adminLastName')}
            disabled={props.viewOnlyMode || props.editMode}
            name="last_name"
            value={editdata.adminLastName}
            validators={['required', 'isValidName']}
            errorMessages={[
              'this field is required',
              'This is not a valid name',
            ]}
            variant="outlined"
            fullWidth
          />
          <br />
          <TextValidator
            label="Admin - Mobile Number"
            name="mobile_number"
            disabled={props.viewOnlyMode || props.editMode}
            value={editdata.adminMobile}
            onChange={handleValueChange('adminMobile')}
            validators={['required', 'isPhoneNumber']}
            variant="outlined"
            fullWidth
            errorMessages={[
              'This field is required',
              'Mobile number is not valid',
            ]}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  {MOBILE_NUMBER_PREFIX}
                </InputAdornment>
              ),
            }}
          />
          <br />
          <TextValidator
            label="Admin - Email ID"
            onChange={handleValueChange('adminEmail')}
            disabled={props.viewOnlyMode || props.editMode}
            name="email"
            value={editdata.adminEmail}
            validators={['required', 'isEmail']}
            errorMessages={['this field is required', 'email is not valid']}
            variant="outlined"
            fullWidth
          />
          <br />
          {!props.editMode ? (
            <>
              <SelectValidator
                label="Send login credentials through"
                onChange={handleDeliveryMediumChange}
                name="Delivery Medium"
                value={selectedDeliveryMedium}
                validators={['required']}
                errorMessages={['this field is required']}
                variant="outlined"
                fullWidth
              >
                {Object.keys(CREDENTIALS_DELIVERY_OPTIONS).map((medium) => (
                  <MenuItem
                    key={medium}
                    value={CREDENTIALS_DELIVERY_OPTIONS[medium]}
                  >
                    {CREDENTIALS_DELIVERY_OPTIONS[medium]}
                  </MenuItem>
                ))}
              </SelectValidator>
              <br />
            </>
          ) : null}
          <SelectValidator
            label="Admin - Gender"
            onChange={handleValueChange('adminGender')}
            disabled={props.viewOnlyMode || props.editMode}
            name="gender"
            value={editdata.adminGender}
            validators={['required']}
            errorMessages={['this field is required']}
            variant="outlined"
            fullWidth
          >
            {props.genderOptions.map((gender, index) => (
              <MenuItem key={index} value={gender}>
                {gender}
              </MenuItem>
            ))}
          </SelectValidator>
          <br />
        </div>
        <Divider className="contentMarginTop" />
        <Toolbar disableGutters>
          <dir className="flexGrow1" />
          <Button
            color="primary"
            variant="outlined"
            type="submit"
            onClick={onCancel}
            className="toolbarAction"
          >
            {props.viewOnlyMode ? 'Close' : 'Cancel'}
          </Button>
          {!props.viewOnlyMode && (
            <Button
              color="primary"
              variant="contained"
              type="submit"
              className="toolbarAction"
              disabled={submitted}
              startIcon={submitted && <CircularProgress size={24} />}
            >
              {(submitted && 'Saving') || (!submitted && 'Submit')}
            </Button>
          )}
        </Toolbar>
      </ValidatorForm>
    </div>
  );
}

SchoolEntry.propTypes = {
  data: PropTypes.shape({
    id: PropTypes.string.isRequired,
    schoolName: PropTypes.string.isRequired,
    branch: PropTypes.string.isRequired,
    address: PropTypes.string.isRequired,
    adminFirstName: PropTypes.string.isRequired,
    adminLastName: PropTypes.string.isRequired,
    adminEmail: PropTypes.string.isRequired,
    adminMobile: PropTypes.string.isRequired,
    adminGender: PropTypes.string.isRequired,
  }),
  editMode: PropTypes.bool.isRequired,
  viewOnlyMode: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  genderOptions: PropTypes.array.isRequired,
};
