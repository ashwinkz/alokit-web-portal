import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import {
  Button,
  Modal,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  Toolbar,
  Tooltip,
  Typography,
} from '@material-ui/core';
import { Add as AddIcon } from '@material-ui/icons';
import { connect } from 'react-redux';

import SearchBar from 'Components/search/index';
import SchoolEntry from './schoolEntry';
import { MOBILE_NUMBER_PREFIX } from 'Utils/appShell';
import { DEFAULT_PAGE_SIZE, AUTH_ROLES } from 'Utils/constants';
import { fetchSchools, addSchool, editSchool } from 'Actions/school';
import { NOTIFICATION_SEVERITY } from 'Components/notify/index';
import {
  showProgressBar,
  hideProgressBar,
  showNotification,
} from 'Actions/appShell';

import DataNotFound from 'Components/utils/DataNotFound';

const useStyles = makeStyles((theme) => ({
  container: {
    maxHeight: 440,
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  close: {
    backgroundColor: theme.palette.background.paper,
  },
}));

function SchoolListing(props) {
  const classes = useStyles();

  const [schools, setSchoolData] = React.useState([]);
  const [filteredSchools, setFilteredSchools] = React.useState([]);
  const [fetchSchoolListing, setFetchSchoolListing] = React.useState(false);

  const mapSchoolRecord = (s) => ({
    id: s.id,
    schoolName: s.name,
    branch: s.branch,
    address: s.address,
    adminFirstName: s.admin.firstName,
    adminLastName: s.admin.lastName,
    adminGender: s.admin.gender,
    adminEmail: s.admin.email,
    adminMobile: s.admin.phoneNumber,
  });
  const [editFlag, setEditFlag] = React.useState(false);
  const [viewOnlyFlag, setViewOnlyFlag] = React.useState(true);
  React.useEffect(() => {
    if (props.currentUserProfileLoaded) {
      setFetchSchoolListing(true);
      if (props.currentUserRole === AUTH_ROLES.ALOKIT_ADMIN)
        setViewOnlyFlag(false);
    }
  }, [props.currentUserProfileLoaded]);
  React.useEffect(async () => {
    if (fetchSchoolListing) {
      console.log('fetching schools from backend');
      props.showProgressBar();
      const schools = await fetchSchools();
      setFetchSchoolListing(false);
      const formattedSchoolsData = schools.map((s) => mapSchoolRecord(s));
      setSchoolData(formattedSchoolsData);
      setFilteredSchools(formattedSchoolsData);
      props.hideProgressBar();
    }
  }, [fetchSchoolListing]);

  const schoolListHeader = [
    { id: 'School Name', label: 'School Name' },
    { id: 'Branch', label: 'Branch' },
    { id: 'Admin Email', label: 'Admin Email' },
    { id: 'Actions', label: 'Actions' },
  ];
  const [page, setPage] = React.useState(0);
  const [searchValue, setSearchValue] = React.useState('');
  const [rowsPerPage, setRowsPerPage] = React.useState(DEFAULT_PAGE_SIZE);
  const [formOpen, setFormOpen] = React.useState(false);
  const [schoolFormData, setSchoolFormData] = React.useState({
    id: '',
    schoolName: '',
    address: '',
    branch: '',
    adminFirstName: '',
    adminLastName: '',
    adminGender: '',
    adminEmail: '',
    adminMobile: '',
  });
  // const [deleteMessage, setDeleteMessage] = React.useState('');
  // const [deleteFlag, setDeleteFlag] = React.useState(false);
  // const [deleteId, setDeleteId] = React.useState(0);
  const onSchoolOpen = (row) => {
    setEditFlag(true);
    //remove MOBILE_NUMBER_PREFIX from phone number before populating form
    setSchoolFormData({
      id: row.id,
      schoolName: row.schoolName,
      branch: row.branch,
      adminFirstName: row.adminFirstName,
      adminLastName: row.adminLastName,
      adminGender: row.adminGender,
      adminEmail: row.adminEmail,
      address: row.address,
      adminMobile: row.adminMobile.replace(MOBILE_NUMBER_PREFIX, ''),
    });
    setFormOpen(true);
  };
  const onSchoolAdd = () => {
    setEditFlag(false);
    setSchoolFormData({
      id: '',
      schoolName: '',
      branch: '',
      adminFirstName: '',
      adminLastName: '',
      adminGender: '',
      adminEmail: '',
      address: '',
      adminMobile: '',
    });
    setFormOpen(true);
  };

  // function idSorter() {
  //   return function (a, b) {
  //     let id1 = parseInt(a['id'].split('-')[1]);
  //     let id2 = parseInt(b['id'].split('-')[1]);
  //     if (id1 > id2) {
  //       return -1;
  //     } else if (id1 < id2) {
  //       return 1;
  //     }
  //     return 0;
  //   };
  // }

  // const fetchLatestSchoolID = () => {
  //   if (Object.keys(schools).length > 0) {
  //     schools.sort(idSorter());
  //     return parseInt(schools[0]['id'].split('-')[1]);
  //   } else {
  //     return 0;
  //   }
  // };
  // const getRegId = () => {
  //   var len = 8 - (fetchLatestSchoolID() + 1).toString().length;
  //   var regId = 'ALOKIT-';
  //   while (len > 0) {
  //     regId = regId + '0';
  //     len--;
  //   }
  //   return regId + (fetchLatestSchoolID() + 1).toString();
  // };
  // const handleSchoolDeletion = (dataObtained) => {
  //   setFormOpen(true);
  //   setDeleteFlag(true);
  //   setDeleteId(dataObtained.id);
  //   setDeleteMessage(
  //     'Are you sure you want to delete ' + dataObtained.schoolName + ' ?'
  //   );
  // };

  const handleModalClose = () => {
    setFormOpen(false);
  };

  const handleSchoolSubmission = async (schoolDetails, deliveryMedium) => {
    let actionResponse = {};
    props.showProgressBar();
    if (!editFlag) {
      actionResponse = await addSchool(
        schoolDetails.schoolName,
        schoolDetails.branch,
        schoolDetails.address,
        schoolDetails.adminFirstName,
        schoolDetails.adminLastName,
        MOBILE_NUMBER_PREFIX.concat(schoolDetails.adminMobile), // add country code
        schoolDetails.adminEmail,
        schoolDetails.adminGender,
        deliveryMedium
      );
    } else {
      // only name/branch/address can be updated
      actionResponse = await editSchool(
        schoolDetails.id,
        schoolDetails.schoolName,
        schoolDetails.branch,
        schoolDetails.address
      );
    }

    // display action result
    props.showNotification(
      actionResponse.message,
      actionResponse.success
        ? NOTIFICATION_SEVERITY.SUCCESS
        : NOTIFICATION_SEVERITY.ERROR
    );
    props.hideProgressBar();
    handleModalClose();

    if (actionResponse.success) {
      //refresh school listing
      setFetchSchoolListing(true);
    } else {
      // re-open form with entered data
      setSchoolFormData(schoolDetails);
      setFormOpen(true);
    }
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  const filterSchoolList = () => {
    const newFilteredData = searchValue
      ? schools.filter((record) =>
          record['schoolName'].toLowerCase().includes(searchValue.toLowerCase())
        )
      : schools;
    setFilteredSchools(newFilteredData);
  };
  React.useEffect(() => {
    filterSchoolList();
  }, [searchValue]);

  return (
    <div>
      <Toolbar className={'pageToolbar'}>
        <Typography variant="h6">
          {viewOnlyFlag ? 'View' : 'Manage'} Schools
        </Typography>
        <div className={'flexGrow1'} />
        <SearchBar
          value={searchValue}
          onChange={(newSearchValue) => setSearchValue(newSearchValue)}
          onCancelSearch={() => setSearchValue('')}
          className={'pageToolbarAction'}
        />
        {!viewOnlyFlag && (
          <Button
            color="primary"
            variant="contained"
            startIcon={<AddIcon />}
            onClick={onSchoolAdd}
            className={'pageToolbarAction'}
          >
            Add School
          </Button>
        )}
      </Toolbar>
      <Paper>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="School list table">
            <TableHead>
              <TableRow key="School list header">
                {schoolListHeader.map((column) => (
                  <TableCell key={column.id}>{column.label}</TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              <Modal
                open={formOpen}
                onclose={(event, reason) => {
                  if (reason === 'escapeKeyDown') handleModalClose();
                }}
                disableEnforceFocus
                aria-labelledby="School data operations modal"
                aria-describedby="School data add/edit/delete modal"
                className={classes.modal}
              >
                <div>
                  <SchoolEntry
                    data={schoolFormData}
                    editMode={editFlag}
                    viewOnlyMode={viewOnlyFlag}
                    onCancel={handleModalClose}
                    onSubmit={handleSchoolSubmission}
                    genderOptions={props.GENDER_LIST}
                  />
                </div>
              </Modal>
              {(schools === undefined || schools.length === 0) && (
                <DataNotFound />
              )}
              {schools !== undefined &&
                filteredSchools
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((slicedRow) => {
                    return (
                      <TableRow
                        hover
                        role="checkbox"
                        tabIndex={-1}
                        key={slicedRow.id}
                      >
                        <TableCell>{slicedRow.schoolName}</TableCell>
                        <TableCell>{slicedRow.branch}</TableCell>
                        <TableCell>{slicedRow.adminEmail}</TableCell>
                        <TableCell>
                          <Tooltip
                            title={
                              (viewOnlyFlag ? 'View ' : 'Edit ') +
                              slicedRow.schoolName +
                              ' - ' +
                              slicedRow.branch
                            }
                          >
                            <Button
                              color="secondary"
                              variant="contained"
                              onClick={() => onSchoolOpen(slicedRow)}
                              disableElevation
                            >
                              {viewOnlyFlag ? 'View' : 'Edit'}
                            </Button>
                          </Tooltip>
                          {/* <IconButton
                          aria-label="delete"
                          onClick={() => handleSchoolDeletion(slicedRow)}
                        >
                          <DeleteIcon />
                        </IconButton> */}
                        </TableCell>
                      </TableRow>
                    );
                  })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 15]}
          component="div"
          count={schools !== undefined ? filteredSchools.length : 0}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
}

SchoolListing.propTypes = {
  GENDER_LIST: PropTypes.array.isRequired,
  currentUserProfileLoaded: PropTypes.bool,
  currentUserRole: PropTypes.string,
  showProgressBar: PropTypes.func.isRequired,
  hideProgressBar: PropTypes.func.isRequired,
  showNotification: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    GENDER_LIST: state.authUser.GENDER_LIST,
    currentUserProfileLoaded: state.authUser.userProfileLoaded,
    currentUserRole: state.authUser.userProfile.userRole,
  };
}

export default connect(mapStateToProps, {
  showProgressBar,
  hideProgressBar,
  showNotification,
})(SchoolListing);
