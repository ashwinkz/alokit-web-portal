import React from 'react';
import { Paper, Modal, Toolbar, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
}));

const useDataGridStyles = makeStyles(
  {
    root: {
      border: 0,
    },
    cell: {
      borderRight: '1px solid rgba(224, 224, 224, 1)',
    },

    colCell: {
      borderRight: '1px solid rgba(224, 224, 224, 1)',
      minHeight: '52px',
      maxHeight: '52px',
      borderBottom: 'none !important',
    },
    columnsContainer: {
      borderBottom: '1px solid rgba(224, 224, 224, 1)',
      lineHeight: '20px !important',
    },
    iconSeparator: {
      display: 'none !important',
    },
  },
  { name: 'MuiDataGrid' }
);

import { DataGrid } from '@material-ui/data-grid';

import SchoolDashBoard from '../dashboard/schoolDashBoard';

export default function Report(props) {
  const dataGridClasses = useDataGridStyles();
  const [open, setOpen] = React.useState(false);
  const [schoolID, setSchoolID] = React.useState(1);
  const classes = useStyles();

  const handleModalClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Paper style={{ height: 400, width: '100%' }}>
        <Modal
          open={open}
          onClose={handleModalClose}
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          className={classes.modal}
        >
          <div style={{ width: '80%' }}>
            <SchoolDashBoard schoolID={schoolID} />
          </div>
        </Modal>
        <Toolbar className={'pageToolbar'} style={{ marginBottom: '10px' }}>
          <Typography variant="h6">{props.data.label} Dashboard</Typography>
          <div className={'flexGrow1'} />
        </Toolbar>
        <DataGrid
          classes={dataGridClasses}
          columns={props.data.headerList}
          rows={props.data.dataList}
          onRowClick={(event) => {
            if (props.data.label === 'Alokit') {
              setOpen(true);
              setSchoolID(event.row.id);
            }
          }}
        />
      </Paper>
    </div>
  );
}
