import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { Prompt } from 'react-router';
import {
  Button,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Toolbar,
  Typography,
  Modal,
} from '@material-ui/core';
import {
  Add as AddIcon,
  Check as CheckIcon,
  Delete as DeleteIcon,
  Edit as EditIcon,
  Info as InfoIcon,
  Publish as PublishIcon,
} from '@material-ui/icons';
import { Alert, AlertTitle, Autocomplete } from '@material-ui/lab';
import { TextValidator, ValidatorForm } from 'react-material-ui-form-validator';

import { fetchStudentNames, createSummary } from 'Actions/summary';
import {
  showNotification,
  showProgressBar,
  hideProgressBar,
} from 'Actions/appShell';
import { NOTIFICATION_SEVERITY } from 'Components/notify/index';

const useStyles = makeStyles((theme) => ({
  container: {
    maxHeight: 440,
  },
  button: {
    backgroundColor: '#89bcff',
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  selectOption: {
    width: 250,
    backgroundColor: '#FFFFFF',
    marginLeft: 32,
  },
  warningText: {
    color: theme.palette.warning.main,
  },
}));

function SummaryAdd(props) {
  const classes = useStyles();
  const [inputFields, setInputFields] = React.useState([]);
  const summaryListHeader = [
    { id: 'Student Name', label: 'Student Name' },
    { id: 'Score', label: 'Score' },
    { id: 'Comments', label: 'Comments' },
    { id: 'Actions', label: 'Actions' },
  ];

  const [teacherId, setTeacherId] = React.useState(null);
  const [scoreOptions, setScoreOptions] = React.useState([]);
  const [studentNameOptions, setStudentNameOptions] = React.useState([]);
  const [studentsList, setStudentsList] = React.useState(null);
  const [openAlert, setOpenAlert] = useState(false);
  const [inputFieldId, setInputFieldId] = useState(0);
  const [publishFlag, setpublishFlag] = useState(false);
  const [unsavedDataWarningFlag, setUnsavedDataWarningFlag] = useState(false);
  const [gradeFilterOptions, setGradeFilterOptions] = useState([]);
  const [gradeFilterValue, setGradeFilterValue] = React.useState(null);
  const [gradeFilterTempValue, setGradeFilterTempValue] = React.useState(null);
  const [isSubmitRequested, setIsSubmitRequested] = useState(false);

  // compare grade filter option (objects) using label field
  const filterOptionComparator = (e1, e2) =>
    (e1 || { label: '' }).label.localeCompare(
      (e2 || { label: '' }).label,
      undefined,
      { numeric: true }
    );

  // once logged in teacher's profile is available, load students list
  React.useEffect(async () => {
    if (props.currentUserProfileLoaded) {
      props.showProgressBar();
      const students = await fetchStudentNames(
        props.currentUserSchoolID,
        props.currentTeacherGradeList
      );
      setStudentsList(students);
      props.hideProgressBar();
      setGradeFilterOptions(
        props.currentTeacherGradeList
          .map((e) => ({
            ...e,
            label: `${e.grade} - ${e.section} - ${e.subject}`,
          }))
          .sort(filterOptionComparator)
      );
      setScoreOptions(props.SUMMARY_SCORE_LIST);
      setTeacherId(props.currentUserId);
    }
  }, [props.currentUserProfileLoaded]);

  gradeFilterOptions.sort();

  const getFilteredStudents = () => {
    const studentNames = [];
    const filteredStudentList =
      studentsList != null && gradeFilterValue != null
        ? studentsList.filter(
            (student) =>
              student['grade'] === gradeFilterValue.grade &&
              student['section'] === gradeFilterValue.section
          )
        : [];

    Object.keys(filteredStudentList).forEach((key) =>
      studentNames.push({
        label: filteredStudentList[key]['studentName'],
        value: filteredStudentList[key]['id'],
      })
    );
    setStudentNameOptions(studentNames);
  };

  React.useEffect(async () => {
    getFilteredStudents();
  }, [gradeFilterValue]);

  const submitSummaries = async () => {
    props.showProgressBar();
    const actionResponse = await createSummary(
      props.currentUserSchoolID,
      teacherId,
      gradeFilterValue.grade,
      gradeFilterValue.section,
      gradeFilterValue.subject,
      inputFields.map((e) => ({
        studentId: e.student.value,
        score: e.score,
        comment: e.comment,
      }))
    );
    // TODO: reset input field based on action success?
    setInputFields([]);
    setIsSubmitRequested(false);
    props.showNotification(
      actionResponse.message,
      actionResponse.success
        ? NOTIFICATION_SEVERITY.SUCCESS
        : NOTIFICATION_SEVERITY.ERROR
    );
    props.hideProgressBar();
  };

  const handleModalClose = () => {
    setOpenAlert(false);
  };

  const handleOnPublish = () => {
    setIsSubmitRequested(true);
    setOpenAlert(true);
    setpublishFlag(true);
  };

  const handleConfirmButton = (name, index) => (event, value) => {
    const newArr = [...inputFields];
    newArr[index] = {
      ...inputFields[index],
      [name]: true,
    };
    setInputFields(newArr);
  };

  const handleEditButton = (name, index) => (event, value) => {
    const newArr = [...inputFields];
    newArr[index] = {
      ...inputFields[index],
      [name]: false,
    };
    setInputFields(newArr);
  };
  const onSummaryDelete = (index) => {
    const values = [...inputFields];
    values.splice(index, 1);
    setInputFields(values);
  };

  const handleInputFieldAdd = () => {
    setInputFieldId(inputFieldId + 1);
    setInputFields([
      ...inputFields,
      {
        id: inputFieldId,
        student: '',
        score: '',
        comment: '',
      },
    ]);
  };

  const changeFilter = () => {
    setOpenAlert(false);
    setUnsavedDataWarningFlag(false);
    setInputFields([]);
    setGradeFilterValue(gradeFilterTempValue);
  };

  const handleFilterChange = (event, value) => {
    // act only if filter value has changed
    if (filterOptionComparator(gradeFilterValue, value) !== 0) {
      setGradeFilterTempValue(value);
      // in case there is unsaved data
      if (inputFields.length > 0) {
        setOpenAlert(true);
        setUnsavedDataWarningFlag(true);
      } else {
        setGradeFilterValue(value);
      }
    }
  };

  const handleAutoCompleteOnChange = (name, index) => (event, value) => {
    const newArr = [...inputFields];
    newArr[index] = {
      ...inputFields[index],
      [name]: value,
      ['subject']: gradeFilterValue.subject,
      ['grade']: gradeFilterValue.grade,
    };
    setInputFields(newArr);
  };

  const handleSummaryOnChange = (index, event) => {
    const newArr = [...inputFields];
    newArr[index] = {
      ...inputFields[index],
      [event.target.name]: event.target.value,
    };
    setInputFields(newArr);
  };

  return (
    <div>
      <Prompt
        when={inputFields.length > 0}
        message={
          'Your changes have not been saved. Are you sure you want to leave?'
        }
      />
      <div>
        <ValidatorForm onSubmit={handleOnPublish}>
          <Toolbar className={'pageToolbar'} style={{ height: '100px' }}>
            <Typography variant="h6" align="center">
              Add Student Score
            </Typography>
            <div className={'flexGrow1'} />
            {!gradeFilterValue || gradeFilterOptions.length <= 0 ? (
              <Typography variant="subtitle1" className={classes.warningText}>
                {gradeFilterOptions.length <= 0
                  ? 'There are no grades assigned to you'
                  : 'Please select a grade to add score'}
              </Typography>
            ) : null}
            <Autocomplete
              id="filter-dropdown"
              options={gradeFilterOptions}
              className={classes.selectOption}
              onChange={handleFilterChange}
              getOptionLabel={(e) => e.label}
              renderInput={(params) => (
                <TextValidator
                  {...params}
                  label="Grade-Section-Topic"
                  variant="filled"
                  validators={['required']}
                  value={{ gradeFilterValue }}
                />
              )}
            />

            <Button
              color="primary"
              variant="contained"
              startIcon={<AddIcon />}
              onClick={handleInputFieldAdd}
              className={'pageToolbarAction'}
              disabled={!gradeFilterValue || studentNameOptions.length <= 0}
            >
              New Student Score
            </Button>
            <Button
              color="primary"
              variant="contained"
              startIcon={<PublishIcon />}
              className={'pageToolbarAction'}
              type="submit"
              disabled={isSubmitRequested}
            >
              Publish
            </Button>
          </Toolbar>
          <Paper>
            <TableContainer className={classes.container}>
              <Table stickyHeader aria-label="Summary list table">
                <TableHead>
                  <TableRow key="Summary list header">
                    {summaryListHeader.map((column) => (
                      <TableCell key={column.id}>{column.label}</TableCell>
                    ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  <Modal
                    open={openAlert}
                    aria-labelledby="Summary data operations modal"
                    aria-describedby="Summary data Submit"
                    className={classes.modal}
                  >
                    <div>
                      {unsavedDataWarningFlag && (
                        <Alert
                          severity="warning"
                          action={
                            <div>
                              <Button
                                color="inherit"
                                size="small"
                                onClick={() => {
                                  changeFilter();
                                }}
                              >
                                YES
                              </Button>
                              <Button
                                color="inherit"
                                size="small"
                                onClick={() => {
                                  setOpenAlert(false);
                                  setUnsavedDataWarningFlag(false);
                                }}
                              >
                                NO
                              </Button>
                            </div>
                          }
                        >
                          <AlertTitle>
                            Your Changes have not been saved. Are you sure you
                            want to change the filter?
                          </AlertTitle>
                        </Alert>
                      )}

                      {publishFlag && (
                        <Alert
                          severity="info"
                          action={
                            <div>
                              <Button
                                color="inherit"
                                size="small"
                                onClick={() => {
                                  submitSummaries();
                                  handleModalClose();
                                }}
                              >
                                YES
                              </Button>
                              <Button
                                color="inherit"
                                size="small"
                                onClick={() => {
                                  setIsSubmitRequested(false);
                                  handleModalClose();
                                }}
                              >
                                CANCEL
                              </Button>
                            </div>
                          }
                        >
                          <AlertTitle>
                            Do you want to submit the data?
                          </AlertTitle>
                        </Alert>
                      )}
                    </div>
                  </Modal>
                  {gradeFilterValue != null && studentNameOptions.length <= 0 && (
                    <List>
                      <ListItem>
                        <ListItemIcon>
                          <InfoIcon />
                        </ListItemIcon>
                        <ListItemText
                          primary={
                            'There are no students available for the selected grade/section filter'
                          }
                        />
                      </ListItem>
                    </List>
                  )}
                  {gradeFilterValue &&
                    studentNameOptions.length > 0 &&
                    inputFields.map((inputField, index) => (
                      <TableRow
                        hover
                        role="checkbox"
                        tabIndex={-1}
                        key={inputField.id}
                      >
                        <TableCell style={{ width: '20%' }}>
                          <Autocomplete
                            id="name-dropdown"
                            options={studentNameOptions}
                            getOptionLabel={(option) => option.label}
                            style={{ width: '100%' }}
                            onChange={handleAutoCompleteOnChange(
                              'student',
                              index
                            )}
                            disabled={inputField.disabled}
                            renderInput={(params) => (
                              <TextValidator
                                {...params}
                                label="Student Name"
                                variant="outlined"
                                validators={['required']}
                                errorMessages={['this field is required']}
                                value={inputField.student.label}
                              />
                            )}
                          />
                        </TableCell>
                        <TableCell style={{ width: '20%' }}>
                          <Autocomplete
                            id="score-dropdown"
                            options={scoreOptions}
                            style={{ width: '100%' }}
                            value={inputField.score}
                            onChange={handleAutoCompleteOnChange(
                              'score',
                              index
                            )}
                            disabled={inputField.disabled}
                            renderInput={(params) => (
                              <TextValidator
                                {...params}
                                label="Score"
                                variant="outlined"
                                validators={['required']}
                                errorMessages={['this field is required']}
                                value={inputField.score}
                              />
                            )}
                          />
                        </TableCell>
                        <TableCell style={{ width: '55%' }}>
                          <TextValidator
                            name="comment"
                            label="Comments"
                            multiline
                            variant="outlined"
                            disabled={inputField.disabled}
                            onChange={(event) =>
                              handleSummaryOnChange(index, event)
                            }
                            validators={['required']}
                            errorMessages={['this field is required']}
                            value={inputField.comment}
                            style={{ width: '100%' }}
                          />
                        </TableCell>
                        <TableCell style={{ width: '15%' }}>
                          <IconButton
                            aria-label="check"
                            onClick={handleConfirmButton('disabled', index)}
                          >
                            <CheckIcon />
                          </IconButton>
                          <IconButton
                            aria-label="edit"
                            onClick={handleEditButton('disabled', index)}
                          >
                            <EditIcon />
                          </IconButton>
                          <IconButton
                            aria-label="delete"
                            onClick={() => onSummaryDelete(index)}
                          >
                            <DeleteIcon />
                          </IconButton>
                        </TableCell>
                      </TableRow>
                    ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Paper>
        </ValidatorForm>
      </div>
    </div>
  );
}

SummaryAdd.propTypes = {
  SUMMARY_SCORE_LIST: PropTypes.array,
  currentUserSchoolID: PropTypes.string,
  currentUserId: PropTypes.string,
  currentTeacherGradeList: PropTypes.array,
  currentUserProfileLoaded: PropTypes.bool,
  showProgressBar: PropTypes.func.isRequired,
  hideProgressBar: PropTypes.func.isRequired,
  showNotification: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    SUMMARY_SCORE_LIST: state.authUser.SUMMARY_SCORE_LIST,
    currentUserSchoolID: state.authUser.userProfile.school.id,
    currentUserId: state.authUser.userProfile.username,
    currentTeacherGradeList: state.authUser.userProfile.userGradeSectionList,
    currentUserProfileLoaded: state.authUser.userProfileLoaded,
  };
}

export default connect(mapStateToProps, {
  showNotification,
  showProgressBar,
  hideProgressBar,
})(SummaryAdd);
