/* eslint-disable no-undef */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import {
  Button,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  Toolbar,
  Typography,
} from '@material-ui/core';
import { Info as InfoIcon } from '@material-ui/icons';
import { Autocomplete } from '@material-ui/lab';
import { TextValidator, ValidatorForm } from 'react-material-ui-form-validator';

import DataNotFound from 'Components/utils/DataNotFound';
import { fetchSummaries } from 'Actions/summary';
import { showProgressBar, hideProgressBar } from 'Actions/appShell';
import { DEFAULT_PAGE_SIZE } from 'Utils/constants';

const useStyles = makeStyles((theme) => ({
  container: {
    maxHeight: 440,
  },
  button: {
    backgroundColor: '#89bcff',
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  selectStyle: {
    color: 'black',
    background: '#d5d5d5',
  },
  filterLabel: {
    color: '#d5d5d5',
    fontWeight: 500,
    fontSize: '1rem',
    lineHeight: 3,
  },
  selectOption: {
    width: 250,
    backgroundColor: '#FFFFFF',
    marginLeft: 32,
  },
  infoBg: {
    backgroundColor: theme.palette.info.light,
  },
}));

function SummaryListing(props) {
  const classes = useStyles();

  const summaryListHeader = [
    { id: 'Student Name', label: 'Student Name' },
    { id: 'Grade', label: 'Grade' },
    { id: 'Topic', label: 'Topic' },
    { id: 'Score', label: 'Score' },
    { id: 'Comments', label: 'Comments' },
    { id: 'Entry Date', label: 'Entry Date' },
  ];

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(DEFAULT_PAGE_SIZE);
  const [studentSummaryData, setStudentSummaryData] = React.useState([]);
  const [filterSummaryData, setFilterSummaryData] = React.useState([]);
  const [gradeFilterOptions, setGradeFilterOptions] = React.useState([]);
  const [gradeFilterValue, setGradeFilterValue] = React.useState(null);

  const currentMonth = new Date().toLocaleString('en-us', {
    month: 'long',
    year: 'numeric',
  });

  // compare grade filter option (objects) using label field
  const filterOptionComparator = (e1, e2) =>
    (e1 || { label: '' }).label.localeCompare(
      (e2 || { label: '' }).label,
      undefined,
      { numeric: true }
    );

  React.useEffect(async () => {
    if (props.currentUserProfileLoaded) {
      props.showProgressBar();
      const studentSummaryData = await fetchSummaries(
        props.currentUserSchoolID,
        props.currentUserId
      );
      setStudentSummaryData(studentSummaryData);
      setFilterSummaryData(studentSummaryData);
      props.hideProgressBar();
      setGradeFilterOptions(
        props.currentTeacherGradeList
          .map((e) => ({
            ...e,
            label: `${e.grade} - ${e.section} - ${e.subject}`,
          }))
          .sort(filterOptionComparator)
      );
    }
  }, [props.currentUserProfileLoaded]);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const filterSummaryDataList = () => {
    const newFormattedData =
      gradeFilterValue != null
        ? studentSummaryData.filter(
            (summary) =>
              summary.student &&
              summary.subject === gradeFilterValue.subject &&
              summary.grade === gradeFilterValue.grade &&
              summary.section === gradeFilterValue.section
          )
        : studentSummaryData;
    setFilterSummaryData(newFormattedData);
  };

  React.useEffect(() => {
    filterSummaryDataList();
  }, [gradeFilterValue]);
  const handleFilterChange = (event, value) => {
    setGradeFilterValue(value);
  };

  return (
    <div>
      <ValidatorForm>
        <Toolbar className={'pageToolbar'} style={{ height: '100px' }}>
          <Typography variant="h6" align="center">
            View Student Score
          </Typography>
          <div className={'flexGrow1'} />
          <Autocomplete
            id="section-dropdown"
            options={gradeFilterOptions}
            getOptionLabel={(e) => e.label}
            className={classes.selectOption}
            onChange={handleFilterChange}
            renderInput={(params) => (
              <TextValidator
                {...params}
                label="Filter"
                variant="filled"
                validators={['required']}
                errorMessages={['this field is required']}
                value={gradeFilterValue}
              />
            )}
          />
        </Toolbar>
      </ValidatorForm>
      <List>
        <ListItem className={classes.infoBg}>
          <ListItemIcon>
            <Button variant="text" disabled startIcon={<InfoIcon />}>
              NOTE:
            </Button>
          </ListItemIcon>
          <ListItemText
            primary={`Data shown here is only for the current month - ${currentMonth}`}
          />
        </ListItem>
      </List>
      <Paper>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="Summary list table">
            <TableHead>
              <TableRow key="Summary list header">
                {summaryListHeader.map((column) => (
                  <TableCell key={column.id}>{column.label}</TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {(studentSummaryData === undefined ||
                studentSummaryData.length === 0) && <DataNotFound />}
              {studentSummaryData !== undefined &&
                ((filteredData) => {
                  return filteredData.length > 0 ? (
                    filteredData
                      .slice(
                        page * rowsPerPage,
                        page * rowsPerPage + rowsPerPage
                      )
                      .map((slicedRow) => {
                        if (
                          slicedRow.student !== null &&
                          slicedRow.student['studentName'] &&
                          slicedRow.student['grade'] &&
                          slicedRow.subject &&
                          slicedRow.score &&
                          slicedRow.comment
                        )
                          return (
                            <TableRow
                              hover
                              role="checkbox"
                              tabIndex={-1}
                              key={slicedRow.id}
                            >
                              <TableCell>
                                {slicedRow.student['studentName']}
                              </TableCell>
                              <TableCell>{slicedRow.grade}</TableCell>
                              <TableCell>{slicedRow.subject}</TableCell>
                              <TableCell>{slicedRow.score}</TableCell>
                              <TableCell>{slicedRow.comment}</TableCell>
                              <TableCell>
                                {new Date(
                                  slicedRow.createdAt
                                ).toLocaleDateString()}
                              </TableCell>
                            </TableRow>
                          );
                      })
                  ) : (
                    <TableRow hover role="checkbox" tabIndex={-1} key={0}>
                      <TableCell colSpan={summaryListHeader.length}>
                        <Typography variant="subtitle2">
                          No data to display
                        </Typography>
                      </TableCell>
                    </TableRow>
                  );
                })(filterSummaryData)}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 15]}
          component="div"
          count={
            studentSummaryData !== undefined ? filterSummaryData.length : 0
          }
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
}

SummaryListing.propTypes = {
  currentUserSchoolID: PropTypes.string,
  currentTeacherGradeList: PropTypes.array,
  currentUserId: PropTypes.string,
  currentUserProfileLoaded: PropTypes.bool,
  showProgressBar: PropTypes.func.isRequired,
  hideProgressBar: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    currentUserSchoolID: state.authUser.userProfile.school.id,
    currentUserId: state.authUser.userProfile.username,
    currentTeacherGradeList: state.authUser.userProfile.userGradeSectionList,
    currentUserProfileLoaded: state.authUser.userProfileLoaded,
  };
}

export default connect(mapStateToProps, {
  showProgressBar,
  hideProgressBar,
})(SummaryListing);
