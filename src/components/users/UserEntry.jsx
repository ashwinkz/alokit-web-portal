import React from 'react';
import {
  Button,
  Divider,
  InputAdornment,
  MenuItem,
  Typography,
  Toolbar,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {
  ValidatorForm,
  TextValidator,
  SelectValidator,
} from 'react-material-ui-form-validator';
import PropTypes from 'prop-types';

import {
  MOBILE_NUMBER_PREFIX,
  validatePhoneNumber,
  validateName,
} from 'Utils/appShell';
import { CREDENTIALS_DELIVERY_OPTIONS } from 'Utils/constants';

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'relative',
    width: 800,
    height: '50%',
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2, 4, 3),
    justify: 'center',
  },
}));

export default function UserEntry(props) {
  const classes = useStyles();
  const [editdata, setEditdata] = React.useState({
    id: props.data.id,
    firstName: props.data.firstName,
    lastName: props.data.lastName,
    role: props.data.role,
    email: props.data.email,
    phoneNumber: props.data.phoneNumber,
  });
  const [submitted, setSubmitted] = React.useState(false);
  const handleValueChange = (name) => (e) => {
    const value = e.target.value;
    setEditdata({ ...editdata, [name]: value });
  };
  const [selectedDeliveryMedium, setSelectedDeliveryMedium] = React.useState(
    CREDENTIALS_DELIVERY_OPTIONS.EMAIL
  );
  const handleDeliveryMediumChange = (e) => {
    setSelectedDeliveryMedium(e.target.value);
  };

  const onSubmitHandle = () => {
    setSubmitted(true);
    props.onSubmit(editdata, selectedDeliveryMedium);
  };

  ValidatorForm.addValidationRule('isPhoneNumber', (value) =>
    validatePhoneNumber(value)
  );
  ValidatorForm.addValidationRule('isValidName', (value) =>
    validateName(value)
  );

  const onCancel = () => {
    props.onCancel(true);
  };

  return (
    <div className={classes.paper}>
      <ValidatorForm onSubmit={onSubmitHandle}>
        <Typography variant="h5" align="center">
          {props.editMode ? 'Edit' : 'Add'} User
        </Typography>
        <Divider className="contentMarginTop" />
        <div className="popupFormContent">
          <TextValidator
            label="First Name"
            onChange={handleValueChange('firstName')}
            name="firstName"
            value={editdata.firstName}
            validators={['required', 'isValidName']}
            errorMessages={[
              'this field is required',
              'This is not a valid name',
            ]}
            fullWidth
            variant="outlined"
          />
          <br />
          <TextValidator
            label="Last Name"
            onChange={handleValueChange('lastName')}
            name="lastName"
            value={editdata.lastName}
            validators={['required', 'isValidName']}
            errorMessages={[
              'this field is required',
              'This is not a valid name',
            ]}
            fullWidth
            variant="outlined"
          />
          <br />
          <TextValidator
            label="Phone Number"
            name="phoneNumber"
            value={editdata.phoneNumber}
            onChange={handleValueChange('phoneNumber')}
            validators={['required', 'isPhoneNumber']}
            errorMessages={[
              'This field is required',
              'Mobile number is not valid',
            ]}
            disabled={props.editMode}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  {MOBILE_NUMBER_PREFIX}
                </InputAdornment>
              ),
            }}
            fullWidth
            variant="outlined"
          />
          <br />
          <TextValidator
            label="Email ID"
            onChange={handleValueChange('email')}
            name="emailID"
            value={editdata.email}
            validators={['required', 'isEmail']}
            errorMessages={['this field is required', 'email is not valid']}
            disabled={props.editMode}
            fullWidth
            variant="outlined"
          />
          <br />
          {props.editMode === false ? (
            <>
              <SelectValidator
                label="Send login credentials through"
                onChange={handleDeliveryMediumChange}
                name="Delivery Medium"
                value={selectedDeliveryMedium}
                validators={['required']}
                errorMessages={['this field is required']}
                variant="outlined"
                fullWidth
              >
                {Object.keys(CREDENTIALS_DELIVERY_OPTIONS).map((medium) => (
                  <MenuItem
                    key={medium}
                    value={CREDENTIALS_DELIVERY_OPTIONS[medium]}
                  >
                    {CREDENTIALS_DELIVERY_OPTIONS[medium]}
                  </MenuItem>
                ))}
              </SelectValidator>

              <br />
            </>
          ) : null}

          <SelectValidator
            label="Role"
            onChange={handleValueChange('role')}
            disabled={
              props.editMode && editdata.role === props.adminRoleToFreeze
            }
            name="role"
            value={editdata.role}
            validators={['required']}
            errorMessages={['this field is required']}
            variant="outlined"
            fullWidth
          >
            {props.roleOptions.map((role, index) => (
              <MenuItem key={index} value={role}>
                {role}
              </MenuItem>
            ))}
          </SelectValidator>
          <br />
        </div>
        <Divider className="contentMarginTop" />
        <Toolbar disableGutters>
          <dir className="flexGrow1" />
          <Button
            color="primary"
            variant="outlined"
            type="submit"
            className="toolbarAction"
            onClick={onCancel}
          >
            Cancel
          </Button>
          <Button
            color="primary"
            variant="contained"
            type="submit"
            className="toolbarAction"
            disabled={submitted}
          >
            {(submitted && 'Your form is submitted!') ||
              (!submitted && 'Submit')}
          </Button>
        </Toolbar>
      </ValidatorForm>
    </div>
  );
}

UserEntry.propTypes = {
  data: PropTypes.shape({
    id: PropTypes.string.isRequired,
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    role: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    phoneNumber: PropTypes.string.isRequired,
  }),
  editMode: PropTypes.bool.isRequired,
  adminRoleToFreeze: PropTypes.string.isRequired,
  roleOptions: PropTypes.array.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
};
