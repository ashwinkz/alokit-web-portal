import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Button,
  Fade,
  IconButton,
  ListItemText,
  Menu,
  MenuItem,
  Modal,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  Toolbar,
  Tooltip,
  Typography,
} from '@material-ui/core';
import { Add as AddIcon, MoreVert as MoreVertIcon } from '@material-ui/icons';
import { connect } from 'react-redux';

import SearchBar from 'Components/search/index';
import UserEntry from './UserEntry';
import {
  fetchAlokitUsers,
  addAlokitUser,
  editAlokitUser,
} from 'Actions/alokitUser';
import { MOBILE_NUMBER_PREFIX } from 'Utils/appShell';
import { NOTIFICATION_SEVERITY } from 'Components/notify/index';
import {
  showNotification,
  showProgressBar,
  hideProgressBar,
} from 'Actions/appShell';
import { AUTH_ROLES, DEFAULT_PAGE_SIZE } from 'Utils/constants';
import DataNotFound from 'Components/utils/DataNotFound';
import { modifyAlokitUserAccess } from 'Actions/alokitUser';

const useStyles = makeStyles((theme) => ({
  container: {
    maxHeight: 440,
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    minHeight: '600',
  },
  close: {
    backgroundColor: theme.palette.background.paper,
  },
}));

function userListing(props) {
  const classes = useStyles();
  const [fetchUserData, setFetchUserData] = React.useState(false);
  const [userData, setUserData] = React.useState([]);
  const [filteredUserData, setFilteredUserData] = React.useState([]);
  const UserListHeader = [
    { id: 'First Name', label: 'First Name' },
    { id: 'Last Name', label: 'Last Name' },
    { id: 'Role', label: 'Role' },
    { id: 'emailID', label: 'Email' },
    { id: 'Actions', label: 'Actions' },
  ];
  const [page, setPage] = React.useState(0);
  const [searchValue, setSearchValue] = React.useState('');
  const [rowsPerPage, setRowsPerPage] = React.useState(DEFAULT_PAGE_SIZE);
  const [selectedUserId, setSelectedUserId] = React.useState(null);
  const [optionsMenuAnchor, setOptionsMenuAnchor] = React.useState(null);
  const [formOpen, setFormOpen] = React.useState(false);
  const [editFlag, setEditFlag] = React.useState(false);
  const [userFormData, setUserFormData] = React.useState({
    id: '',
    firstName: '',
    lastName: '',
    email: '',
    role: '',
    phoneNumber: '',
  });
  // const [deleteMessage, setDeleteMessage] = React.useState('');
  // const [deleteFlag, setDeleteFlag] = React.useState(false);
  // const [deleteId, setDeleteId] = React.useState(0);

  const mapUserRecord = (u) => ({
    id: u.cognitoUsername,
    firstName: u.firstName,
    lastName: u.lastName,
    role: u.role,
    email: u.email,
    phoneNumber: u.phoneNumber,
    disabled: u.disabled,
  });
  React.useEffect(() => {
    if (props.currentUserProfileLoaded) setFetchUserData(true);
  }, [props.currentUserProfileLoaded]);
  React.useEffect(async () => {
    if (fetchUserData) {
      console.log('fetching users from backend');
      props.showProgressBar();
      const userList = await fetchAlokitUsers();
      setFetchUserData(false);
      const formattedUserData = userList.map((u) => mapUserRecord(u));
      setUserData(formattedUserData);
      setFilteredUserData(formattedUserData);
      props.hideProgressBar();
    }
  }, [fetchUserData]);

  const onUserEdit = (row) => {
    setEditFlag(true);
    setUserFormData({
      id: row.id,
      firstName: row.firstName,
      lastName: row.lastName,
      role: row.role,
      email: row.email,
      phoneNumber: row.phoneNumber.replace(MOBILE_NUMBER_PREFIX, ''),
    });
    setFormOpen(true);
  };

  const onUserAdd = () => {
    setEditFlag(false);
    setUserFormData({
      id: '',
      firstName: '',
      lastName: '',
      role: '',
      email: '',
      phoneNumber: '',
    });
    setFormOpen(true);
  };

  const handleOpenOptionsMenu = (e, selectedTeacherId) => {
    setSelectedUserId(selectedTeacherId);
    setOptionsMenuAnchor(e.currentTarget);
  };

  const handleCloseOptionsMenu = () => {
    setSelectedUserId(null);
    setOptionsMenuAnchor(null);
  };

  const handleModalClose = () => {
    setFormOpen(false);
  };

  const handleModifyAccessAction = async (userId, disableUser) => {
    handleCloseOptionsMenu();
    props.showProgressBar();
    const actionResponse = await modifyAlokitUserAccess(userId, disableUser);

    // display action result
    props.showNotification(
      actionResponse.message,
      actionResponse.success
        ? NOTIFICATION_SEVERITY.SUCCESS
        : NOTIFICATION_SEVERITY.ERROR
    );
    props.hideProgressBar();

    if (actionResponse.success) {
      // refresh teacher listing
      setFetchUserData(true);
    }
  };

  const handleUserSubmission = async (userDetails, deliveryMedium) => {
    let actionResponse = {};
    props.showProgressBar();
    if (!editFlag) {
      actionResponse = await addAlokitUser(
        userDetails.firstName,
        userDetails.lastName,
        MOBILE_NUMBER_PREFIX.concat(userDetails.phoneNumber), // add country code
        userDetails.email,
        userDetails.role,
        deliveryMedium
      );
    } else {
      // only name/branch/address can be updated
      actionResponse = await editAlokitUser(
        userDetails.id,
        userDetails.firstName,
        userDetails.lastName,
        userDetails.role
      );
    }

    // display action result
    props.showNotification(
      actionResponse.message,
      actionResponse.success
        ? NOTIFICATION_SEVERITY.SUCCESS
        : NOTIFICATION_SEVERITY.ERROR
    );
    props.hideProgressBar();
    handleModalClose();

    if (actionResponse.success) {
      //refresh users listing
      setFetchUserData(true);
    } else {
      //re-open form with entered data
      setUserFormData(userDetails);
      setFormOpen(true);
    }
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const filterUserList = () => {
    const newFilteredData = searchValue
      ? userData.filter(
          (record) =>
            record['firstName']
              .toLowerCase()
              .includes(searchValue.toLowerCase()) ||
            record['lastName'].toLowerCase().includes(searchValue.toLowerCase())
        )
      : userData;
    setFilteredUserData(newFilteredData);
  };
  React.useEffect(() => {
    filterUserList();
  }, [searchValue]);
  return (
    <div>
      <Toolbar className={'pageToolbar'}>
        <Typography variant="h6">Manage Users</Typography>
        <div className={'flexGrow1'} />
        <SearchBar
          value={searchValue}
          onChange={(newSearchValue) => setSearchValue(newSearchValue)}
          onCancelSearch={() => setSearchValue('')}
          className={'pageToolbarAction'}
        />
        <Button
          color="primary"
          variant="contained"
          startIcon={<AddIcon />}
          onClick={onUserAdd}
          className={'pageToolbarAction'}
        >
          Add User
        </Button>
      </Toolbar>
      <Paper>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="Aloki Users table">
            <TableHead>
              <TableRow key="Alokit Users header">
                {UserListHeader.map((column) => (
                  <TableCell key={column.label}>{column.label}</TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              <Modal
                open={formOpen}
                onclose={(event, reason) => {
                  if (reason === 'escapeKeyDown') handleModalClose();
                }}
                aria-labelledby="Alokit Users data operations modal"
                aria-describedby="Alokit User data add/edit/delete modal"
                className={classes.modal}
              >
                <div>
                  <UserEntry
                    data={userFormData}
                    editMode={editFlag}
                    adminRoleToFreeze={AUTH_ROLES.ALOKIT_ADMIN}
                    roleOptions={props.ALOKIT_USER_ROLE_LIST}
                    onCancel={handleModalClose}
                    onSubmit={handleUserSubmission}
                  />
                </div>
              </Modal>
              {userData !== undefined &&
                filteredUserData
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((slicedRow) => {
                    return (
                      <TableRow
                        hover
                        role="checkbox"
                        tabIndex={-1}
                        key={slicedRow.cognitoUsername}
                      >
                        <TableCell>{slicedRow.firstName}</TableCell>
                        <TableCell>{slicedRow.lastName}</TableCell>
                        <TableCell>{slicedRow.role}</TableCell>
                        <TableCell>{slicedRow.email}</TableCell>
                        <TableCell>
                          <Tooltip
                            title={
                              'Edit ' +
                              slicedRow.firstName +
                              ' ' +
                              slicedRow.lastName
                            }
                          >
                            <Button
                              color="secondary"
                              variant="contained"
                              aria-label="edit"
                              onClick={() => onUserEdit(slicedRow)}
                              disableElevation
                              disabled={slicedRow.disabled}
                            >
                              Edit
                            </Button>
                          </Tooltip>
                          {slicedRow.role !== AUTH_ROLES.ALOKIT_ADMIN && (
                            <Tooltip title={'more options'}>
                              <IconButton
                                aria-label="more"
                                aria-controls="alokit-user-menu"
                                aria-haspopup="true"
                                onClick={(e) =>
                                  handleOpenOptionsMenu(e, slicedRow.id)
                                }
                              >
                                <MoreVertIcon />
                              </IconButton>
                            </Tooltip>
                          )}
                          {slicedRow.id === selectedUserId && (
                            <Menu
                              id="alokit-user-menu"
                              anchorEl={optionsMenuAnchor}
                              open={Boolean(optionsMenuAnchor)}
                              onClose={handleCloseOptionsMenu}
                              TransitionComponent={Fade}
                            >
                              <MenuItem
                                onClick={() =>
                                  handleModifyAccessAction(
                                    slicedRow.id,
                                    !slicedRow.disabled
                                  )
                                }
                              >
                                <ListItemText
                                  primary={
                                    slicedRow.disabled
                                      ? 'Re-enable Access'
                                      : 'Remove Access'
                                  }
                                />
                              </MenuItem>
                            </Menu>
                          )}
                        </TableCell>
                      </TableRow>
                    );
                  })}
              {(userData === undefined || userData.length === 0) && (
                <DataNotFound />
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 15]}
          component="div"
          count={userData !== undefined ? filteredUserData.length : 0}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
}

function mapStateToProps(state) {
  return {
    ALOKIT_USER_ROLE_LIST: state.authUser.ALOKIT_USER_ROLE_LIST,
    currentUserProfileLoaded: state.authUser.userProfileLoaded,
  };
}

export default connect(mapStateToProps, {
  showNotification,
  showProgressBar,
  hideProgressBar,
})(userListing);
