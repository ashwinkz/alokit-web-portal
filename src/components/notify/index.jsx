import React from 'react';
import PropTypes from 'prop-types';
import { Alert, AlertTitle } from '@material-ui/lab';
import { Button, Snackbar, LinearProgress } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { compose } from 'redux';
import { connect } from 'react-redux';

import { hideNotification } from 'Actions/appShell';

const contentStyles = (theme) => ({
  title: {
    textTransform: 'capitalize',
  },
  message: {},
});

class ActionNotification extends React.Component {
  constructor(props) {
    super(props);
    this.handleSnackbarClose = this.handleSnackbarClose.bind(this);
  }

  handleSnackbarClose = () => {
    this.props.hideNotification();
  };

  render() {
    const { classes, showNotification, message, severity } = this.props;
    return (
      <div>
        <Snackbar
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
          open={showNotification}
          // autoHideDuration={6000}
        >
          <Alert
            action={
              <Button
                variant="outlined"
                color="inherit"
                size="medium"
                disableElevation
                onClick={this.handleSnackbarClose}
              >
                OK
              </Button>
            }
            variant="filled"
            severity={severity}
          >
            <AlertTitle className={classes.title}>{severity}</AlertTitle>
            {message}
          </Alert>
        </Snackbar>
      </div>
    );
  }
}

ActionNotification.propTypes = {
  classes: PropTypes.object.isRequired,
  showNotification: PropTypes.bool.isRequired,
  message: PropTypes.string,
  severity: PropTypes.string,
  hideNotification: PropTypes.func.isRequired,
};

ActionNotification = compose(
  withStyles(contentStyles, { withTheme: true }),
  connect(
    (state) => ({
      showNotification: state.appShell.showNotification,
      message: state.appShell.notification.message,
      severity: state.appShell.notification.severity,
    }),
    {
      hideNotification,
    }
  )
)(ActionNotification);

let ProgressNotification = (props) => {
  return <div>{props.showProgressBar && <LinearProgress />}</div>;
};

ProgressNotification.propTypes = { showProgressBar: PropTypes.bool };

ProgressNotification = compose(
  withStyles(contentStyles, { withTheme: true }),
  connect(
    (state) => ({
      showProgressBar: state.appShell.showProgress,
    }),
    null
  )
)(ProgressNotification);

const NOTIFICATION_SEVERITY = {
  SUCCESS: 'success',
  ERROR: 'error',
};

export { ActionNotification, ProgressNotification, NOTIFICATION_SEVERITY };
