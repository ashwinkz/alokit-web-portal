import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Button,
  Modal,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Toolbar,
  Tooltip,
  Typography,
  TablePagination,
} from '@material-ui/core';
import { connect } from 'react-redux';

import SearchBar from 'Components/search/index';
import StudentEdit from './StudentEdit';
import { fetchStudents } from 'Actions/student';
import {
  showProgressBar,
  hideProgressBar,
  showNotification,
} from 'Actions/appShell';
import { editStudentInfo } from 'Actions/student';
import { NOTIFICATION_SEVERITY } from 'Components/notify/index';
import { DEFAULT_PAGE_SIZE } from 'Utils/constants';
import DataNotFound from 'Components/utils/DataNotFound';

const useStyles = makeStyles((theme) => ({
  container: {
    maxHeight: 440,
  },
  button: {
    backgroundColor: '#89bcff',
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  close: {
    backgroundColor: theme.palette.background.paper,
  },
}));

function studentListing(props) {
  const classes = useStyles();
  const [studentData, setStudentData] = React.useState([]);
  const [filterStudentData, setFilterStudentData] = React.useState([]);
  const [fetchStudentData, setFetchStudentData] = React.useState(false);

  const mapStudentRecord = (t) => ({
    studentId: t.id,
    rollNumber: t.rollNumber,
    studentName: t.studentName,
    gender: t.gender,
    grade: t.grade,
    section: t.section,
    schoolId: t.schoolId,
  });
  React.useEffect(() => {
    if (props.currentUserProfileLoaded) setFetchStudentData(true);
  }, [props.currentUserProfileLoaded]);
  React.useEffect(async () => {
    if (fetchStudentData) {
      props.showProgressBar();
      const fetchedData = await fetchStudents(props.currentUserSchoolId);
      setFetchStudentData(false);
      const formattedStudentData = fetchedData.map((t) => mapStudentRecord(t));
      setStudentData(formattedStudentData);
      setFilterStudentData(formattedStudentData);
      props.hideProgressBar();
    }
  }, [fetchStudentData]);

  const StudentListHeader = [
    { id: 'Roll Number', label: 'Roll Number' },
    { id: 'Student Name', label: 'Student Name' },
    { id: 'Gender', label: 'Gender' },
    { id: 'Grade', label: 'Grade' },
    { id: 'Section', label: 'Section' },
    { id: 'Actions', label: 'Actions' },
  ];
  const [page, setPage] = React.useState(0);
  const [searchValue, setSearchValue] = React.useState('');
  const [rowsPerPage, setRowsPerPage] = React.useState(DEFAULT_PAGE_SIZE);
  const [formOpen, setFormOpen] = React.useState(false);
  const [formData, setFormData] = React.useState({
    studentId: '',
    rollNumber: '',
    studentName: '',
    gender: '',
    grade: '',
    section: '',
  });
  // const [deleteMessage, setDeleteMessage] = React.useState('');
  // const [deleteFlag, setDeleteFlag] = React.useState(false);
  // const [deleteId, setDeleteId] = React.useState(0);

  const onStudentEdit = (row) => {
    setFormOpen(true);
    setFormData({
      studentId: row.studentId,
      rollNumber: row.rollNumber,
      studentName: row.studentName,
      gender: row.gender,
      grade: row.grade,
      section: row.section,
    });
  };

  // const onStudentDelete = (studentDetails) => {
  //   setFormOpen(true);
  //   setDeleteFlag(true);
  //   setDeleteId(studentDetails.studentId);
  //   setDeleteMessage(
  //     'Are you sure you want to delete ' + studentDetails.studentName + ' ?'
  //   );
  // };

  const handleModalClose = () => {
    setFormOpen(false);
  };

  const handleSubmission = async (studentDetails) => {
    props.showProgressBar();
    const actionResponse = await editStudentInfo(
      studentDetails.studentId,
      studentDetails.rollNumber,
      studentDetails.studentName,
      studentDetails.gender,
      studentDetails.grade,
      studentDetails.section
    );
    // display action result
    props.showNotification(
      actionResponse.message,
      actionResponse.success
        ? NOTIFICATION_SEVERITY.SUCCESS
        : NOTIFICATION_SEVERITY.ERROR
    );
    props.hideProgressBar();
    handleModalClose();

    if (actionResponse.success) {
      //refresh school listing
      setFetchStudentData(true);
    } else {
      // re-open form with entered data
      setFormData(studentDetails);
      setFormOpen(true);
    }
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  const filterStudentDataList = () => {
    const newFilteredData = searchValue
      ? studentData.filter((record) =>
          record['studentName']
            .toLowerCase()
            .includes(searchValue.toLowerCase())
        )
      : studentData;
    setFilterStudentData(newFilteredData);
  };

  React.useEffect(() => {
    filterStudentDataList();
  }, [searchValue]);

  return (
    <div>
      <Toolbar className={'pageToolbar'}>
        <Typography variant="h6">Manage Students</Typography>
        <div className={'flexGrow1'} />
        <SearchBar
          value={searchValue}
          onChange={(newSearchValue) => setSearchValue(newSearchValue)}
          onCancelSearch={() => setSearchValue('')}
          className={'pageToolbarAction'}
        />
      </Toolbar>
      <Paper>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="Student list table">
            <TableHead>
              <TableRow key="Student list header">
                {StudentListHeader.map((column) => (
                  <TableCell key={column.id}>{column.label}</TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              <Modal
                open={formOpen}
                onclose={(event, reason) => {
                  if (reason === 'escapeKeyDown') handleModalClose();
                }}
                aria-labelledby="Student data operations modal"
                aria-describedby="Student data add/edit/delete modal"
                className={classes.modal}
              >
                {
                  // deleteFlag === true ? (
                  //   <Alert
                  //     severity="warning"
                  //     action={
                  //       <div>
                  //         <Button
                  //           color="inherit"
                  //           size="small"
                  //           onClick={() => {
                  //             console.log('deleting id' + deleteId);
                  //             var newData = [...studentData];
                  //             var result = studentData.findIndex(
                  //               (row) => row.studentId === deleteId
                  //             );
                  //             newData.splice(result, 1);
                  //             deleteStudentInfo(deleteId);
                  //             setStudentData(newData);
                  //             handleModalClose();
                  //           }}
                  //         >
                  //           YES
                  //         </Button>
                  //         <Button
                  //           color="inherit"
                  //           size="small"
                  //           onClick={() => {
                  //             handleModalClose();
                  //           }}
                  //         >
                  //           CANCEL
                  //         </Button>
                  //       </div>
                  //     }
                  //   >
                  //     <AlertTitle>Warning</AlertTitle>
                  //     <strong>{deleteMessage}</strong>
                  //   </Alert>
                  // ) : (
                  <div>
                    {/* <div className={classes.close}>
                        <IconButton onClick={handleModalClose}>
                          <CloseIcon />
                        </IconButton>
                      </div> */}
                    <StudentEdit
                      data={formData}
                      genderOptions={props.GENDER_LIST}
                      gradeOptions={props.GRADE_LIST}
                      sectionOptions={props.SECTION_LIST}
                      onCancel={handleModalClose}
                      onSubmit={handleSubmission}
                    />
                  </div>
                  // )
                }
              </Modal>
              {(studentData === undefined || studentData.length === 0) && (
                <DataNotFound />
              )}
              {filterStudentData
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((slicedRow) => {
                  return (
                    <TableRow
                      hover
                      role="checkbox"
                      tabIndex={-1}
                      key={slicedRow.studentId}
                    >
                      <TableCell>{slicedRow.rollNumber}</TableCell>
                      <TableCell>{slicedRow.studentName}</TableCell>
                      <TableCell>{slicedRow.gender}</TableCell>
                      <TableCell>{slicedRow.grade}</TableCell>
                      <TableCell>{slicedRow.section}</TableCell>
                      <TableCell>
                        <Tooltip title={`Edit ${slicedRow.studentName}`}>
                          <Button
                            color="secondary"
                            variant="contained"
                            aria-label="edit"
                            onClick={() => onStudentEdit(slicedRow)}
                            disableElevation
                          >
                            Edit
                          </Button>
                        </Tooltip>
                      </TableCell>
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 15]}
          component="div"
          count={studentData !== undefined ? filterStudentData.length : 0}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
}

function mapStateToProps(state) {
  return {
    GENDER_LIST: state.authUser.GENDER_LIST,
    GRADE_LIST: state.authUser.GRADE_LIST,
    SECTION_LIST: state.authUser.SECTION_LIST,
    currentUserSchoolId: state.authUser.userProfile.school.id,
    currentUserProfileLoaded: state.authUser.userProfileLoaded,
  };
}

export default connect(mapStateToProps, {
  showProgressBar,
  hideProgressBar,
  showNotification,
})(studentListing);
