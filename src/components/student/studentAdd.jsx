import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Button,
  IconButton,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Toolbar,
  Typography,
  Modal,
} from '@material-ui/core';
import {
  Add as AddIcon,
  Check as CheckIcon,
  Delete as DeleteIcon,
  Edit as EditIcon,
  Publish as PublishIcon,
} from '@material-ui/icons';
import { Alert, AlertTitle, Autocomplete } from '@material-ui/lab';
import { connect } from 'react-redux';
import { TextValidator, ValidatorForm } from 'react-material-ui-form-validator';

import { addStudentInfo } from 'Actions/student';
import {
  showNotification,
  showProgressBar,
  hideProgressBar,
} from 'Actions/appShell';
import { NOTIFICATION_SEVERITY } from 'Components/notify/index';
import { validateName } from 'Utils/appShell';

const useStyles = makeStyles((theme) => ({
  container: {
    maxHeight: 440,
  },
  button: {
    backgroundColor: '#89bcff',
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  selectOption: {
    width: 150,
    backgroundColor: '#FFFFFF',
    marginLeft: 32,
  },
}));

function studentAdd(props) {
  const classes = useStyles();
  const [inputFields, setInputFields] = React.useState([
    {
      rollNumber: '',
      studentName: '',
      gender: '',
      grade: '',
      section: '',
      schoolId: props.currentUserSchoolID,
    },
  ]);
  const handleConfirmButton = (name, index) => (event, value) => {
    const newArr = [...inputFields];
    newArr[index] = {
      ...inputFields[index],
      [name]: true,
    };
    setInputFields(newArr);
  };

  const handleEditButton = (name, index) => (event, value) => {
    const newArr = [...inputFields];
    newArr[index] = {
      ...inputFields[index],
      [name]: false,
    };
    setInputFields(newArr);
  };

  const studentListHeader = [
    { id: 'Roll Number', label: 'Roll Number' },
    { id: 'Student Name', label: 'Student Name' },
    { id: 'Gender', label: 'Gender' },
    { id: 'Grade', label: 'Grade' },
    { id: 'Section', label: 'Section' },
    { id: 'Actions', label: 'Actions' },
  ];
  const [openAlert, setOpenAlert] = useState(false);
  const [inputFieldId, setInputFieldId] = useState(0);
  const [publishFlag, setpublishFlag] = useState(false);
  const [isSubmitRequested, setIsSubmitRequested] = useState(false);

  const submitStudents = async () => {
    props.showProgressBar();
    const actionResponse = await addStudentInfo(inputFields);
    // TODO: reset input field based on action success?
    setInputFields([]);
    setIsSubmitRequested(false);
    props.showNotification(
      actionResponse.message,
      actionResponse.success
        ? NOTIFICATION_SEVERITY.SUCCESS
        : NOTIFICATION_SEVERITY.ERROR
    );
    props.hideProgressBar();
  };

  const handleModalClose = () => {
    setOpenAlert(false);
  };

  const handleOnPublish = () => {
    setIsSubmitRequested(true);
    setOpenAlert(true);
    setpublishFlag(true);
  };

  ValidatorForm.addValidationRule('isValidName', (value) =>
    validateName(value)
  );

  const onSummaryDelete = (index) => {
    const values = [...inputFields];
    values.splice(index, 1);
    setInputFields(values);
  };

  const handleInputFieldAdd = () => {
    setInputFieldId(inputFieldId + 1);
    setInputFields([
      ...inputFields,
      {
        id: inputFieldId,
        rollNumber: '',
        studentName: '',
        gender: '',
        grade: '',
        section: '',
        schoolId: props.currentUserSchoolID,
      },
    ]);
  };
  const handleAutoCompleteOnChange = (name, index) => (event, value) => {
    const newArr = [...inputFields];
    newArr[index] = {
      ...inputFields[index],
      [name]: value,
    };
    setInputFields(newArr);
  };
  const handleStudentChange = (index, event) => {
    const newArr = [...inputFields];
    newArr[index] = {
      ...inputFields[index],
      [event.target.name]: event.target.value,
    };
    setInputFields(newArr);
  };

  return (
    <div>
      <ValidatorForm onSubmit={handleOnPublish}>
        <Toolbar className={'pageToolbar'} style={{ height: '100px' }}>
          <Typography variant="h6" align="center">
            Add Student
          </Typography>
          <div className={'flexGrow1'} />
          <Button
            color="primary"
            variant="contained"
            startIcon={<AddIcon />}
            onClick={handleInputFieldAdd}
            className={'pageToolbarAction'}
          >
            New Student
          </Button>
          <Button
            color="primary"
            variant="contained"
            startIcon={<PublishIcon />}
            className={'pageToolbarAction'}
            type="submit"
            disabled={isSubmitRequested}
          >
            Publish
          </Button>
        </Toolbar>
        <Paper>
          <TableContainer className={classes.container}>
            <Table stickyHeader aria-label="Student list table">
              <TableHead>
                <TableRow key="Student list header">
                  {studentListHeader.map((column) => (
                    <TableCell key={column.id}>{column.label}</TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                <Modal
                  open={openAlert}
                  aria-labelledby="Add data operations modal"
                  aria-describedby="Add data Submit"
                  className={classes.modal}
                >
                  <div>
                    {publishFlag && (
                      <Alert
                        severity="info"
                        action={
                          <div>
                            <Button
                              color="inherit"
                              size="small"
                              onClick={() => {
                                submitStudents();
                                handleModalClose();
                              }}
                            >
                              YES
                            </Button>
                            <Button
                              color="inherit"
                              size="small"
                              onClick={() => {
                                setIsSubmitRequested(false);
                                handleModalClose();
                              }}
                            >
                              CANCEL
                            </Button>
                          </div>
                        }
                      >
                        <AlertTitle>Do you want to submit the data?</AlertTitle>
                      </Alert>
                    )}
                  </div>
                </Modal>
                {inputFields.map((inputField, index) => (
                  <TableRow
                    hover
                    role="checkbox"
                    tabIndex={-1}
                    key={inputField.id}
                  >
                    <TableCell style={{ width: '10%' }}>
                      <TextValidator
                        name="rollNumber"
                        label="Roll Number"
                        variant="outlined"
                        disabled={inputField.disabled}
                        onChange={(event) => handleStudentChange(index, event)}
                        validators={['required']}
                        errorMessages={['this field is required']}
                        value={inputField.rollNumber}
                        style={{ width: '100%' }}
                      />
                    </TableCell>
                    <TableCell style={{ width: '10%' }}>
                      <TextValidator
                        name="studentName"
                        label="Student Name"
                        variant="outlined"
                        disabled={inputField.disabled}
                        onChange={(event) => handleStudentChange(index, event)}
                        validators={['required', 'isValidName']}
                        errorMessages={[
                          'this field is required',
                          'This is not a valid name',
                        ]}
                        value={inputField.studentName}
                        style={{ width: '100%' }}
                      />
                    </TableCell>
                    <TableCell style={{ width: '10%' }}>
                      <Autocomplete
                        id="gender-dropdown"
                        options={props.GENDER_LIST}
                        style={{ width: '100%' }}
                        onChange={handleAutoCompleteOnChange('gender', index)}
                        disabled={inputField.disabled}
                        renderInput={(params) => (
                          <TextValidator
                            {...params}
                            label="Gender"
                            variant="outlined"
                            validators={['required']}
                            errorMessages={['this field is required']}
                            value={inputField.gender}
                          />
                        )}
                      />
                    </TableCell>
                    <TableCell style={{ width: '10%' }}>
                      <Autocomplete
                        id="grade-dropdown"
                        options={props.GRADE_LIST}
                        style={{ width: '100%' }}
                        onChange={handleAutoCompleteOnChange('grade', index)}
                        disabled={inputField.disabled}
                        renderInput={(params) => (
                          <TextValidator
                            {...params}
                            label="Grade"
                            variant="outlined"
                            validators={['required']}
                            errorMessages={['this field is required']}
                            value={inputField.grade}
                          />
                        )}
                      />
                    </TableCell>
                    <TableCell style={{ width: '10%' }}>
                      <Autocomplete
                        id="section-dropdown"
                        options={props.SECTION_LIST}
                        style={{ width: '100%' }}
                        onChange={handleAutoCompleteOnChange('section', index)}
                        disabled={inputField.disabled}
                        renderInput={(params) => (
                          <TextValidator
                            {...params}
                            label="Section"
                            variant="outlined"
                            validators={['required']}
                            errorMessages={['this field is required']}
                            value={inputField.section}
                          />
                        )}
                      />
                    </TableCell>

                    <TableCell style={{ width: '15%' }}>
                      <IconButton
                        aria-label="check"
                        onClick={handleConfirmButton('disabled', index)}
                      >
                        <CheckIcon />
                      </IconButton>
                      <IconButton
                        aria-label="edit"
                        onClick={handleEditButton('disabled', index)}
                      >
                        <EditIcon />
                      </IconButton>
                      <IconButton
                        aria-label="delete"
                        onClick={() => onSummaryDelete(index)}
                      >
                        <DeleteIcon />
                      </IconButton>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Paper>
      </ValidatorForm>
    </div>
  );
}

function mapStateToProps(state) {
  return {
    GENDER_LIST: state.authUser.GENDER_LIST,
    GRADE_LIST: state.authUser.GRADE_LIST,
    SECTION_LIST: state.authUser.SECTION_LIST,
    currentUserSchoolID: state.authUser.userProfile.school.id,
  };
}

export default connect(mapStateToProps, {
  showNotification,
  showProgressBar,
  hideProgressBar,
})(studentAdd);
