import React from 'react';
import {
  Button,
  CircularProgress,
  Divider,
  MenuItem,
  Toolbar,
  Typography,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {
  ValidatorForm,
  TextValidator,
  SelectValidator,
} from 'react-material-ui-form-validator';
import PropTypes from 'prop-types';

import { validateName } from 'Utils/appShell';

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'relative',
    width: 800,
    height: '50%',
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2, 4, 3),
    justify: 'center',
  },
  formRoot: {
    '& .MuiTextField-root': {
      marginBottom: theme.spacing(1),
      width: '20ch',
    },
  },
}));

export default function StudentEdit(props) {
  const classes = useStyles();
  const [editdata, setEditdata] = React.useState({
    studentId: props.data.studentId,
    rollNumber: props.data.rollNumber,
    studentName: props.data.studentName,
    grade: props.data.grade,
    section: props.data.section,
    gender: props.data.gender,
  });

  const [submitted, setSubmitted] = React.useState(false);
  const handleChange = (name) => (e) => {
    const value = e.target.value;
    setEditdata({ ...editdata, [name]: value });
  };

  const onSubmitHandle = () => {
    setSubmitted(true);
    props.onSubmit(editdata);
  };
  const onCancel = () => {
    props.onCancel(true);
  };

  ValidatorForm.addValidationRule('isValidName', (value) =>
    validateName(value)
  );
  return (
    <div className={classes.paper}>
      <ValidatorForm onSubmit={onSubmitHandle}>
        <Typography variant="h5" align="center">
          Edit Student
        </Typography>
        <Divider className="contentMarginTop" />
        <div className="popupFormContent">
          <TextValidator
            label="RollNumber"
            onChange={handleChange('rollNumber')}
            name="rollNumber"
            value={editdata.rollNumber}
            validators={['required']}
            errorMessages={['this field is required']}
            fullWidth
            variant="outlined"
          />
          <br />
          <TextValidator
            label="Student Name"
            onChange={handleChange('studentName')}
            name="studentName"
            value={editdata.studentName}
            validators={['required', 'isValidName']}
            errorMessages={[
              'this field is required',
              'This is not a valid name',
            ]}
            variant="outlined"
            fullWidth
          />

          <br />
          <SelectValidator
            label="Gender"
            onChange={handleChange('gender')}
            name="gender"
            value={editdata.gender}
            validators={['required']}
            errorMessages={['this field is required']}
            variant="outlined"
            fullWidth
          >
            {props.genderOptions.map((gender, index) => (
              <MenuItem key={index} value={gender}>
                {gender}
              </MenuItem>
            ))}
          </SelectValidator>
          <br />
          <SelectValidator
            label="Grade"
            onChange={handleChange('grade')}
            name="grade"
            value={editdata.grade}
            validators={['required']}
            errorMessages={['this field is required']}
            fullWidth
            variant="outlined"
          >
            {props.gradeOptions.map((grade, index) => (
              <MenuItem key={index} value={grade}>
                {grade}
              </MenuItem>
            ))}
          </SelectValidator>
          <br />
          <SelectValidator
            label="Section"
            onChange={handleChange('section')}
            name="section"
            value={editdata.section}
            validators={['required']}
            errorMessages={['this field is required']}
            fullWidth
            variant="outlined"
          >
            {props.sectionOptions.map((section, index) => (
              <MenuItem key={index} value={section}>
                {section}
              </MenuItem>
            ))}
          </SelectValidator>
        </div>
        <Divider className="contentMarginTop" />
        <Toolbar disableGutters>
          <div className="flexGrow1" />
          <Button
            color="primary"
            variant="outlined"
            type="submit"
            onClick={onCancel}
            className="toolbarAction"
          >
            Cancel
          </Button>
          <Button
            color="primary"
            variant="contained"
            type="submit"
            className="toolbarAction"
            disabled={submitted}
            startIcon={submitted && <CircularProgress size={24} />}
          >
            {(submitted && 'Saving ...') || (!submitted && 'Submit')}
          </Button>
        </Toolbar>
      </ValidatorForm>
    </div>
  );
}

StudentEdit.propTypes = {
  data: PropTypes.shape({
    studentId: PropTypes.string.isRequired,
    rollNumber: PropTypes.string.isRequired,
    studentName: PropTypes.string.isRequired,
    section: PropTypes.string.isRequired,
    gender: PropTypes.string.isRequired,
    grade: PropTypes.string.isRequired,
  }),
  genderOptions: PropTypes.array.isRequired,
  gradeOptions: PropTypes.array.isRequired,
  sectionOptions: PropTypes.array.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
};
