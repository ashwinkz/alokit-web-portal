import React from 'react';
import {
  Button,
  Chip,
  CircularProgress,
  Divider,
  IconButton,
  InputAdornment,
  MenuItem,
  Toolbar,
  Typography,
} from '@material-ui/core';
import {
  CancelOutlined as CancelOutlinedIcon,
  CheckCircleOutline as CheckCircleOutlineIcon,
  PlaylistAdd as PlaylistAddIcon,
} from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import {
  ValidatorForm,
  TextValidator,
  SelectValidator,
} from 'react-material-ui-form-validator';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import { CREDENTIALS_DELIVERY_OPTIONS } from 'Utils/constants';

import {
  MOBILE_NUMBER_PREFIX,
  validatePhoneNumber,
  validateName,
} from 'Utils/appShell';

const useStyles = makeStyles((theme) => ({
  paper: {
    height: '50%',
    position: 'relative',
    width: '800px',
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2, 4, 3),
    justify: 'center',
  },
  formRoot: {
    '& .MuiTextField-root': {
      marginBottom: theme.spacing(1),
      width: '100%',
    },
  },
  chipList: {
    display: 'flex',
    justifyContent: 'left',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(0.5),
    },
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));
const useInputBaseStyles = makeStyles(
  {
    input: {
      minWidth: '100px',
    },
  },
  { name: 'MuiInputBase' }
);
export default function TeacherEntry(props) {
  const classes = useStyles();
  const inputBaseclasses = useInputBaseStyles();
  const [editdata, setEditdata] = React.useState({
    id: props.data.id,
    firstName: props.data.firstName,
    lastName: props.data.lastName,
    gender: props.data.gender,
    phoneNumber: props.data.phoneNumber,
    email: props.data.email,
    role: props.data.role,
  });

  const [gradeSecList, setGradeSecList] = React.useState([
    ...props.data.gradeSecList,
  ]);

  const [
    showGradeSelectionToolbar,
    setShowGradeSelectionToolbar,
  ] = React.useState(false);
  const [selectedGradeSec, setSelectedGradeSec] = React.useState({
    grade: '',
    section: '',
    subject: '',
  });
  // const [deletedGradSecIDS, setDeletedGradSecIDS] = React.useState([]);
  const [submitted, setSubmitted] = React.useState(false);
  const [selectedDeliveryMedium, setSelectedDeliveryMedium] = React.useState(
    CREDENTIALS_DELIVERY_OPTIONS.EMAIL
  );
  const handleDeliveryMediumChange = (e) => {
    setSelectedDeliveryMedium(e.target.value);
  };

  const handleValueChange = (name) => (e) => {
    const value = e.target.value;
    setEditdata({ ...editdata, [name]: value });
  };
  const handleGradeSectionChange = (name) => (e) => {
    const value = e.target.value;
    setSelectedGradeSec({ ...selectedGradeSec, [name]: value });
  };

  const handleShowGradeSelection = (show) => {
    setShowGradeSelectionToolbar(show);
  };

  const onSubmitHandle = () => {
    setSubmitted(true);
    props.onSubmit({ ...editdata, gradeSecList }, selectedDeliveryMedium);
  };
  const onAddGradeSection = () => {
    const ignoreSelection = gradeSecList.some(
      (item) =>
        item.grade === selectedGradeSec.grade &&
        item.section === selectedGradeSec.section &&
        item.subject === selectedGradeSec.subject
    );
    if (!ignoreSelection) {
      const newGradeSecList = [...gradeSecList, selectedGradeSec];
      setGradeSecList(newGradeSecList);
      setSelectedGradeSec({ grade: '', section: '', subject: '' });
    }
  };
  const onRemoveGradeSection = (grade, section, subject) => {
    setGradeSecList(
      gradeSecList.filter(
        (gs) =>
          !(
            gs.grade === grade &&
            gs.section === section &&
            gs.subject === subject
          )
      )
    );
  };

  ValidatorForm.addValidationRule('isPhoneNumber', (value) =>
    validatePhoneNumber(value)
  );
  ValidatorForm.addValidationRule('isValidName', (value) =>
    validateName(value)
  );

  const onCancel = () => {
    props.onCancel(true);
  };

  return (
    <div className={classes.paper}>
      <ValidatorForm onSubmit={onSubmitHandle} className={classes.formRoot}>
        <Typography variant="h5" align="center">
          {props.editMode ? 'Edit' : 'Add'} Teacher
        </Typography>
        <Divider className="contentMarginTop" />
        <div className="popupFormContent">
          <TextValidator
            label="First Name"
            onChange={handleValueChange('firstName')}
            name="firstName"
            value={editdata.firstName}
            validators={['required', 'isValidName']}
            errorMessages={[
              'this field is required',
              'This is not a valid name',
            ]}
            fullWidth
            variant="outlined"
          />
          <br />
          <TextValidator
            label="Last Name"
            onChange={handleValueChange('lastName')}
            name="lastName"
            value={editdata.lastName}
            validators={['required', 'isValidName']}
            errorMessages={[
              'this field is required',
              'This is not a valid name',
            ]}
            fullWidth
            variant="outlined"
          />
          <br />
          <TextValidator
            label="Phone Number"
            name="phoneNumber"
            value={editdata.phoneNumber}
            onChange={handleValueChange('phoneNumber')}
            validators={['required', 'isPhoneNumber']}
            errorMessages={[
              'This field is required',
              'Mobile number is not valid',
            ]}
            disabled={props.editMode}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  {MOBILE_NUMBER_PREFIX}
                </InputAdornment>
              ),
            }}
            fullWidth
            variant="outlined"
          />
          <br />
          <TextValidator
            label="Email ID"
            onChange={handleValueChange('email')}
            name="emailID"
            value={editdata.email}
            validators={['required', 'isEmail']}
            errorMessages={['this field is required', 'email is not valid']}
            disabled={props.editMode}
            fullWidth
            variant="outlined"
          />
          <br />
          {!props.editMode && (
            <>
              <SelectValidator
                label="Send login credentials through"
                onChange={handleDeliveryMediumChange}
                name="Delivery Medium"
                value={selectedDeliveryMedium}
                validators={['required']}
                errorMessages={['this field is required']}
                variant="outlined"
                fullWidth
              >
                {Object.keys(CREDENTIALS_DELIVERY_OPTIONS).map((medium) => (
                  <MenuItem
                    key={medium}
                    value={CREDENTIALS_DELIVERY_OPTIONS[medium]}
                  >
                    {CREDENTIALS_DELIVERY_OPTIONS[medium]}
                  </MenuItem>
                ))}
              </SelectValidator>

              <br />
            </>
          )}
          <SelectValidator
            label="Gender"
            onChange={handleValueChange('gender')}
            name="gender"
            value={editdata.gender}
            validators={['required']}
            errorMessages={['this field is required']}
            variant="outlined"
            fullWidth
          >
            {props.genderOptions.map((gender, index) => (
              <MenuItem key={index} value={gender}>
                {gender}
              </MenuItem>
            ))}
          </SelectValidator>
          <br />
          <SelectValidator
            label="Role"
            onChange={handleValueChange('role')}
            disabled={
              props.editMode && editdata.role === props.adminRoleToFreeze
            }
            name="role"
            value={editdata.role}
            validators={['required']}
            errorMessages={['this field is required']}
            variant="outlined"
            fullWidth
          >
            {props.roleOptions.map((role, index) => (
              <MenuItem key={index} value={role}>
                {role}
              </MenuItem>
            ))}
          </SelectValidator>
          <br />
          <Divider />
          <Typography color="textSecondary" display="block" variant="caption">
            Grades / Subjects assigned
          </Typography>
          <div className={classes.chipList}>
            {gradeSecList.map((gs, index) => (
              <Chip
                key={index}
                color="primary"
                variant="outlined"
                label={`${gs.grade} - ${gs.section} - ${gs.subject}`}
                onDelete={() =>
                  onRemoveGradeSection(gs.grade, gs.section, gs.subject)
                }
              />
            ))}
          </div>
          {showGradeSelectionToolbar ? (
            <Toolbar
              variant="dense"
              disableGutters
              className={clsx('fullWidth')}
            >
              <SelectValidator
                label="Grade"
                onChange={handleGradeSectionChange('grade')}
                name="Grade"
                value={selectedGradeSec.grade}
                fullWidth
                variant="outlined"
                classes={inputBaseclasses}
              >
                {props.gradeOptions.map((grade, index) => (
                  <MenuItem key={index} value={grade}>
                    {grade}
                  </MenuItem>
                ))}
              </SelectValidator>
              <SelectValidator
                label="Section"
                onChange={handleGradeSectionChange('section')}
                name="Section"
                value={selectedGradeSec.section}
                fullWidth
                variant="outlined"
                classes={inputBaseclasses}
              >
                {props.sectionOptions.map((section, index) => (
                  <MenuItem key={index} value={section}>
                    {section}
                  </MenuItem>
                ))}
              </SelectValidator>
              <SelectValidator
                label="Subject"
                onChange={handleGradeSectionChange('subject')}
                name="Subject"
                value={selectedGradeSec.subject}
                fullWidth
                variant="outlined"
                classes={inputBaseclasses}
              >
                {props.subjectOptions.map((subject, index) => (
                  <MenuItem key={index} value={subject}>
                    {subject}
                  </MenuItem>
                ))}
              </SelectValidator>
              <IconButton
                color="primary"
                disabled={
                  !(
                    selectedGradeSec.grade &&
                    selectedGradeSec.section &&
                    selectedGradeSec.subject
                  )
                }
                onClick={onAddGradeSection}
              >
                <CheckCircleOutlineIcon />
              </IconButton>
              <IconButton
                color="secondary"
                onClick={() => handleShowGradeSelection(false)}
              >
                <CancelOutlinedIcon />
              </IconButton>
            </Toolbar>
          ) : (
            <Button
              variant="contained"
              disableElevation
              startIcon={<PlaylistAddIcon />}
              onClick={() => handleShowGradeSelection(true)}
            >
              Add Grades
            </Button>
          )}
        </div>
        <Divider className="contentMarginTop" />
        <Toolbar disableGutters>
          <dir className="flexGrow1" />
          <Button
            color="primary"
            variant="outlined"
            type="submit"
            className={clsx('toolbarAction')}
            onClick={onCancel}
          >
            Cancel
          </Button>
          <Button
            color="primary"
            variant="contained"
            type="submit"
            disabled={submitted}
            className={clsx('toolbarAction')}
            startIcon={submitted && <CircularProgress size={24} />}
          >
            {(submitted && 'Saving ...') || (!submitted && 'Submit')}
          </Button>
        </Toolbar>
      </ValidatorForm>
    </div>
  );
}

TeacherEntry.propTypes = {
  data: PropTypes.shape({
    id: PropTypes.string,
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    phoneNumber: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    gender: PropTypes.string.isRequired,
    role: PropTypes.string.isRequired,
    gradeSecList: PropTypes.arrayOf(PropTypes.object),
  }),
  editMode: PropTypes.bool.isRequired,
  adminRoleToFreeze: PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  genderOptions: PropTypes.array.isRequired,
  roleOptions: PropTypes.array.isRequired,
  gradeOptions: PropTypes.array.isRequired,
  sectionOptions: PropTypes.array.isRequired,
  subjectOptions: PropTypes.array.isRequired,
};
