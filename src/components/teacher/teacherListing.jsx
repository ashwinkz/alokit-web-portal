import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Button,
  Chip,
  Fade,
  IconButton,
  ListItemText,
  Menu,
  MenuItem,
  Modal,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Toolbar,
  Tooltip,
  Typography,
  TablePagination,
} from '@material-ui/core';
import { Add as AddIcon, MoreVert as MoreVertIcon } from '@material-ui/icons';
import { connect } from 'react-redux';

import SearchBar from 'Components/search/index';
import { MOBILE_NUMBER_PREFIX } from 'Utils/appShell';
import TeacherEntry from './TeacherEntry';
import { fetchTeachers, addTeacher, editTeacher } from 'Actions/teacher';
import { NOTIFICATION_SEVERITY } from 'Components/notify/index';
import {
  showNotification,
  showProgressBar,
  hideProgressBar,
} from 'Actions/appShell';
import { AUTH_ROLES, DEFAULT_PAGE_SIZE } from 'Utils/constants';

import DataNotFound from 'Components/utils/DataNotFound';
import { modifySchoolUserAccess } from 'Actions/teacher';

const useStyles = makeStyles((theme) => ({
  container: {
    maxHeight: 440,
  },
  button: {
    backgroundColor: '#89bcff',
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  chip: {
    margin: theme.spacing(0.5),
  },
}));

function TeacherListing(props) {
  const classes = useStyles();
  const [teacherData, setTeacherData] = React.useState([]);
  const [filteredTeacherData, setFilteredTeacherData] = React.useState([]);
  const TeacherListHeader = [
    { id: 'S. No.', label: 'S. No.' },
    { id: 'First Name', label: 'First Name' },
    { id: 'Last Name', label: 'Last Name' },
    { id: 'Role', label: 'Role' },
    { id: 'Grades / Subjects assigned', label: 'Grades assigned' },
    { id: 'Actions', label: 'Actions' },
  ];
  const [page, setPage] = React.useState(0);
  const [searchValue, setSearchValue] = React.useState('');
  const [rowsPerPage, setRowsPerPage] = React.useState(DEFAULT_PAGE_SIZE);
  const [selectedTeacherId, setSelectedTeacherId] = React.useState(null);
  const [optionsMenuAnchor, setOptionsMenuAnchor] = React.useState(null);
  const [formOpen, setFormOpen] = React.useState(false);
  const [editFlag, setEditFlag] = React.useState(false);
  const [fetchTeacherData, setFetchTeacherData] = React.useState(false);
  const [formData, setFormData] = React.useState({
    id: '',
    firstName: '',
    lastName: '',
    phoneNumber: '',
    email: '',
    role: '',
    gender: '',
    gradeSecList: [],
  });
  // const [deleteMessage, setDeleteMessage] = React.useState('');
  // const [deleteFlag, setDeleteFlag] = React.useState(false);
  // const [deleteData, setDeleteData] = React.useState(0);

  const mapTeacherRecord = (t) => ({
    id: t.cognitoUsername,
    firstName: t.firstName,
    lastName: t.lastName,
    phoneNumber: t.phoneNumber,
    email: t.email,
    role: t.role,
    gender: t.gender,
    disabled: t.disabled,
    gradeSecList: t.gradeSecList.items.map((gs) => ({
      id: gs.id,
      grade: gs.grade,
      section: gs.section,
      subject: gs.subject,
    })),
  });
  React.useEffect(() => {
    if (props.currentUserProfileLoaded) setFetchTeacherData(true);
  }, [props.currentUserProfileLoaded]);
  React.useEffect(async () => {
    if (fetchTeacherData) {
      console.log('loading teacher data from backend');
      const fetchedData = await fetchTeachers(props.currentUserSchoolId);
      setFetchTeacherData(false);
      const formattedTeachersData = fetchedData.map((t) => mapTeacherRecord(t));
      setTeacherData(formattedTeachersData);
      setFilteredTeacherData(formattedTeachersData);
    }
  }, [fetchTeacherData]);

  const onTeacherEdit = (row) => {
    setFormOpen(true);
    setEditFlag(true);
    setFormData({
      id: row.id,
      firstName: row.firstName,
      lastName: row.lastName,
      phoneNumber: row.phoneNumber.replace(MOBILE_NUMBER_PREFIX, ''),
      email: row.email,
      role: row.role,
      gender: row.gender,
      gradeSecList: row.gradeSecList,
    });
  };
  const onTeacherAdd = () => {
    setFormOpen(true);
    setEditFlag(false);
    setFormData({
      firstName: '',
      lastName: '',
      phoneNumber: '',
      email: '',
      role: '',
      gender: '',
      gradeSecList: [],
    });
  };

  const handleOpenOptionsMenu = (e, selectedTeacherId) => {
    setSelectedTeacherId(selectedTeacherId);
    setOptionsMenuAnchor(e.currentTarget);
  };

  const handleCloseOptionsMenu = () => {
    setSelectedTeacherId(null);
    setOptionsMenuAnchor(null);
  };

  const handleModalClose = () => {
    setFormOpen(false);
  };

  const handleModifyAccessAction = async (teacherId, disableUser) => {
    handleCloseOptionsMenu();
    props.showProgressBar();
    const actionResponse = await modifySchoolUserAccess(teacherId, disableUser);

    // display action result
    props.showNotification(
      actionResponse.message,
      actionResponse.success
        ? NOTIFICATION_SEVERITY.SUCCESS
        : NOTIFICATION_SEVERITY.ERROR
    );
    props.hideProgressBar();

    if (actionResponse.success) {
      // refresh teacher listing
      setFetchTeacherData(true);
    }
  };

  const handleTeacherSubmission = async (teacherDetails, deliveryMedium) => {
    let actionResponse = {};
    props.showProgressBar();
    if (!editFlag) {
      actionResponse = await addTeacher(
        props.currentUserSchoolId,
        teacherDetails.firstName,
        teacherDetails.lastName,
        MOBILE_NUMBER_PREFIX.concat(teacherDetails.phoneNumber),
        teacherDetails.email,
        teacherDetails.gender,
        teacherDetails.role,
        teacherDetails.gradeSecList,
        deliveryMedium
      );
    } else {
      // only name/branch/address can be updated
      actionResponse = await editTeacher(
        teacherDetails.id,
        teacherDetails.firstName,
        teacherDetails.lastName,
        teacherDetails.gender,
        teacherDetails.role,
        teacherDetails.gradeSecList
      );
    }

    // display action result
    props.showNotification(
      actionResponse.message,
      actionResponse.success
        ? NOTIFICATION_SEVERITY.SUCCESS
        : NOTIFICATION_SEVERITY.ERROR
    );
    props.hideProgressBar();
    handleModalClose();

    if (actionResponse.success) {
      // refresh teacher listing
      setFetchTeacherData(true);
    } else {
      // re-open form with entered data
      setFormData(teacherDetails);
      setFormOpen(true);
    }
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const filterTeacherList = () => {
    console.log('filterTeacherList', teacherData);
    const newFilteredData = searchValue
      ? teacherData.filter(
          (record) =>
            record['firstName']
              .toLowerCase()
              .includes(searchValue.toLowerCase()) ||
            record['lastName'].toLowerCase().includes(searchValue.toLowerCase())
        )
      : teacherData;
    setFilteredTeacherData(newFilteredData);
  };
  React.useEffect(() => {
    filterTeacherList();
  }, [searchValue]);

  return (
    <div>
      <Toolbar className={'pageToolbar'}>
        <Typography variant="h6">Manage Teachers</Typography>
        <div className={'flexGrow1'} />
        <SearchBar
          value={searchValue}
          onChange={(newSearchValue) => setSearchValue(newSearchValue)}
          onCancelSearch={() => setSearchValue('')}
          className={'pageToolbarAction'}
        />
        <Button
          color="primary"
          variant="contained"
          startIcon={<AddIcon />}
          onClick={onTeacherAdd}
          className={'pageToolbarAction'}
        >
          Add Teacher
        </Button>
      </Toolbar>
      <Paper>
        <Modal
          open={formOpen}
          onclose={(event, reason) => {
            if (reason === 'escapeKeyDown') handleModalClose();
          }}
          disableEnforceFocus
          aria-labelledby="Teachers data operations modal"
          aria-describedby="Teachers data add/edit/delete modal"
          className={classes.modal}
        >
          <div>
            <TeacherEntry
              data={formData}
              editMode={editFlag}
              adminRoleToFreeze={AUTH_ROLES.SCHOOL_ADMIN}
              genderOptions={props.GENDER_LIST}
              gradeOptions={props.GRADE_LIST}
              sectionOptions={props.SECTION_LIST}
              subjectOptions={props.SUBJECT_LIST}
              roleOptions={props.SCHOOL_USER_ROLE_LIST}
              onCancel={handleModalClose}
              onSubmit={handleTeacherSubmission}
            />
          </div>
        </Modal>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="Teachers list table">
            <TableHead>
              <TableRow key="Teachers list header">
                {TeacherListHeader.map((column) => (
                  <TableCell key={column.id}>{column.label}</TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {(teacherData === undefined || teacherData.length === 0) && (
                <DataNotFound />
              )}
              {teacherData !== undefined &&
                filteredTeacherData
                  .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((slicedRow, index) => {
                    return (
                      <>
                        <TableRow
                          hover
                          role="checkbox"
                          tabIndex={-1}
                          key={slicedRow.id}
                        >
                          <TableCell>
                            {`${page * rowsPerPage + index + 1}.`}
                          </TableCell>
                          <TableCell>{slicedRow.firstName}</TableCell>
                          <TableCell>{slicedRow.lastName}</TableCell>
                          <TableCell>{slicedRow.role}</TableCell>

                          <TableCell>
                            {slicedRow.gradeSecList.map((gs, index) => (
                              <Chip
                                key={index}
                                color="primary"
                                variant="outlined"
                                label={`${gs.grade} - ${gs.section} - ${gs.subject}`}
                                className={classes.chip}
                              />
                            ))}
                          </TableCell>

                          <TableCell>
                            <Tooltip
                              title={
                                'Edit ' +
                                slicedRow.firstName +
                                ' ' +
                                slicedRow.lastName
                              }
                            >
                              <Button
                                color="secondary"
                                variant="contained"
                                aria-label="edit"
                                onClick={() => onTeacherEdit(slicedRow)}
                                disableElevation
                                disabled={slicedRow.disabled}
                              >
                                Edit
                              </Button>
                            </Tooltip>
                            {slicedRow.role !== AUTH_ROLES.SCHOOL_ADMIN && (
                              <Tooltip title={'more options'}>
                                <IconButton
                                  aria-label="more"
                                  aria-controls="school-user-menu"
                                  aria-haspopup="true"
                                  onClick={(e) =>
                                    handleOpenOptionsMenu(e, slicedRow.id)
                                  }
                                >
                                  <MoreVertIcon />
                                </IconButton>
                              </Tooltip>
                            )}
                            {slicedRow.id === selectedTeacherId && (
                              <Menu
                                id="school-user-menu"
                                anchorEl={optionsMenuAnchor}
                                open={Boolean(optionsMenuAnchor)}
                                onClose={handleCloseOptionsMenu}
                                TransitionComponent={Fade}
                              >
                                <MenuItem
                                  onClick={() =>
                                    handleModifyAccessAction(
                                      slicedRow.id,
                                      !slicedRow.disabled
                                    )
                                  }
                                >
                                  <ListItemText
                                    primary={
                                      slicedRow.disabled
                                        ? 'Re-enable Access'
                                        : 'Remove Access'
                                    }
                                  />
                                </MenuItem>
                              </Menu>
                            )}
                          </TableCell>
                        </TableRow>
                      </>
                    );
                  })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 15]}
          component="div"
          count={teacherData !== undefined ? filteredTeacherData.length : 0}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
}

function mapStateToProps(state) {
  return {
    SCHOOL_USER_ROLE_LIST: state.authUser.SCHOOL_USER_ROLE_LIST,
    GENDER_LIST: state.authUser.GENDER_LIST,
    GRADE_LIST: state.authUser.GRADE_LIST,
    SECTION_LIST: state.authUser.SECTION_LIST,
    SUBJECT_LIST: state.authUser.SUBJECT_LIST,
    currentUserSchoolId: state.authUser.userProfile.school.id,
    currentUserProfileLoaded: state.authUser.userProfileLoaded,
  };
}

export default connect(mapStateToProps, {
  showNotification,
  showProgressBar,
  hideProgressBar,
})(TeacherListing);
