import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Button,
  CircularProgress,
  Grow,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Paper,
  Toolbar,
} from '@material-ui/core';
import { FormatListBulleted as FormatListBulletedIcon } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';

import ReportDisplay, { TableLegendPopover } from './reportDisplay';
import { showProgressBar, hideProgressBar } from 'Actions/appShell';
import { DEFAULT_PAGE_SIZE } from 'Utils/constants';
import SchoolDashBoard from './schoolDashBoard';

const useStyles = makeStyles((theme) => ({
  root: {
    width: 'max-content',
  },
  infoBg: {
    backgroundColor: theme.palette.info.light,
  },
}));

function AlokitDashboard(props) {
  const classes = useStyles();

  const currMonth = new Date();
  currMonth.setDate(1);
  const lastMonth = new Date();
  lastMonth.setMonth(currMonth.getMonth() - 1);
  const prevMonth = new Date();
  prevMonth.setMonth(currMonth.getMonth() - 2);
  const lastMonthStr = lastMonth.toLocaleString('en-us', {
    month: 'long',
    year: 'numeric',
  });

  const [
    allSchoolPerformanceData,
    setAllSchoolPerformanceData,
  ] = React.useState([]);
  const [selectedSchoolId, setSelectedSchoolId] = React.useState(null);
  const [displaySchoolReport, setDisplaySchoolReport] = React.useState(false);

  React.useEffect(() => {
    if (props.dashboardReportsLoaded) {
      const allSchoolsData = Object.keys(props.dashboardReports).map((k) => {
        const schoolData =
          props.dashboardReports[k].displayData.schoolPerformanceData;
        const reportPeriods =
          props.dashboardReports[k].displayData.reportPeriods;
        return {
          id: props.dashboardReports[k].schoolId,
          ...schoolData,
          reportPeriods,
        };
      });
      setAllSchoolPerformanceData(allSchoolsData);
    }
  }, [props.dashboardReportsLoaded]);

  const [legendPopoverAnchorEl, setLegendPopoverAnchorEl] = React.useState(
    null
  );
  const openLegend = Boolean(legendPopoverAnchorEl);
  const handleOpenLegend = (event) => {
    setLegendPopoverAnchorEl(event.currentTarget);
  };
  const handleCloseLegend = () => {
    setLegendPopoverAnchorEl(null);
  };
  const handleOpenSchoolReport = (schoolId) => {
    setSelectedSchoolId(schoolId);
    setDisplaySchoolReport(true);
  };
  const handleCloseSchoolReport = () => {
    setSelectedSchoolId(null);
    setDisplaySchoolReport(false);
  };

  return (
    <div className={classes.root}>
      {displaySchoolReport ? (
        <SchoolDashBoard
          schoolId={selectedSchoolId}
          SUBJECT_LIST={props.SUBJECT_LIST}
          onBackNav={handleCloseSchoolReport}
          dashboardReportsLoaded={props.dashboardReportsLoaded}
          dashboardReports={props.dashboardReports}
        />
      ) : (
        <>
          {!props.dashboardReportsLoaded && (
            <List>
              <ListItem className={classes.infoBg} key="1">
                <ListItemIcon>
                  <CircularProgress size={24} />
                </ListItemIcon>
                <ListItemText primary="Loading reports" />
              </ListItem>
            </List>
          )}
          {props.dashboardReportsLoaded && (
            <Paper elevation={0} TransitionComponent={Grow}>
              <Toolbar
                variant="dense"
                disableGutters
                className={'contentMarginBottom'}
              >
                <Button
                  variant="outlined"
                  className={'noTextTransform'}
                  startIcon={<FormatListBulletedIcon />}
                  color="primary"
                  onClick={handleOpenLegend}
                >
                  View Legend
                </Button>
                <div className={'flexGrow1'} />
                <TableLegendPopover
                  open={openLegend}
                  anchorEl={legendPopoverAnchorEl}
                  handleClose={handleCloseLegend}
                />
              </Toolbar>

              <ReportDisplay
                title={`Schools Performance - ${lastMonthStr}`}
                data={allSchoolPerformanceData}
                subjectList={props.SUBJECT_LIST}
                pageSize={DEFAULT_PAGE_SIZE}
                rowTitleHeader={'School'}
                onRowTitleClick={handleOpenSchoolReport}
                paginateData
              />
            </Paper>
          )}
          <br />
        </>
      )}
    </div>
  );
}

AlokitDashboard.propTypes = {
  SUBJECT_LIST: PropTypes.array,
  showProgressBar: PropTypes.func.isRequired,
  hideProgressBar: PropTypes.func.isRequired,
  dashboardReportsLoaded: PropTypes.bool.isRequired,
  dashboardReports: PropTypes.object,
};

export default connect(null, {
  showProgressBar,
  hideProgressBar,
})(AlokitDashboard);
