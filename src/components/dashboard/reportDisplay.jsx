import React from 'react';
import PropTypes from 'prop-types';
import {
  Divider,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Paper,
  Popover,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  Toolbar,
  Tooltip,
  Typography,
} from '@material-ui/core';
import { Stop as StopIcon } from '@material-ui/icons';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import {
  LATEST_REPORT_LABEL,
  PREVIOUS_REPORT_LABEL,
  BASELINE_REPORT_LABEL,
  SCORE_LABEL,
  STUDENT_COUNT_LABEL,
} from 'Actions/reports';

const CELL_WIDTH = 180;

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100%',
    width: 'max-content',
    minWidth: 6.4 * CELL_WIDTH,
  },
  redFG: {
    color: theme.palette.error.light,
  },
  yellowFG: {
    color: theme.palette.warning.light,
  },
  greenFG: {
    color: theme.palette.success.light,
  },
  redBG: {
    backgroundColor: theme.palette.error.light,
  },
  yellowBG: {
    backgroundColor: theme.palette.warning.light,
  },
  greenBG: {
    backgroundColor: theme.palette.success.light,
  },
  reportTitleCell: {
    padding: 0,
    width: 6.4 * CELL_WIDTH,
  },
  rowTitleCell: {
    width: 1.4 * CELL_WIDTH,
  },
  labelCell: {
    width: CELL_WIDTH,
  },
  scoreCell: {
    width: 0.9 * CELL_WIDTH,
  },
}));

export const TableLegendPopover = (props) => {
  const classes = useStyles();
  return (
    <Popover
      open={props.open}
      anchorEl={props.anchorEl}
      onClose={props.handleClose}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'left',
      }}
    >
      <List>
        <ListItem>
          <ListItemIcon>
            <StopIcon className={classes.greenFG} fontSize="small" />
          </ListItemIcon>
          <ListItemText>
            <Typography variant="caption">
              Score improved over Baseline
            </Typography>
          </ListItemText>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemIcon>
            <StopIcon className={classes.yellowFG} fontSize="small" />
          </ListItemIcon>
          <ListItemText>
            <Typography variant="caption">
              Score same as Baseline / cannot compare
            </Typography>
          </ListItemText>
        </ListItem>
        <Divider />
        <ListItem>
          <ListItemIcon>
            <StopIcon className={classes.redFG} fontSize="small" />
          </ListItemIcon>
          <ListItemText>
            <Typography variant="caption">Score below Baseline</Typography>
          </ListItemText>
        </ListItem>
      </List>
    </Popover>
  );
};

const StyledTableCell = withStyles((theme) => ({
  root: {
    '&:last-child': {
      paddingRight: 0,
    },
  },
  head: {
    backgroundColor: theme.palette.common.white,
    color: theme.palette.common.black,
    borderRightColor: theme.palette.action.hover,
    borderRightSize: 1,
    borderRightStyle: 'solid',
  },
  body: {
    fontSize: 14,
    borderRightColor: theme.palette.action.hover,
    borderRightSize: 1,
    borderRightStyle: 'solid',
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

function ReportDisplay(props) {
  const classes = useStyles();
  const [currentPage, setCurrentPage] = React.useState(0);
  const [pageSize, setPageSize] = React.useState(props.pageSize);

  const handleChangePage = (event, newPage) => {
    setCurrentPage(newPage);
  };
  const handleChangePageSize = (event) => {
    setPageSize(+event.target.value);
    setCurrentPage(0);
  };
  const getClassForCellBackground = (currVal, baselineVal) => {
    const v1 = currVal || baselineVal || 0;
    const v2 = baselineVal || currVal || 0;
    if (v1 > v2) return classes.greenBG;
    else if (v1 < v2) return classes.redBG;
    return classes.yellowBG;
  };

  return (
    <Paper className={clsx(classes.root, 'contentMarginBottom')} elevation={0}>
      <TableContainer>
        <Table stickyHeader aria-label="report data" size="small">
          <TableHead>
            <TableRow key="report title">
              <StyledTableCell
                key={1}
                colSpan={3 + props.subjectList.length}
                className={classes.reportTitleCell}
              >
                <Toolbar className={'contentToolbar'}>
                  <Typography variant="h6">{props.title}</Typography>
                  <div className={'flexGrow1'} />
                </Toolbar>
              </StyledTableCell>
            </TableRow>
            <TableRow key="header cols">
              <StyledTableCell key={1} className={classes.labelCell}>
                {props.rowTitleHeader}
              </StyledTableCell>
              <StyledTableCell key={2} className={classes.labelCell} />
              <StyledTableCell key={3} className={classes.labelCell} />
              {props.subjectList.map((s, i) => (
                <StyledTableCell key={4 + i} className={classes.scoreCell}>
                  {s}
                </StyledTableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {props.data.length <= 0 && (
              <StyledTableRow>
                <StyledTableCell
                  colSpan={3 + props.subjectList.length}
                  className={classes.labelCell}
                >
                  No data to display
                </StyledTableCell>
              </StyledTableRow>
            )}
            {props.data
              .slice(currentPage * pageSize, (currentPage + 1) * pageSize)
              .map((record) => {
                let rowTitle = (
                  <ListItem
                    button={props.onRowTitleClick}
                    onClick={() => {
                      props.onRowTitleClick(record.id);
                    }}
                  >
                    <ListItemText
                      primary={record.label}
                      secondary={record.subLabel}
                    />
                  </ListItem>
                );
                if (props.onRowTitleClick) {
                  rowTitle = (
                    <Tooltip
                      title={'click to open detailed report'}
                      placement="bottom"
                      arrow
                    >
                      {rowTitle}
                    </Tooltip>
                  );
                }
                return record.data ? (
                  [
                    <StyledTableRow>
                      <StyledTableCell
                        rowSpan={4}
                        className={classes.rowTitleCell}
                      >
                        {rowTitle}
                      </StyledTableCell>
                      <StyledTableCell className={classes.labelCell}>
                        No. of Students
                      </StyledTableCell>
                      <StyledTableCell className={classes.labelCell}>
                        <Typography variant="caption" className={'italicText'}>
                          {record.reportPeriods[LATEST_REPORT_LABEL] || '-'}
                        </Typography>
                      </StyledTableCell>
                      {props.subjectList.map((subject) => (
                        <StyledTableCell>
                          {record.data[subject][LATEST_REPORT_LABEL][
                            STUDENT_COUNT_LABEL
                          ] || '-'}
                        </StyledTableCell>
                      ))}
                    </StyledTableRow>,
                    <StyledTableRow className={classes.labelCell}>
                      <StyledTableCell>Latest Average</StyledTableCell>
                      <StyledTableCell className={classes.labelCell}>
                        <Typography variant="caption" className={'italicText'}>
                          {record.reportPeriods[LATEST_REPORT_LABEL] || '-'}
                        </Typography>
                      </StyledTableCell>
                      {props.subjectList.map((subject) => (
                        <StyledTableCell
                          className={clsx(
                            classes.scoreCell,
                            getClassForCellBackground(
                              record.data[subject][LATEST_REPORT_LABEL][
                                SCORE_LABEL
                              ],
                              record.data[subject][BASELINE_REPORT_LABEL][
                                SCORE_LABEL
                              ]
                            )
                          )}
                        >
                          {record.data[subject][LATEST_REPORT_LABEL][
                            SCORE_LABEL
                          ] || '-'}
                        </StyledTableCell>
                      ))}
                    </StyledTableRow>,
                    <StyledTableRow>
                      <StyledTableCell>Previous Average</StyledTableCell>
                      <StyledTableCell className={classes.labelCell}>
                        <Typography variant="caption" className={'italicText'}>
                          {record.reportPeriods[PREVIOUS_REPORT_LABEL] || '-'}
                        </Typography>
                      </StyledTableCell>
                      {props.subjectList.map((subject) => (
                        <StyledTableCell>
                          {record.data[subject][PREVIOUS_REPORT_LABEL][
                            SCORE_LABEL
                          ] || '-'}
                        </StyledTableCell>
                      ))}
                    </StyledTableRow>,
                    <StyledTableRow>
                      <StyledTableCell>Baseline Average</StyledTableCell>
                      <StyledTableCell className={classes.labelCell}>
                        <Typography variant="caption" className={'italicText'}>
                          {record.reportPeriods[BASELINE_REPORT_LABEL] || '-'}
                        </Typography>
                      </StyledTableCell>
                      {props.subjectList.map((subject) => (
                        <StyledTableCell>
                          {record.data[subject][BASELINE_REPORT_LABEL][
                            SCORE_LABEL
                          ] || '-'}
                        </StyledTableCell>
                      ))}
                    </StyledTableRow>,
                  ]
                ) : (
                  <StyledTableRow>
                    <StyledTableCell className={classes.rowTitleCell}>
                      {rowTitle}
                    </StyledTableCell>
                    <StyledTableCell
                      colSpan={2 + props.subjectList.length}
                      className={classes.labelCell}
                    >
                      No data to display
                    </StyledTableCell>
                  </StyledTableRow>
                );
              })}
          </TableBody>
        </Table>
        {props.paginateData && (
          <TablePagination
            rowsPerPageOptions={[10, 25, 50]}
            component="div"
            count={props.data.length}
            rowsPerPage={pageSize}
            page={currentPage}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangePageSize}
          />
        )}
      </TableContainer>
    </Paper>
  );
}

ReportDisplay.propTypes = {
  title: PropTypes.string.isRequired,
  data: PropTypes.array.isRequired,
  paginateData: PropTypes.bool,
  pageSize: PropTypes.number,
  subjectList: PropTypes.array.isRequired,
  rowTitleHeader: PropTypes.string.isRequired,
  onRowTitleClick: PropTypes.func,
};

export default ReportDisplay;
