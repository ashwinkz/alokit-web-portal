import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Button,
  CircularProgress,
  Grow,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Paper,
  Toolbar,
} from '@material-ui/core';
import {
  FormatListBulleted as FormatListBulletedIcon,
  NavigateBeforeRounded as NavigateBeforeIcon,
} from '@material-ui/icons';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';

import ReportDisplay, { TableLegendPopover } from './reportDisplay';
import { generateLatestReport } from 'Actions/reports';
import { DEFAULT_PAGE_SIZE } from 'Utils/constants';
import { NOTIFICATION_SEVERITY } from 'Components/notify/index';
import { showNotification } from 'Actions/appShell';

const useStyles = makeStyles((theme) => ({
  root: {
    width: 'max-content',
  },
  infoBg: {
    backgroundColor: theme.palette.info.light,
  },
}));

function SchoolDashBoard(props) {
  const classes = useStyles();

  const currMonth = new Date();
  currMonth.setDate(1);
  const lastMonth = new Date();
  lastMonth.setMonth(currMonth.getMonth() - 1);
  const prevMonth = new Date();
  prevMonth.setMonth(currMonth.getMonth() - 2);
  const lastMonthStr = lastMonth.toLocaleString('en-us', {
    month: 'long',
    year: 'numeric',
  });

  const [lastMonthReportExists, setLastMonthReportExists] = React.useState(
    true
  );
  const [
    lastMonthReportDataExists,
    setLastMonthReportDataExists,
  ] = React.useState(true);
  const [
    reportGenerationRequested,
    setReportGenerationRequested,
  ] = React.useState(false);
  const [schoolPerformanceData, setSchoolPerformanceData] = React.useState([]);
  const [gradePerformanceData, setGradePerformanceData] = React.useState([]);

  React.useEffect(() => {
    if (props.dashboardReportsLoaded) {
      const reports = props.dashboardReports[props.schoolId];
      // check if latest month's report is not available
      if (!reports.actualReports.latestMonthReport)
        setLastMonthReportExists(false);
      // check if latest month's report is available but has no aggregated data
      if (
        reports.actualReports.latestMonthReport &&
        !reports.actualReports.latestReport.schoolAverages.items.length &&
        !reports.actualReports.latestReport.gradeAverages.items.length
      )
        setLastMonthReportDataExists(false);
      const schoolPerformanceData = reports.displayData.schoolPerformanceData;
      setSchoolPerformanceData(
        schoolPerformanceData.data
          ? [
              {
                ...schoolPerformanceData,
                reportPeriods: reports.displayData.reportPeriods,
              },
            ]
          : []
      );
      setGradePerformanceData(
        reports.displayData.gradePerformanceData.map((gd) => ({
          ...gd,
          reportPeriods: reports.displayData.reportPeriods,
        }))
      );
    }
  }, [props.dashboardReportsLoaded]);

  const requestLatestReportGeneration = async () => {
    setReportGenerationRequested(true);
    // call action to generate last month's report
    // report reload happens automatically
    let actionResponse = await props.generateLatestReport(
      props.schoolId,
      props.SUBJECT_LIST
    );
    // notify report generation status
    props.showNotification(
      actionResponse.message,
      actionResponse.success
        ? NOTIFICATION_SEVERITY.SUCCESS
        : NOTIFICATION_SEVERITY.ERROR
    );
    // reset state
    setReportGenerationRequested(false);
    if (actionResponse.success) {
      setLastMonthReportExists(true);
      setLastMonthReportDataExists(true);
    }
  };

  const [legendPopoverAnchorEl, setLegendPopoverAnchorEl] = React.useState(
    null
  );
  const openLegend = Boolean(legendPopoverAnchorEl);
  const handleOpenLegend = (event) => {
    setLegendPopoverAnchorEl(event.currentTarget);
  };
  const handleCloseLegend = () => {
    setLegendPopoverAnchorEl(null);
  };

  return (
    <div className={classes.root}>
      {!props.dashboardReportsLoaded && (
        <List>
          <ListItem className={classes.infoBg} key="1">
            <ListItemIcon>
              <CircularProgress size={24} />
            </ListItemIcon>
            <ListItemText primary="Loading reports" />
          </ListItem>
        </List>
      )}
      {!(lastMonthReportExists && lastMonthReportDataExists) && (
        <List>
          <ListItem className={classes.infoBg} key="2">
            <ListItemText
              primary={`Report data for the month ${lastMonthStr} is not available. Click here to generate it.`}
            />
          </ListItem>
          <ListItem className={classes.infoBg} key="3">
            <ListItemIcon>
              <Button
                variant="contained"
                disabled={reportGenerationRequested}
                onClick={requestLatestReportGeneration}
                startIcon={
                  reportGenerationRequested && <CircularProgress size={24} />
                }
              >
                {reportGenerationRequested
                  ? 'Generating Report'
                  : 'Generate Report'}
              </Button>
            </ListItemIcon>
          </ListItem>
        </List>
      )}
      {props.dashboardReportsLoaded && (
        <Paper elevation={0} TransitionComponent={Grow}>
          <Toolbar
            variant="dense"
            disableGutters
            className={'contentMarginBottom'}
          >
            {props.onBackNav && (
              <Button
                variant="outlined"
                className={'noTextTransform'}
                startIcon={<NavigateBeforeIcon />}
                color="primary"
                onClick={props.onBackNav}
              >
                Back
              </Button>
            )}
            <Button
              variant="outlined"
              className={clsx(
                'noTextTransform',
                props.onBackNav ? 'toolbarAction' : ''
              )}
              startIcon={<FormatListBulletedIcon />}
              color="primary"
              onClick={handleOpenLegend}
            >
              View Legend
            </Button>
            <div className={'flexGrow1'} />
            <TableLegendPopover
              open={openLegend}
              anchorEl={legendPopoverAnchorEl}
              handleClose={handleCloseLegend}
            />
          </Toolbar>
          <ReportDisplay
            title={`School Level Performance - ${lastMonthStr}`}
            data={schoolPerformanceData}
            subjectList={props.SUBJECT_LIST}
            pageSize={DEFAULT_PAGE_SIZE}
            rowTitleHeader={'School'}
          />
          <br />
          <ReportDisplay
            title={`Grade Level Performance - ${lastMonthStr}`}
            data={gradePerformanceData}
            subjectList={props.SUBJECT_LIST}
            pageSize={DEFAULT_PAGE_SIZE}
            rowTitleHeader={'Grade'}
          />
        </Paper>
      )}
      <br />
    </div>
  );
}

SchoolDashBoard.propTypes = {
  schoolId: PropTypes.string,
  SUBJECT_LIST: PropTypes.array,
  generateLatestReport: PropTypes.func.isRequired,
  showNotification: PropTypes.func.isRequired,
  onBackNav: PropTypes.func,
  dashboardReportsLoaded: PropTypes.bool.isRequired,
  dashboardReports: PropTypes.object,
};

export default connect(null, {
  generateLatestReport,
  showNotification,
})(SchoolDashBoard);
