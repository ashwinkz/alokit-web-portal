import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  container: {
    position: 'absolute',
    left: '50%',
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
  },
}));

const DataNotFound = () => {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <Typography variant="subtitle1">No data to display</Typography>
    </div>
  );
};

export default DataNotFound;
