/**
 * CSS style class for custom border radius, add style property to the element passing value for css variable *--custom-border-radius*.
 * The border radius, if not specified, defaults to 6px.
 */

const styles = (theme) => ({
  '@global': {
    '.noPadding': {
      padding: 0,
    },
    '.noTextTransform': {
      textTransform: 'none',
    },
    '.italicText': {
      fontStyle: 'italic',
    },
    '.fillScreen': {
      width: '100vw',
      height: '100vh',
    },
    '.fullWidth': {
      width: '100%',
      maxWidth: '100%',
    },
    '.fullHeight': {
      height: '100%',
      maxHeight: '100%',
    },
    '.contentMarginTop': {
      marginTop: theme.spacing(1),
    },
    '.contentMarginBottom': {
      marginBottom: theme.spacing(1),
    },
    '.popupFormContent': {
      overflow: 'auto',
      maxHeight: '60vh',
      padding: theme.spacing(2),
    },
    '.fitContentWidth': {
      maxWidth: 'fit-content',
    },
    '.flexGrow1': {
      flexGrow: 1,
    },
    '.error': {
      color: 'red',
    },
    '.pageToolbar': {
      color: 'white',
      backgroundColor: '#3f51b5',
      marginBottom: theme.spacing(4),
    },
    '.pageToolbarAction': {
      marginLeft: theme.spacing(4),
    },
    '.contentToolbar': {
      color: 'white',
      backgroundColor: '#3f51b5',
    },
    '.toolbarAction': {
      marginLeft: theme.spacing(2),
    },
  },
});

export default styles;
