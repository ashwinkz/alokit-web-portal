export const primaryButtonColor = '#2196f3';

export const secondaryButtonColor = '#ff7961';

export const errorWarningColor = '#f44336';
