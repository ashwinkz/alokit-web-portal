import { AUTH_ROLES } from './constants';

export const APP_ROUTES = {
  ROOT: '/',
  AUTH_ROOT: '/auth',
  AUTH_FORGOT_PASSWORD: '/auth/forgot_password',
  AUTH_SIGNIN: '/auth/signin',
  AUTH_SIGNIN_LANDING: '/auth/signin/landing',
  AUTH_SIGNED_OUT: '/auth/signout',
  DASHBOARD_PAGE: '/dashboard',
  MANAGE_SCHOOLS_PAGE: '/manage/schools',
  MANAGE_ALOKIT_USERS_PAGE: '/manage/staff',
  MANAGE_TEACHERS_PAGE: '/manage/teachers',
  MANAGE_STUDENTS_PAGE: '/manage/students',
  ADD_STUDENTS_PAGE: '/add/students',
  MANAGE_SUMMARY_PAGE: '/manage/summary',
  ADD_SUMMARY_PAGE: '/add/summary',
  VIEW_SCHOOLS_PAGE: '/schools',
};

export const APP_ROUTE_RESTRICTION_BY_ROLES = {
  [APP_ROUTES.DASHBOARD_PAGE]: [
    AUTH_ROLES.ALOKIT_ADMIN,
    AUTH_ROLES.ALOKIT_STAFF,
    AUTH_ROLES.SCHOOL_ADMIN,
    AUTH_ROLES.SCHOOL_TEACHER,
  ],
  [APP_ROUTES.MANAGE_SCHOOLS_PAGE]: [AUTH_ROLES.ALOKIT_ADMIN],
  [APP_ROUTES.VIEW_SCHOOLS_PAGE]: [AUTH_ROLES.ALOKIT_STAFF],
  [APP_ROUTES.MANAGE_ALOKIT_USERS_PAGE]: [AUTH_ROLES.ALOKIT_ADMIN],
  [APP_ROUTES.MANAGE_TEACHERS_PAGE]: [AUTH_ROLES.SCHOOL_ADMIN],
  [APP_ROUTES.MANAGE_STUDENTS_PAGE]: [AUTH_ROLES.SCHOOL_ADMIN],
  [APP_ROUTES.ADD_STUDENTS_PAGE]: [AUTH_ROLES.SCHOOL_ADMIN],
  [APP_ROUTES.MANAGE_SUMMARY_PAGE]: [AUTH_ROLES.SCHOOL_TEACHER],
  [APP_ROUTES.ADD_SUMMARY_PAGE]: [AUTH_ROLES.SCHOOL_TEACHER],
};

export const MOBILE_NUMBER_PREFIX = '+91';
export const MOBILE_NUMBER_REGEX = /^[1-9]\d{9}$/;
export const NAME_REGEX = /^[^`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/\s0-9].*$/;
// export const NAME_REGEX = /^\D(?!.*(&\$@#_\*)*?[^\n\w .*&$#_@]).*?[\w .*&$#_@]$/;

export const validatePhoneNumber = (value) => MOBILE_NUMBER_REGEX.test(value);
export const validateName = (value) => NAME_REGEX.test(value);
