/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getAlokitUser = /* GraphQL */ `
  query GetAlokitUser($cognitoUsername: ID!) {
    getAlokitUser(cognitoUsername: $cognitoUsername) {
      cognitoUsername
      firstName
      lastName
      role
      email
      phoneNumber
      disabled
      createdAt
      updatedAt
    }
  }
`;
export const listAlokitUsers = /* GraphQL */ `
  query ListAlokitUsers(
    $cognitoUsername: ID
    $filter: ModelAlokitUserFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listAlokitUsers(
      cognitoUsername: $cognitoUsername
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        cognitoUsername
        firstName
        lastName
        role
        email
        phoneNumber
        disabled
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getSummaryData = /* GraphQL */ `
  query GetSummaryData($id: ID!) {
    getSummaryData(id: $id) {
      id
      grade
      section
      subject
      comment
      score
      studentId
      teacherId
      schoolId
      student {
        id
        rollNumber
        studentName
        gender
        grade
        section
        schoolId
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const listSummaryData = /* GraphQL */ `
  query ListSummaryData(
    $filter: ModelSummaryDataFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listSummaryData(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        grade
        section
        subject
        comment
        score
        studentId
        teacherId
        schoolId
        student {
          id
          rollNumber
          studentName
          gender
          grade
          section
          schoolId
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getStudent = /* GraphQL */ `
  query GetStudent($id: ID!) {
    getStudent(id: $id) {
      id
      rollNumber
      studentName
      gender
      grade
      section
      schoolId
      createdAt
      updatedAt
    }
  }
`;
export const listStudents = /* GraphQL */ `
  query ListStudents(
    $filter: ModelStudentFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listStudents(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        rollNumber
        studentName
        gender
        grade
        section
        schoolId
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getGradeAssignment = /* GraphQL */ `
  query GetGradeAssignment($id: ID!) {
    getGradeAssignment(id: $id) {
      id
      grade
      section
      subject
      teacherId
      createdAt
      updatedAt
    }
  }
`;
export const listGradeAssignments = /* GraphQL */ `
  query ListGradeAssignments(
    $filter: ModelGradeAssignmentFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listGradeAssignments(
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        grade
        section
        subject
        teacherId
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getSchoolUser = /* GraphQL */ `
  query GetSchoolUser($cognitoUsername: ID!) {
    getSchoolUser(cognitoUsername: $cognitoUsername) {
      cognitoUsername
      firstName
      lastName
      gender
      gradeSecList {
        items {
          id
          grade
          section
          subject
          teacherId
          createdAt
          updatedAt
        }
        nextToken
      }
      summaryData {
        items {
          id
          grade
          section
          subject
          comment
          score
          studentId
          teacherId
          schoolId
          createdAt
          updatedAt
        }
        nextToken
      }
      role
      schoolId
      email
      phoneNumber
      disabled
      createdAt
      updatedAt
    }
  }
`;
export const listSchoolUsers = /* GraphQL */ `
  query ListSchoolUsers(
    $cognitoUsername: ID
    $filter: ModelSchoolUserFilterInput
    $limit: Int
    $nextToken: String
    $sortDirection: ModelSortDirection
  ) {
    listSchoolUsers(
      cognitoUsername: $cognitoUsername
      filter: $filter
      limit: $limit
      nextToken: $nextToken
      sortDirection: $sortDirection
    ) {
      items {
        cognitoUsername
        firstName
        lastName
        gender
        gradeSecList {
          nextToken
        }
        summaryData {
          nextToken
        }
        role
        schoolId
        email
        phoneNumber
        disabled
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getSchool = /* GraphQL */ `
  query GetSchool($id: ID!) {
    getSchool(id: $id) {
      id
      name
      diseCode
      branch
      address
      admin {
        cognitoUsername
        firstName
        lastName
        gender
        gradeSecList {
          nextToken
        }
        summaryData {
          nextToken
        }
        role
        schoolId
        email
        phoneNumber
        disabled
        createdAt
        updatedAt
      }
      schoolStaff {
        items {
          cognitoUsername
          firstName
          lastName
          gender
          role
          schoolId
          email
          phoneNumber
          disabled
          createdAt
          updatedAt
        }
        nextToken
      }
      schoolStudents {
        items {
          id
          rollNumber
          studentName
          gender
          grade
          section
          schoolId
          createdAt
          updatedAt
        }
        nextToken
      }
      schoolSummaryData {
        items {
          id
          grade
          section
          subject
          comment
          score
          studentId
          teacherId
          schoolId
          createdAt
          updatedAt
        }
        nextToken
      }
      baselineReport {
        id
        schoolId
        reportName
        reportPeriodStart
        reportPeriodEnd
        schoolAverages {
          nextToken
        }
        gradeAverages {
          nextToken
        }
        createdAt
        updatedAt
      }
      monthlyReports {
        items {
          id
          schoolId
          reportName
          reportPeriodStart
          reportPeriodEnd
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const listSchools = /* GraphQL */ `
  query ListSchools(
    $filter: ModelSchoolFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listSchools(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        diseCode
        branch
        address
        admin {
          cognitoUsername
          firstName
          lastName
          gender
          role
          schoolId
          email
          phoneNumber
          disabled
          createdAt
          updatedAt
        }
        schoolStaff {
          nextToken
        }
        schoolStudents {
          nextToken
        }
        schoolSummaryData {
          nextToken
        }
        baselineReport {
          id
          schoolId
          reportName
          reportPeriodStart
          reportPeriodEnd
          createdAt
          updatedAt
        }
        monthlyReports {
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getSchoolAvgScore = /* GraphQL */ `
  query GetSchoolAvgScore($id: ID!) {
    getSchoolAvgScore(id: $id) {
      id
      subject
      score
      studentCount
      reportId
      createdAt
      updatedAt
    }
  }
`;
export const listSchoolAvgScores = /* GraphQL */ `
  query ListSchoolAvgScores(
    $filter: ModelSchoolAvgScoreFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listSchoolAvgScores(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        subject
        score
        studentCount
        reportId
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getGradeAvgScore = /* GraphQL */ `
  query GetGradeAvgScore($id: ID!) {
    getGradeAvgScore(id: $id) {
      id
      subject
      grade
      score
      studentCount
      reportId
      createdAt
      updatedAt
    }
  }
`;
export const listGradeAvgScores = /* GraphQL */ `
  query ListGradeAvgScores(
    $filter: ModelGradeAvgScoreFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listGradeAvgScores(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        subject
        grade
        score
        studentCount
        reportId
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getMonthlyReport = /* GraphQL */ `
  query GetMonthlyReport($id: ID!) {
    getMonthlyReport(id: $id) {
      id
      schoolId
      reportName
      reportPeriodStart
      reportPeriodEnd
      schoolAverages {
        items {
          id
          subject
          score
          studentCount
          reportId
          createdAt
          updatedAt
        }
        nextToken
      }
      gradeAverages {
        items {
          id
          subject
          grade
          score
          studentCount
          reportId
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const listMonthlyReports = /* GraphQL */ `
  query ListMonthlyReports(
    $filter: ModelMonthlyReportFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listMonthlyReports(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        schoolId
        reportName
        reportPeriodStart
        reportPeriodEnd
        schoolAverages {
          nextToken
        }
        gradeAverages {
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const summaryDataBySchool = /* GraphQL */ `
  query SummaryDataBySchool(
    $schoolId: ID
    $sortDirection: ModelSortDirection
    $filter: ModelSummaryDataFilterInput
    $limit: Int
    $nextToken: String
  ) {
    summaryDataBySchool(
      schoolId: $schoolId
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        grade
        section
        subject
        comment
        score
        studentId
        teacherId
        schoolId
        student {
          id
          rollNumber
          studentName
          gender
          grade
          section
          schoolId
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const summaryDataByTeacher = /* GraphQL */ `
  query SummaryDataByTeacher(
    $teacherId: ID
    $sortDirection: ModelSortDirection
    $filter: ModelSummaryDataFilterInput
    $limit: Int
    $nextToken: String
  ) {
    summaryDataByTeacher(
      teacherId: $teacherId
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        grade
        section
        subject
        comment
        score
        studentId
        teacherId
        schoolId
        student {
          id
          rollNumber
          studentName
          gender
          grade
          section
          schoolId
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const summaryDataByStudent = /* GraphQL */ `
  query SummaryDataByStudent(
    $studentId: ID
    $sortDirection: ModelSortDirection
    $filter: ModelSummaryDataFilterInput
    $limit: Int
    $nextToken: String
  ) {
    summaryDataByStudent(
      studentId: $studentId
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        grade
        section
        subject
        comment
        score
        studentId
        teacherId
        schoolId
        student {
          id
          rollNumber
          studentName
          gender
          grade
          section
          schoolId
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const studentsBySchool = /* GraphQL */ `
  query StudentsBySchool(
    $schoolId: ID
    $sortDirection: ModelSortDirection
    $filter: ModelStudentFilterInput
    $limit: Int
    $nextToken: String
  ) {
    studentsBySchool(
      schoolId: $schoolId
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        rollNumber
        studentName
        gender
        grade
        section
        schoolId
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const gradesAssignedByTeacher = /* GraphQL */ `
  query GradesAssignedByTeacher(
    $teacherId: ID
    $sortDirection: ModelSortDirection
    $filter: ModelGradeAssignmentFilterInput
    $limit: Int
    $nextToken: String
  ) {
    gradesAssignedByTeacher(
      teacherId: $teacherId
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        grade
        section
        subject
        teacherId
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const usersBySchool = /* GraphQL */ `
  query UsersBySchool(
    $schoolId: ID
    $sortDirection: ModelSortDirection
    $filter: ModelSchoolUserFilterInput
    $limit: Int
    $nextToken: String
  ) {
    usersBySchool(
      schoolId: $schoolId
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        cognitoUsername
        firstName
        lastName
        gender
        gradeSecList {
          nextToken
        }
        summaryData {
          nextToken
        }
        role
        schoolId
        email
        phoneNumber
        disabled
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const schoolAveragesByReport = /* GraphQL */ `
  query SchoolAveragesByReport(
    $reportId: ID
    $sortDirection: ModelSortDirection
    $filter: ModelSchoolAvgScoreFilterInput
    $limit: Int
    $nextToken: String
  ) {
    schoolAveragesByReport(
      reportId: $reportId
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        subject
        score
        studentCount
        reportId
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const gradeAveragesByReport = /* GraphQL */ `
  query GradeAveragesByReport(
    $reportId: ID
    $sortDirection: ModelSortDirection
    $filter: ModelGradeAvgScoreFilterInput
    $limit: Int
    $nextToken: String
  ) {
    gradeAveragesByReport(
      reportId: $reportId
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        subject
        grade
        score
        studentCount
        reportId
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const monthlyReportsBySchool = /* GraphQL */ `
  query MonthlyReportsBySchool(
    $schoolId: ID
    $createdAt: ModelStringKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelMonthlyReportFilterInput
    $limit: Int
    $nextToken: String
  ) {
    monthlyReportsBySchool(
      schoolId: $schoolId
      createdAt: $createdAt
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        schoolId
        reportName
        reportPeriodStart
        reportPeriodEnd
        schoolAverages {
          nextToken
        }
        gradeAverages {
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
