/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createUserLogin = /* GraphQL */ `
  mutation CreateUserLogin(
    $firstName: String
    $lastName: String
    $email: AWSEmail
    $phoneNumber: AWSPhone
    $sendEmail: Boolean
    $sendSMS: Boolean
  ) {
    createUserLogin(
      firstName: $firstName
      lastName: $lastName
      email: $email
      phoneNumber: $phoneNumber
      sendEmail: $sendEmail
      sendSMS: $sendSMS
    ) {
      created
      message
    }
  }
`;
export const modifyUserAccess = /* GraphQL */ `
  mutation ModifyUserAccess($username: AWSPhone!, $action: String!) {
    modifyUserAccess(username: $username, action: $action) {
      modified
      message
    }
  }
`;
export const createAlokitUser = /* GraphQL */ `
  mutation CreateAlokitUser(
    $input: CreateAlokitUserInput!
    $condition: ModelAlokitUserConditionInput
  ) {
    createAlokitUser(input: $input, condition: $condition) {
      cognitoUsername
      firstName
      lastName
      role
      email
      phoneNumber
      disabled
      createdAt
      updatedAt
    }
  }
`;
export const updateAlokitUser = /* GraphQL */ `
  mutation UpdateAlokitUser(
    $input: UpdateAlokitUserInput!
    $condition: ModelAlokitUserConditionInput
  ) {
    updateAlokitUser(input: $input, condition: $condition) {
      cognitoUsername
      firstName
      lastName
      role
      email
      phoneNumber
      disabled
      createdAt
      updatedAt
    }
  }
`;
export const deleteAlokitUser = /* GraphQL */ `
  mutation DeleteAlokitUser(
    $input: DeleteAlokitUserInput!
    $condition: ModelAlokitUserConditionInput
  ) {
    deleteAlokitUser(input: $input, condition: $condition) {
      cognitoUsername
      firstName
      lastName
      role
      email
      phoneNumber
      disabled
      createdAt
      updatedAt
    }
  }
`;
export const createSummaryData = /* GraphQL */ `
  mutation CreateSummaryData(
    $input: CreateSummaryDataInput!
    $condition: ModelSummaryDataConditionInput
  ) {
    createSummaryData(input: $input, condition: $condition) {
      id
      grade
      section
      subject
      comment
      score
      studentId
      teacherId
      schoolId
      student {
        id
        rollNumber
        studentName
        gender
        grade
        section
        schoolId
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateSummaryData = /* GraphQL */ `
  mutation UpdateSummaryData(
    $input: UpdateSummaryDataInput!
    $condition: ModelSummaryDataConditionInput
  ) {
    updateSummaryData(input: $input, condition: $condition) {
      id
      grade
      section
      subject
      comment
      score
      studentId
      teacherId
      schoolId
      student {
        id
        rollNumber
        studentName
        gender
        grade
        section
        schoolId
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteSummaryData = /* GraphQL */ `
  mutation DeleteSummaryData(
    $input: DeleteSummaryDataInput!
    $condition: ModelSummaryDataConditionInput
  ) {
    deleteSummaryData(input: $input, condition: $condition) {
      id
      grade
      section
      subject
      comment
      score
      studentId
      teacherId
      schoolId
      student {
        id
        rollNumber
        studentName
        gender
        grade
        section
        schoolId
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const createStudent = /* GraphQL */ `
  mutation CreateStudent(
    $input: CreateStudentInput!
    $condition: ModelStudentConditionInput
  ) {
    createStudent(input: $input, condition: $condition) {
      id
      rollNumber
      studentName
      gender
      grade
      section
      schoolId
      createdAt
      updatedAt
    }
  }
`;
export const updateStudent = /* GraphQL */ `
  mutation UpdateStudent(
    $input: UpdateStudentInput!
    $condition: ModelStudentConditionInput
  ) {
    updateStudent(input: $input, condition: $condition) {
      id
      rollNumber
      studentName
      gender
      grade
      section
      schoolId
      createdAt
      updatedAt
    }
  }
`;
export const deleteStudent = /* GraphQL */ `
  mutation DeleteStudent(
    $input: DeleteStudentInput!
    $condition: ModelStudentConditionInput
  ) {
    deleteStudent(input: $input, condition: $condition) {
      id
      rollNumber
      studentName
      gender
      grade
      section
      schoolId
      createdAt
      updatedAt
    }
  }
`;
export const createGradeAssignment = /* GraphQL */ `
  mutation CreateGradeAssignment(
    $input: CreateGradeAssignmentInput!
    $condition: ModelGradeAssignmentConditionInput
  ) {
    createGradeAssignment(input: $input, condition: $condition) {
      id
      grade
      section
      subject
      teacherId
      createdAt
      updatedAt
    }
  }
`;
export const updateGradeAssignment = /* GraphQL */ `
  mutation UpdateGradeAssignment(
    $input: UpdateGradeAssignmentInput!
    $condition: ModelGradeAssignmentConditionInput
  ) {
    updateGradeAssignment(input: $input, condition: $condition) {
      id
      grade
      section
      subject
      teacherId
      createdAt
      updatedAt
    }
  }
`;
export const deleteGradeAssignment = /* GraphQL */ `
  mutation DeleteGradeAssignment(
    $input: DeleteGradeAssignmentInput!
    $condition: ModelGradeAssignmentConditionInput
  ) {
    deleteGradeAssignment(input: $input, condition: $condition) {
      id
      grade
      section
      subject
      teacherId
      createdAt
      updatedAt
    }
  }
`;
export const createSchoolUser = /* GraphQL */ `
  mutation CreateSchoolUser(
    $input: CreateSchoolUserInput!
    $condition: ModelSchoolUserConditionInput
  ) {
    createSchoolUser(input: $input, condition: $condition) {
      cognitoUsername
      firstName
      lastName
      gender
      gradeSecList {
        items {
          id
          grade
          section
          subject
          teacherId
          createdAt
          updatedAt
        }
        nextToken
      }
      summaryData {
        items {
          id
          grade
          section
          subject
          comment
          score
          studentId
          teacherId
          schoolId
          createdAt
          updatedAt
        }
        nextToken
      }
      role
      schoolId
      email
      phoneNumber
      disabled
      createdAt
      updatedAt
    }
  }
`;
export const updateSchoolUser = /* GraphQL */ `
  mutation UpdateSchoolUser(
    $input: UpdateSchoolUserInput!
    $condition: ModelSchoolUserConditionInput
  ) {
    updateSchoolUser(input: $input, condition: $condition) {
      cognitoUsername
      firstName
      lastName
      gender
      gradeSecList {
        items {
          id
          grade
          section
          subject
          teacherId
          createdAt
          updatedAt
        }
        nextToken
      }
      summaryData {
        items {
          id
          grade
          section
          subject
          comment
          score
          studentId
          teacherId
          schoolId
          createdAt
          updatedAt
        }
        nextToken
      }
      role
      schoolId
      email
      phoneNumber
      disabled
      createdAt
      updatedAt
    }
  }
`;
export const deleteSchoolUser = /* GraphQL */ `
  mutation DeleteSchoolUser(
    $input: DeleteSchoolUserInput!
    $condition: ModelSchoolUserConditionInput
  ) {
    deleteSchoolUser(input: $input, condition: $condition) {
      cognitoUsername
      firstName
      lastName
      gender
      gradeSecList {
        items {
          id
          grade
          section
          subject
          teacherId
          createdAt
          updatedAt
        }
        nextToken
      }
      summaryData {
        items {
          id
          grade
          section
          subject
          comment
          score
          studentId
          teacherId
          schoolId
          createdAt
          updatedAt
        }
        nextToken
      }
      role
      schoolId
      email
      phoneNumber
      disabled
      createdAt
      updatedAt
    }
  }
`;
export const createSchool = /* GraphQL */ `
  mutation CreateSchool(
    $input: CreateSchoolInput!
    $condition: ModelSchoolConditionInput
  ) {
    createSchool(input: $input, condition: $condition) {
      id
      name
      diseCode
      branch
      address
      admin {
        cognitoUsername
        firstName
        lastName
        gender
        gradeSecList {
          nextToken
        }
        summaryData {
          nextToken
        }
        role
        schoolId
        email
        phoneNumber
        disabled
        createdAt
        updatedAt
      }
      schoolStaff {
        items {
          cognitoUsername
          firstName
          lastName
          gender
          role
          schoolId
          email
          phoneNumber
          disabled
          createdAt
          updatedAt
        }
        nextToken
      }
      schoolStudents {
        items {
          id
          rollNumber
          studentName
          gender
          grade
          section
          schoolId
          createdAt
          updatedAt
        }
        nextToken
      }
      schoolSummaryData {
        items {
          id
          grade
          section
          subject
          comment
          score
          studentId
          teacherId
          schoolId
          createdAt
          updatedAt
        }
        nextToken
      }
      baselineReport {
        id
        schoolId
        reportName
        reportPeriodStart
        reportPeriodEnd
        schoolAverages {
          nextToken
        }
        gradeAverages {
          nextToken
        }
        createdAt
        updatedAt
      }
      monthlyReports {
        items {
          id
          schoolId
          reportName
          reportPeriodStart
          reportPeriodEnd
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateSchool = /* GraphQL */ `
  mutation UpdateSchool(
    $input: UpdateSchoolInput!
    $condition: ModelSchoolConditionInput
  ) {
    updateSchool(input: $input, condition: $condition) {
      id
      name
      diseCode
      branch
      address
      admin {
        cognitoUsername
        firstName
        lastName
        gender
        gradeSecList {
          nextToken
        }
        summaryData {
          nextToken
        }
        role
        schoolId
        email
        phoneNumber
        disabled
        createdAt
        updatedAt
      }
      schoolStaff {
        items {
          cognitoUsername
          firstName
          lastName
          gender
          role
          schoolId
          email
          phoneNumber
          disabled
          createdAt
          updatedAt
        }
        nextToken
      }
      schoolStudents {
        items {
          id
          rollNumber
          studentName
          gender
          grade
          section
          schoolId
          createdAt
          updatedAt
        }
        nextToken
      }
      schoolSummaryData {
        items {
          id
          grade
          section
          subject
          comment
          score
          studentId
          teacherId
          schoolId
          createdAt
          updatedAt
        }
        nextToken
      }
      baselineReport {
        id
        schoolId
        reportName
        reportPeriodStart
        reportPeriodEnd
        schoolAverages {
          nextToken
        }
        gradeAverages {
          nextToken
        }
        createdAt
        updatedAt
      }
      monthlyReports {
        items {
          id
          schoolId
          reportName
          reportPeriodStart
          reportPeriodEnd
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteSchool = /* GraphQL */ `
  mutation DeleteSchool(
    $input: DeleteSchoolInput!
    $condition: ModelSchoolConditionInput
  ) {
    deleteSchool(input: $input, condition: $condition) {
      id
      name
      diseCode
      branch
      address
      admin {
        cognitoUsername
        firstName
        lastName
        gender
        gradeSecList {
          nextToken
        }
        summaryData {
          nextToken
        }
        role
        schoolId
        email
        phoneNumber
        disabled
        createdAt
        updatedAt
      }
      schoolStaff {
        items {
          cognitoUsername
          firstName
          lastName
          gender
          role
          schoolId
          email
          phoneNumber
          disabled
          createdAt
          updatedAt
        }
        nextToken
      }
      schoolStudents {
        items {
          id
          rollNumber
          studentName
          gender
          grade
          section
          schoolId
          createdAt
          updatedAt
        }
        nextToken
      }
      schoolSummaryData {
        items {
          id
          grade
          section
          subject
          comment
          score
          studentId
          teacherId
          schoolId
          createdAt
          updatedAt
        }
        nextToken
      }
      baselineReport {
        id
        schoolId
        reportName
        reportPeriodStart
        reportPeriodEnd
        schoolAverages {
          nextToken
        }
        gradeAverages {
          nextToken
        }
        createdAt
        updatedAt
      }
      monthlyReports {
        items {
          id
          schoolId
          reportName
          reportPeriodStart
          reportPeriodEnd
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const createSchoolAvgScore = /* GraphQL */ `
  mutation CreateSchoolAvgScore(
    $input: CreateSchoolAvgScoreInput!
    $condition: ModelSchoolAvgScoreConditionInput
  ) {
    createSchoolAvgScore(input: $input, condition: $condition) {
      id
      subject
      score
      studentCount
      reportId
      createdAt
      updatedAt
    }
  }
`;
export const updateSchoolAvgScore = /* GraphQL */ `
  mutation UpdateSchoolAvgScore(
    $input: UpdateSchoolAvgScoreInput!
    $condition: ModelSchoolAvgScoreConditionInput
  ) {
    updateSchoolAvgScore(input: $input, condition: $condition) {
      id
      subject
      score
      studentCount
      reportId
      createdAt
      updatedAt
    }
  }
`;
export const deleteSchoolAvgScore = /* GraphQL */ `
  mutation DeleteSchoolAvgScore(
    $input: DeleteSchoolAvgScoreInput!
    $condition: ModelSchoolAvgScoreConditionInput
  ) {
    deleteSchoolAvgScore(input: $input, condition: $condition) {
      id
      subject
      score
      studentCount
      reportId
      createdAt
      updatedAt
    }
  }
`;
export const createGradeAvgScore = /* GraphQL */ `
  mutation CreateGradeAvgScore(
    $input: CreateGradeAvgScoreInput!
    $condition: ModelGradeAvgScoreConditionInput
  ) {
    createGradeAvgScore(input: $input, condition: $condition) {
      id
      subject
      grade
      score
      studentCount
      reportId
      createdAt
      updatedAt
    }
  }
`;
export const updateGradeAvgScore = /* GraphQL */ `
  mutation UpdateGradeAvgScore(
    $input: UpdateGradeAvgScoreInput!
    $condition: ModelGradeAvgScoreConditionInput
  ) {
    updateGradeAvgScore(input: $input, condition: $condition) {
      id
      subject
      grade
      score
      studentCount
      reportId
      createdAt
      updatedAt
    }
  }
`;
export const deleteGradeAvgScore = /* GraphQL */ `
  mutation DeleteGradeAvgScore(
    $input: DeleteGradeAvgScoreInput!
    $condition: ModelGradeAvgScoreConditionInput
  ) {
    deleteGradeAvgScore(input: $input, condition: $condition) {
      id
      subject
      grade
      score
      studentCount
      reportId
      createdAt
      updatedAt
    }
  }
`;
export const createMonthlyReport = /* GraphQL */ `
  mutation CreateMonthlyReport(
    $input: CreateMonthlyReportInput!
    $condition: ModelMonthlyReportConditionInput
  ) {
    createMonthlyReport(input: $input, condition: $condition) {
      id
      schoolId
      reportName
      reportPeriodStart
      reportPeriodEnd
      schoolAverages {
        items {
          id
          subject
          score
          studentCount
          reportId
          createdAt
          updatedAt
        }
        nextToken
      }
      gradeAverages {
        items {
          id
          subject
          grade
          score
          studentCount
          reportId
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateMonthlyReport = /* GraphQL */ `
  mutation UpdateMonthlyReport(
    $input: UpdateMonthlyReportInput!
    $condition: ModelMonthlyReportConditionInput
  ) {
    updateMonthlyReport(input: $input, condition: $condition) {
      id
      schoolId
      reportName
      reportPeriodStart
      reportPeriodEnd
      schoolAverages {
        items {
          id
          subject
          score
          studentCount
          reportId
          createdAt
          updatedAt
        }
        nextToken
      }
      gradeAverages {
        items {
          id
          subject
          grade
          score
          studentCount
          reportId
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteMonthlyReport = /* GraphQL */ `
  mutation DeleteMonthlyReport(
    $input: DeleteMonthlyReportInput!
    $condition: ModelMonthlyReportConditionInput
  ) {
    deleteMonthlyReport(input: $input, condition: $condition) {
      id
      schoolId
      reportName
      reportPeriodStart
      reportPeriodEnd
      schoolAverages {
        items {
          id
          subject
          score
          studentCount
          reportId
          createdAt
          updatedAt
        }
        nextToken
      }
      gradeAverages {
        items {
          id
          subject
          grade
          score
          studentCount
          reportId
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
