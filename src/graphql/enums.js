export const preloadEnumValues = /* GraphQL */ `
  query introspectEnumData {
    alokitUserRoleList: __type(name: "AlokitUserRoleEnum") {
      enumValues {
        name
      }
    }
    schoolUserRoleList: __type(name: "SchoolUserRoleEnum") {
      enumValues {
        name
      }
    }
    genderList: __type(name: "GenderEnum") {
      enumValues {
        name
      }
    }
    sectionList: __type(name: "SectionEnum") {
      enumValues {
        name
      }
    }
    gradeList: __type(name: "GradeEnum") {
      enumValues {
        name
      }
    }
    subjectList: __type(name: "SubjectEnum") {
      enumValues {
        name
      }
    }
    summaryScoreList: __type(name: "SummaryScoreEnum") {
      enumValues {
        name
      }
    }
  }
`;
