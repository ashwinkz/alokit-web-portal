/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateAlokitUser = /* GraphQL */ `
  subscription OnCreateAlokitUser {
    onCreateAlokitUser {
      cognitoUsername
      firstName
      lastName
      role
      email
      phoneNumber
      disabled
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateAlokitUser = /* GraphQL */ `
  subscription OnUpdateAlokitUser {
    onUpdateAlokitUser {
      cognitoUsername
      firstName
      lastName
      role
      email
      phoneNumber
      disabled
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteAlokitUser = /* GraphQL */ `
  subscription OnDeleteAlokitUser {
    onDeleteAlokitUser {
      cognitoUsername
      firstName
      lastName
      role
      email
      phoneNumber
      disabled
      createdAt
      updatedAt
    }
  }
`;
export const onCreateSummaryData = /* GraphQL */ `
  subscription OnCreateSummaryData {
    onCreateSummaryData {
      id
      grade
      section
      subject
      comment
      score
      studentId
      teacherId
      schoolId
      student {
        id
        rollNumber
        studentName
        gender
        grade
        section
        schoolId
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateSummaryData = /* GraphQL */ `
  subscription OnUpdateSummaryData {
    onUpdateSummaryData {
      id
      grade
      section
      subject
      comment
      score
      studentId
      teacherId
      schoolId
      student {
        id
        rollNumber
        studentName
        gender
        grade
        section
        schoolId
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteSummaryData = /* GraphQL */ `
  subscription OnDeleteSummaryData {
    onDeleteSummaryData {
      id
      grade
      section
      subject
      comment
      score
      studentId
      teacherId
      schoolId
      student {
        id
        rollNumber
        studentName
        gender
        grade
        section
        schoolId
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateStudent = /* GraphQL */ `
  subscription OnCreateStudent {
    onCreateStudent {
      id
      rollNumber
      studentName
      gender
      grade
      section
      schoolId
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateStudent = /* GraphQL */ `
  subscription OnUpdateStudent {
    onUpdateStudent {
      id
      rollNumber
      studentName
      gender
      grade
      section
      schoolId
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteStudent = /* GraphQL */ `
  subscription OnDeleteStudent {
    onDeleteStudent {
      id
      rollNumber
      studentName
      gender
      grade
      section
      schoolId
      createdAt
      updatedAt
    }
  }
`;
export const onCreateGradeAssignment = /* GraphQL */ `
  subscription OnCreateGradeAssignment {
    onCreateGradeAssignment {
      id
      grade
      section
      subject
      teacherId
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateGradeAssignment = /* GraphQL */ `
  subscription OnUpdateGradeAssignment {
    onUpdateGradeAssignment {
      id
      grade
      section
      subject
      teacherId
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteGradeAssignment = /* GraphQL */ `
  subscription OnDeleteGradeAssignment {
    onDeleteGradeAssignment {
      id
      grade
      section
      subject
      teacherId
      createdAt
      updatedAt
    }
  }
`;
export const onCreateSchoolUser = /* GraphQL */ `
  subscription OnCreateSchoolUser {
    onCreateSchoolUser {
      cognitoUsername
      firstName
      lastName
      gender
      gradeSecList {
        items {
          id
          grade
          section
          subject
          teacherId
          createdAt
          updatedAt
        }
        nextToken
      }
      summaryData {
        items {
          id
          grade
          section
          subject
          comment
          score
          studentId
          teacherId
          schoolId
          createdAt
          updatedAt
        }
        nextToken
      }
      role
      schoolId
      email
      phoneNumber
      disabled
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateSchoolUser = /* GraphQL */ `
  subscription OnUpdateSchoolUser {
    onUpdateSchoolUser {
      cognitoUsername
      firstName
      lastName
      gender
      gradeSecList {
        items {
          id
          grade
          section
          subject
          teacherId
          createdAt
          updatedAt
        }
        nextToken
      }
      summaryData {
        items {
          id
          grade
          section
          subject
          comment
          score
          studentId
          teacherId
          schoolId
          createdAt
          updatedAt
        }
        nextToken
      }
      role
      schoolId
      email
      phoneNumber
      disabled
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteSchoolUser = /* GraphQL */ `
  subscription OnDeleteSchoolUser {
    onDeleteSchoolUser {
      cognitoUsername
      firstName
      lastName
      gender
      gradeSecList {
        items {
          id
          grade
          section
          subject
          teacherId
          createdAt
          updatedAt
        }
        nextToken
      }
      summaryData {
        items {
          id
          grade
          section
          subject
          comment
          score
          studentId
          teacherId
          schoolId
          createdAt
          updatedAt
        }
        nextToken
      }
      role
      schoolId
      email
      phoneNumber
      disabled
      createdAt
      updatedAt
    }
  }
`;
export const onCreateSchool = /* GraphQL */ `
  subscription OnCreateSchool {
    onCreateSchool {
      id
      name
      diseCode
      branch
      address
      admin {
        cognitoUsername
        firstName
        lastName
        gender
        gradeSecList {
          nextToken
        }
        summaryData {
          nextToken
        }
        role
        schoolId
        email
        phoneNumber
        disabled
        createdAt
        updatedAt
      }
      schoolStaff {
        items {
          cognitoUsername
          firstName
          lastName
          gender
          role
          schoolId
          email
          phoneNumber
          disabled
          createdAt
          updatedAt
        }
        nextToken
      }
      schoolStudents {
        items {
          id
          rollNumber
          studentName
          gender
          grade
          section
          schoolId
          createdAt
          updatedAt
        }
        nextToken
      }
      schoolSummaryData {
        items {
          id
          grade
          section
          subject
          comment
          score
          studentId
          teacherId
          schoolId
          createdAt
          updatedAt
        }
        nextToken
      }
      baselineReport {
        id
        schoolId
        reportName
        reportPeriodStart
        reportPeriodEnd
        schoolAverages {
          nextToken
        }
        gradeAverages {
          nextToken
        }
        createdAt
        updatedAt
      }
      monthlyReports {
        items {
          id
          schoolId
          reportName
          reportPeriodStart
          reportPeriodEnd
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateSchool = /* GraphQL */ `
  subscription OnUpdateSchool {
    onUpdateSchool {
      id
      name
      diseCode
      branch
      address
      admin {
        cognitoUsername
        firstName
        lastName
        gender
        gradeSecList {
          nextToken
        }
        summaryData {
          nextToken
        }
        role
        schoolId
        email
        phoneNumber
        disabled
        createdAt
        updatedAt
      }
      schoolStaff {
        items {
          cognitoUsername
          firstName
          lastName
          gender
          role
          schoolId
          email
          phoneNumber
          disabled
          createdAt
          updatedAt
        }
        nextToken
      }
      schoolStudents {
        items {
          id
          rollNumber
          studentName
          gender
          grade
          section
          schoolId
          createdAt
          updatedAt
        }
        nextToken
      }
      schoolSummaryData {
        items {
          id
          grade
          section
          subject
          comment
          score
          studentId
          teacherId
          schoolId
          createdAt
          updatedAt
        }
        nextToken
      }
      baselineReport {
        id
        schoolId
        reportName
        reportPeriodStart
        reportPeriodEnd
        schoolAverages {
          nextToken
        }
        gradeAverages {
          nextToken
        }
        createdAt
        updatedAt
      }
      monthlyReports {
        items {
          id
          schoolId
          reportName
          reportPeriodStart
          reportPeriodEnd
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteSchool = /* GraphQL */ `
  subscription OnDeleteSchool {
    onDeleteSchool {
      id
      name
      diseCode
      branch
      address
      admin {
        cognitoUsername
        firstName
        lastName
        gender
        gradeSecList {
          nextToken
        }
        summaryData {
          nextToken
        }
        role
        schoolId
        email
        phoneNumber
        disabled
        createdAt
        updatedAt
      }
      schoolStaff {
        items {
          cognitoUsername
          firstName
          lastName
          gender
          role
          schoolId
          email
          phoneNumber
          disabled
          createdAt
          updatedAt
        }
        nextToken
      }
      schoolStudents {
        items {
          id
          rollNumber
          studentName
          gender
          grade
          section
          schoolId
          createdAt
          updatedAt
        }
        nextToken
      }
      schoolSummaryData {
        items {
          id
          grade
          section
          subject
          comment
          score
          studentId
          teacherId
          schoolId
          createdAt
          updatedAt
        }
        nextToken
      }
      baselineReport {
        id
        schoolId
        reportName
        reportPeriodStart
        reportPeriodEnd
        schoolAverages {
          nextToken
        }
        gradeAverages {
          nextToken
        }
        createdAt
        updatedAt
      }
      monthlyReports {
        items {
          id
          schoolId
          reportName
          reportPeriodStart
          reportPeriodEnd
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateSchoolAvgScore = /* GraphQL */ `
  subscription OnCreateSchoolAvgScore {
    onCreateSchoolAvgScore {
      id
      subject
      score
      studentCount
      reportId
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateSchoolAvgScore = /* GraphQL */ `
  subscription OnUpdateSchoolAvgScore {
    onUpdateSchoolAvgScore {
      id
      subject
      score
      studentCount
      reportId
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteSchoolAvgScore = /* GraphQL */ `
  subscription OnDeleteSchoolAvgScore {
    onDeleteSchoolAvgScore {
      id
      subject
      score
      studentCount
      reportId
      createdAt
      updatedAt
    }
  }
`;
export const onCreateGradeAvgScore = /* GraphQL */ `
  subscription OnCreateGradeAvgScore {
    onCreateGradeAvgScore {
      id
      subject
      grade
      score
      studentCount
      reportId
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateGradeAvgScore = /* GraphQL */ `
  subscription OnUpdateGradeAvgScore {
    onUpdateGradeAvgScore {
      id
      subject
      grade
      score
      studentCount
      reportId
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteGradeAvgScore = /* GraphQL */ `
  subscription OnDeleteGradeAvgScore {
    onDeleteGradeAvgScore {
      id
      subject
      grade
      score
      studentCount
      reportId
      createdAt
      updatedAt
    }
  }
`;
export const onCreateMonthlyReport = /* GraphQL */ `
  subscription OnCreateMonthlyReport {
    onCreateMonthlyReport {
      id
      schoolId
      reportName
      reportPeriodStart
      reportPeriodEnd
      schoolAverages {
        items {
          id
          subject
          score
          studentCount
          reportId
          createdAt
          updatedAt
        }
        nextToken
      }
      gradeAverages {
        items {
          id
          subject
          grade
          score
          studentCount
          reportId
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateMonthlyReport = /* GraphQL */ `
  subscription OnUpdateMonthlyReport {
    onUpdateMonthlyReport {
      id
      schoolId
      reportName
      reportPeriodStart
      reportPeriodEnd
      schoolAverages {
        items {
          id
          subject
          score
          studentCount
          reportId
          createdAt
          updatedAt
        }
        nextToken
      }
      gradeAverages {
        items {
          id
          subject
          grade
          score
          studentCount
          reportId
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteMonthlyReport = /* GraphQL */ `
  subscription OnDeleteMonthlyReport {
    onDeleteMonthlyReport {
      id
      schoolId
      reportName
      reportPeriodStart
      reportPeriodEnd
      schoolAverages {
        items {
          id
          subject
          score
          studentCount
          reportId
          createdAt
          updatedAt
        }
        nextToken
      }
      gradeAverages {
        items {
          id
          subject
          grade
          score
          studentCount
          reportId
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
