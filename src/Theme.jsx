import { createMuiTheme } from '@material-ui/core/styles';
import {
  errorWarningColor,
  primaryButtonColor,
  secondaryButtonColor,
} from './utils/themeConstants';
export default createMuiTheme({
  palette: {
    type: 'light',
    background: {
      page: '#FFFFFF',
      navigation: '#F8F8F8',
    },
    text: {
      primary: '#000000',
      navigation: '#38393D',
    },
    selection: {
      navigation: '#FC9D2B',
    },
    primary: { main: `${primaryButtonColor}` },
    secondary: { main: `${secondaryButtonColor}` },
    error: { main: `${errorWarningColor}` },
  },
  typography: {
    useNextVariants: true,
  },
});
