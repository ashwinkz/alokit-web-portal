import { Typography } from '@material-ui/core';
import React, { Component } from 'react';

export default class MobileLandingPage extends Component {
  render() {
    return (
      <div
        style={{
          marginTop: screen.height / 2,
          marginBottom: screen.height / 2,
          marginRight: screen.width / 4,
          marginLeft: screen.width / 4,
        }}
      >
        <Typography variant="h4" align="center">
          Website currently works only on Desktop view
        </Typography>
      </div>
    );
  }
}
