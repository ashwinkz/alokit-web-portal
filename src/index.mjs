// this is the main module, and hence the file extension '.mjs' -- https://v8.dev/features/modules#mjs

import React from 'react';
import ReactDOM from 'react-dom';

import configureStore from './configureStore';
import App from './App.jsx';

const store = configureStore();

ReactDOM.render(<App store={store} />, document.getElementById('root'));
