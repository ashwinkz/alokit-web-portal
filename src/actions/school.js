import { API, graphqlOperation } from 'aws-amplify';

import { listSchools } from 'GraphQL/queries';
import {
  createSchoolUser,
  createUserLogin,
  updateSchool,
} from 'GraphQL/mutations';
import { getCredentialDeliveryFlags } from './index';

export const fetchSchools = async () => {
  try {
    const apiResponse = await API.graphql(graphqlOperation(listSchools));
    return apiResponse.data.listSchools.items;
  } catch (error) {
    console.log('Error fetching summaries from API: ', error);
  }
};

export const fetchSchoolNames = async () => {
  try {
    const apiResponse = await API.graphql(
      graphqlOperation(`query schoolNames {
          listSchools {
            items {
              id
              name
              branch
            }
          }
        }
      `)
    );
    return apiResponse.data.listSchools.items;
  } catch (error) {
    console.log('Error fetching school names from API: ', error);
  }
};

export const addSchool = async (
  name,
  branch,
  address,
  adminFirstName,
  adminLastName,
  adminPhoneNumber,
  adminEmail,
  adminGender,
  deliveryMedium
  // eslint-disable-next-line max-params
) => {
  try {
    const deliveryMediumFlags = getCredentialDeliveryFlags(deliveryMedium);

    const adminLoginResponse = await API.graphql(
      graphqlOperation(createUserLogin, {
        firstName: adminFirstName,
        lastName: adminLastName,
        email: adminEmail,
        phoneNumber: adminPhoneNumber,
        sendEmail: deliveryMediumFlags.sendEmail,
        sendSMS: deliveryMediumFlags.sendSMS,
      })
    );
    //check if admin login creation failed (handles case of using already registered email or phone number)
    if (!adminLoginResponse.data.createUserLogin.created)
      return {
        school: null,
        success: false,
        message: adminLoginResponse.data.createUserLogin.message,
      };

    const schoolInput = {
      name,
      branch,
      address,
      schoolAdminId: adminPhoneNumber,
    };
    const schoolResponse = await API.graphql(
      graphqlOperation(
        ` mutation CreateSchool(
            $input: CreateSchoolInput!
            $condition: ModelSchoolConditionInput
          ) {
            createSchool(input: $input, condition: $condition) {
              id
              name
              diseCode
              branch
              address
              createdAt
              updatedAt
            }
          }
      `,
        { input: schoolInput }
      )
    );
    const schoolId = schoolResponse.data.createSchool.id;
    const adminInput = {
      cognitoUsername: adminPhoneNumber,
      phoneNumber: adminPhoneNumber,
      firstName: adminFirstName,
      lastName: adminLastName,
      email: adminEmail,
      gender: adminGender,
      role: 'SCHOOL_ADMIN',
      schoolId,
      disabled: false,
    };
    await API.graphql(
      graphqlOperation(createSchoolUser, { input: adminInput })
    );
    return {
      school: schoolResponse.data.createSchool,
      success: true,
      message: `School details saved. ${adminLoginResponse.data.createUserLogin.message}.`,
    };
  } catch (error) {
    console.log('Error creating school: ', error);
    return { school: null, success: false, message: 'Failed to add school' };
  }
};

export const editSchool = async (id, name, branch, address) => {
  try {
    const schoolInput = {
      id,
      name,
      branch,
      address,
    };
    const school = await API.graphql(
      graphqlOperation(updateSchool, { input: schoolInput })
    );
    return {
      success: true,
      school: { ...school.data.updateSchool },
      message: 'School details saved',
    };
  } catch (error) {
    console.log('Error updating school: ', error);
    return {
      success: false,
      school: null,
      message: 'Failed to save school details',
    };
  }
};
