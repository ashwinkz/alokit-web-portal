import { API, graphqlOperation } from 'aws-amplify';

import { studentsBySchool } from 'GraphQL/queries';
import { createStudent, updateStudent } from 'GraphQL/mutations';

export const fetchStudents = async (schoolId) => {
  try {
    const apiResponse = await API.graphql(
      graphqlOperation(studentsBySchool, { schoolId })
    );
    return apiResponse.data.studentsBySchool.items;
  } catch (error) {
    console.log('Error fetching students data  from API: ', error);
  }
};

export const addStudentInfo = async (studentList) => {
  try {
    // console.log('Input for student addition', studentList);
    studentList.forEach(async (element) => {
      const input = {
        rollNumber: element.rollNumber,
        studentName: element.studentName,
        gender: element.gender,
        grade: element.grade,
        section: element.section,
        schoolId: element.schoolId,
      };
      await API.graphql(graphqlOperation(createStudent, { input }));
    });
    return {
      success: true,
      message: 'Student details saved',
    };
  } catch (error) {
    console.log('Error adding student data from API: ', error);
    return {
      success: false,
      student: null,
      message: 'Failed to save student details',
    };
  }
};

export const editStudentInfo = async (
  id,
  rollNumber,
  name,
  gender,
  grade,
  section
  // eslint-disable-next-line max-params
) => {
  try {
    const input = {
      id,
      rollNumber,
      studentName: name,
      gender,
      grade,
      section,
    };
    const studentResponse = await API.graphql(
      graphqlOperation(updateStudent, { input })
    );
    return {
      success: true,
      student: { ...studentResponse.data.updateStudent },
      message: 'Student details saved',
    };
  } catch (error) {
    console.log('Error updating student data  from API: ', error);
    return {
      success: false,
      student: null,
      message: 'Failed to save student details',
    };
  }
};

// export const deleteStudentInfo = async (deleteId) => {
//   try {
//     const inputInfo = {
//       studentId: deleteId,
//     };
//     console.log('deleting row ' + deleteId);
//     const deleteStudentdata = await API.graphql({
//       query: mutations.deleteStudent,
//       variables: { input: inputInfo },
//     });
//     console.log(deleteStudentdata.data);
//   } catch (error) {
//     console.log('Error deleting data  from API: ', error);
//   }
// };
