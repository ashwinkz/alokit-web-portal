import { API, graphqlOperation } from 'aws-amplify';

import { AUTH_USER_ACTIONS } from 'Actions/index';
import { AUTH_STATES, AUTH_ROLES } from 'Utils/constants';
import { preloadEnumValues } from 'GraphQL/enums';
import {
  fetchSchoolReportsForDashboard,
  fetchAllSchoolReportsForDashboard,
} from './reports';

const {
  SET_AUTH_STATE,
  SET_REDIRECT_PATH,
  SET_LOGGED_IN_USER_DETAILS,
  CLEAR_LOGGED_IN_USER_DETAILS,
  SET_ENUM_VALUES,
} = AUTH_USER_ACTIONS;

export const setUserSignedIn = () => (dispatch) =>
  //set in redux store
  dispatch({
    type: SET_AUTH_STATE,
    payload: { authState: AUTH_STATES.SIGNED_IN },
  });

export const setUserSignedOut = () => (dispatch) => {
  //set in redux store
  dispatch({
    type: SET_AUTH_STATE,
    payload: { authState: AUTH_STATES.SIGNED_OUT },
  });
  dispatch({
    type: CLEAR_LOGGED_IN_USER_DETAILS,
    payload: {},
  });
};

/**
 * Set the path to redirect to after the user signs in next time. Ignore parameter to redirect to the default page.
 * @param redirectPath
 */
export const setPostSigninRedirectPath = (redirectPath) => (dispatch) =>
  //set in redux store
  dispatch({
    type: SET_REDIRECT_PATH,
    payload: { redirectPath },
  });

export const preloadDataFromBackend = (username) => async (dispatch) => {
  // pre-load logged in user details, enum values for GENDER / ROLES / GRADES / SECTIONS / SUMMARY TOPICS / SUMMARY SCORES
  let apiResponse = await API.graphql(
    graphqlOperation(
      ` query getUserDetails ($cognitoUsername: ID!) {
          getSchoolUser(cognitoUsername: $cognitoUsername) {
            cognitoUsername
            schoolId
            firstName
            lastName
            role
            gradeSecList {
              items {
                grade
                section
                subject
              }
            }
          }
          getAlokitUser(cognitoUsername: $cognitoUsername) {
            cognitoUsername
            firstName
            lastName
            role
          }
        }
    `,
      { cognitoUsername: username }
    )
  );
  const user = apiResponse.data.getAlokitUser
    ? apiResponse.data.getAlokitUser
    : apiResponse.data.getSchoolUser;

  let schoolName = null;
  let schoolBranch = null;

  if (
    [AUTH_ROLES.SCHOOL_ADMIN, AUTH_ROLES.SCHOOL_TEACHER].includes(user.role)
  ) {
    apiResponse = await API.graphql(
      graphqlOperation(
        ` query getSchool ($schoolId: ID!) {
          getSchool(id: $schoolId) {
            id
            name
            branch
            address
          }
        }
      `,
        { schoolId: user.schoolId }
      )
    );
    schoolName = apiResponse.data.getSchool.name;
    schoolBranch = apiResponse.data.getSchool.branch;
  }

  //set in redux store
  dispatch({
    type: SET_LOGGED_IN_USER_DETAILS,
    payload: {
      username: user.cognitoUsername,
      firstName: user.firstName,
      lastName: user.lastName,
      userRole: user.role,
      schoolID: user.schoolId,
      schoolName,
      schoolBranch,
      userGradeSectionList: user.gradeSecList ? user.gradeSecList.items : [],
    },
  });

  apiResponse = await API.graphql(graphqlOperation(preloadEnumValues));
  // console.log('Preloaded ENUM values: ', apiResponse.data);
  const alokitUserRoleList = apiResponse.data.alokitUserRoleList.enumValues.map(
    (e) => e.name
  );
  const schoolUserRoleList = apiResponse.data.schoolUserRoleList.enumValues.map(
    (e) => e.name
  );
  const genderList = apiResponse.data.genderList.enumValues.map((e) => e.name);
  const gradeList = apiResponse.data.gradeList.enumValues.map((e) => e.name);
  const sectionList = apiResponse.data.sectionList.enumValues.map(
    (e) => e.name
  );
  const subjectList = apiResponse.data.subjectList.enumValues.map(
    (e) => e.name
  );
  const summaryScoreList = apiResponse.data.summaryScoreList.enumValues.map(
    (e) => e.name
  );

  dispatch({
    type: SET_ENUM_VALUES,
    payload: {
      alokitUserRoleList,
      schoolUserRoleList,
      genderList,
      gradeList,
      sectionList,
      subjectList,
      summaryScoreList,
    },
  });

  // pre-load dashboard reports
  if ([AUTH_ROLES.ALOKIT_ADMIN, AUTH_ROLES.ALOKIT_STAFF].includes(user.role)) {
    fetchAllSchoolReportsForDashboard(dispatch, subjectList);
  } else {
    fetchSchoolReportsForDashboard(dispatch, user.schoolId, subjectList);
  }
};
