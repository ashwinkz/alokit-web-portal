import { APP_SHELL_ACTIONS } from 'Actions/index';

const {
  SET_SHOW_PROGRESS,
  SET_HIDE_PROGRESS,
  SET_SHOW_NOTIFICATION,
  SET_HIDE_NOTIFICATION,
} = APP_SHELL_ACTIONS;

const setShowProgress = () => ({
  type: SET_SHOW_PROGRESS,
  payload: {},
});

const setHideProgress = () => ({
  type: SET_HIDE_PROGRESS,
  payload: {},
});

const setShowNotification = (message, severity) => ({
  type: SET_SHOW_NOTIFICATION,
  payload: { message, severity },
});

const setHideNotification = () => ({
  type: SET_HIDE_NOTIFICATION,
  payload: {},
});

export const showProgressBar = () => (dispatch) => dispatch(setShowProgress());

export const hideProgressBar = () => (dispatch) => dispatch(setHideProgress());

export const showNotification = (message, severity) => (dispatch) =>
  dispatch(setShowNotification(message, severity));

export const hideNotification = () => (dispatch) =>
  dispatch(setHideNotification());
