import { API, graphqlOperation } from 'aws-amplify';

import {
  updateSchoolUser,
  createSchoolUser,
  createUserLogin,
  modifyUserAccess,
} from 'GraphQL/mutations';
import {
  getCredentialDeliveryFlags,
  DISABLE_USER_ACTION,
  ENABLE_USER_ACTION,
} from './index';

export const fetchTeachers = async (schoolId) => {
  try {
    const apiResponse = await API.graphql(
      graphqlOperation(
        `query UsersBySchool(
          $schoolId: ID
          $sortDirection: ModelSortDirection
          $filter: ModelSchoolUserFilterInput
          $limit: Int
          $nextToken: String
        ) {
          usersBySchool(
            schoolId: $schoolId
            sortDirection: $sortDirection
            filter: $filter
            limit: $limit
            nextToken: $nextToken
          ) {
            items {
              cognitoUsername
              firstName
              lastName
              gender
              gradeSecList {
                items {
                  id
                  grade
                  section
                  subject
                }
              }
              role
              schoolId
              email
              phoneNumber
              disabled
            }
            nextToken
          }
        }
      `,
        { schoolId }
      )
    );
    return apiResponse.data.usersBySchool.items;
  } catch (error) {
    console.log('Error fetching teachers from API: ', error);
  }
};

// Save Grade#Section list for a teacher.
// This will handle all scenarios - add Grade#Section not already assigned, remove Grade#Section no longer needed, leave other (already) assigned Grade#Section
const saveGradeAssignment = async (teacherId, gradeSectionList) => {
  try {
    //read current list for the teacher
    //prepare add and delete list
    //construct single mutation operation for all add/delete mutations (with alias)
    const currentListResponse = await API.graphql(
      graphqlOperation(`query MyQuery {
          listGradeAssignments(filter: {teacherId: {eq: "${teacherId}"}}) {
            items {
              id
              section
              grade
              subject
            }
          }
        }
      `)
    );
    const currentList = currentListResponse.data.listGradeAssignments.items;
    const addList = gradeSectionList.filter(
      (gs) =>
        !currentList.some(
          (ci) =>
            ci.grade === gs.grade &&
            ci.section === gs.section &&
            ci.subject === gs.subject
        )
    );
    const removeList = currentList.filter(
      (ci) =>
        !gradeSectionList.some(
          (gs) =>
            ci.grade === gs.grade &&
            ci.section === gs.section &&
            ci.subject === gs.subject
        )
    );
    const addMutations = addList
      .map(
        (i, index) =>
          `cm${index}: createGradeAssignment(input: {grade: ${i.grade}, section: ${i.section},subject: ${i.subject}, teacherId: "${teacherId}"}) { id }`
      )
      .join(' ');
    const deleteMutations = removeList
      .map(
        (i, index) =>
          `dm${index}: deleteGradeAssignment(input: {id: "${i.id}"}) { id }`
      )
      .join(' ');
    const mutationOperation = `mutation addGrades { ${addMutations} ${deleteMutations} }`;
    await API.graphql(graphqlOperation(mutationOperation));
  } catch (error) {
    console.log(
      'Error creating teacher subject grade records from API: ',
      error
    );
  }
};

export const editTeacher = async (
  cognitoUsername,
  firstName,
  lastName,
  gender,
  role,
  gradeSectionList
  // eslint-disable-next-line max-params
) => {
  try {
    const input = {
      cognitoUsername,
      firstName,
      lastName,
      role,
      gender,
    };

    const teacherResponse = await API.graphql(
      graphqlOperation(updateSchoolUser, { input })
    );
    await saveGradeAssignment(cognitoUsername, gradeSectionList);
    return {
      success: true,
      teacher: { ...teacherResponse.data.updateSchoolUser },
      message: 'Teacher details saved',
    };
  } catch (error) {
    console.log('Error modifying teacher from API: ', error);
    return {
      success: false,
      teacher: null,
      message: 'Failed to save teacher details',
    };
  }
};

export const addTeacher = async (
  schoolId,
  firstName,
  lastName,
  phoneNumber,
  email,
  gender,
  role,
  gradeSectionList,
  deliveryMedium
  // eslint-disable-next-line max-params
) => {
  try {
    const deliveryMediumFlags = getCredentialDeliveryFlags(deliveryMedium);
    const userLoginResponse = await API.graphql(
      graphqlOperation(createUserLogin, {
        firstName: firstName,
        lastName: lastName,
        email: email,
        phoneNumber: phoneNumber,
        sendEmail: deliveryMediumFlags.sendEmail,
        sendSMS: deliveryMediumFlags.sendSMS,
      })
    );
    //check if user login creation failed (handles case of using already registered email or phone number)
    if (!userLoginResponse.data.createUserLogin.created)
      return {
        user: null,
        success: false,
        message: userLoginResponse.data.createUserLogin.message,
      };

    const userResponse = await API.graphql(
      graphqlOperation(createSchoolUser, {
        input: {
          schoolId,
          cognitoUsername: phoneNumber,
          firstName,
          lastName,
          phoneNumber,
          email,
          gender,
          role,
          disabled: false,
        },
      })
    );
    await saveGradeAssignment(
      userResponse.data.createSchoolUser.cognitoUsername,
      gradeSectionList
    );
    return {
      success: true,
      teacher: { ...userResponse.data.createSchoolUser },
      message: `Teacher details saved. ${userLoginResponse.data.createUserLogin.message}.`,
    };
  } catch (error) {
    console.log('Error adding teacher from API: ', error);
    return {
      success: false,
      teacher: null,
      message: `Failed to save teacher details`,
    };
  }
};

export const modifySchoolUserAccess = async (phoneNumber, disableFlag) => {
  try {
    const apiResponse = await API.graphql(
      graphqlOperation(modifyUserAccess, {
        username: phoneNumber,
        action: disableFlag ? DISABLE_USER_ACTION : ENABLE_USER_ACTION,
      })
    );

    //check if user login creation failed (handles case of using already registered email or phone number)
    if (!apiResponse.data.modifyUserAccess.modified)
      return {
        success: false,
        message: apiResponse.data.modifyUserAccess.message,
      };

    await API.graphql(
      graphqlOperation(updateSchoolUser, {
        input: {
          cognitoUsername: phoneNumber,
          disabled: disableFlag,
        },
      })
    );

    return {
      success: true,
      message: apiResponse.data.modifyUserAccess.message,
    };
  } catch (error) {
    console.log('Error modifying school user access: ', error);
    return { message: 'Failed to modify school user access', success: false };
  }
};
