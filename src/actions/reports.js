import { API, graphqlOperation } from 'aws-amplify';
import moment from 'moment';

import { AUTH_USER_ACTIONS } from 'Actions/index';
import { createMonthlyReport, updateSchool } from 'GraphQL/mutations';

const {
  SET_DASHBOARD_REPORTS_LOADED,
  SET_DASHBOARD_REPORT,
} = AUTH_USER_ACTIONS;

const REPORT_PERIOD_DATE_FORMAT = 'YYYY-MM-DD';

export const ALL_GRADES_LABEL = 'All Grades';
export const LATEST_REPORT_LABEL = 'latest';
export const PREVIOUS_REPORT_LABEL = 'previous';
export const BASELINE_REPORT_LABEL = 'baseline';
export const SCORE_LABEL = 'score';
export const STUDENT_COUNT_LABEL = 'student_count';

const getDateFilterForLatestReports = () => {
  // Month start dates to filter reports for dashboard
  const currMonth = new Date();
  currMonth.setDate(1);
  const lastMonth = new Date(currMonth);
  lastMonth.setMonth(currMonth.getMonth() - 1);
  return {
    lastMonthStart: lastMonth,
    reportDateFilter: {
      reportPeriodStart: {
        le: moment(lastMonth).format(REPORT_PERIOD_DATE_FORMAT),
      },
    },
  };
};

/** Transform report data, ready to be displayed on dashboard report component */
const transformSchoolReportsForDashboard = (
  schoolData,
  lastMonthStart,
  SUBJECT_LIST
) => {
  const actualReports = {
    baselineReport: schoolData.baselineReport,
    latestMonthReport: schoolData.monthlyReports.items.find(
      (r) =>
        moment(r.reportPeriodStart).format(REPORT_PERIOD_DATE_FORMAT) ===
        moment(lastMonthStart).format(REPORT_PERIOD_DATE_FORMAT)
    ),
    // reports are fetched from backend ordered by created date DESC
    latestReport: schoolData.monthlyReports.items[0] || null,
    previousReport: schoolData.monthlyReports.items[1] || null,
  };

  // convert data for dashboard report component
  const reportDataByGrades = {};
  const populateGradeDataForReport = (
    gradeLabel,
    subject,
    report,
    score,
    studentCount
  ) => {
    if (!reportDataByGrades.hasOwnProperty(gradeLabel)) {
      reportDataByGrades[gradeLabel] = {};
      SUBJECT_LIST.map((s) => {
        reportDataByGrades[gradeLabel][s] = {
          [LATEST_REPORT_LABEL]: {},
          [PREVIOUS_REPORT_LABEL]: {},
          [BASELINE_REPORT_LABEL]: {},
        };
      });
    }
    reportDataByGrades[gradeLabel][subject][report] = {
      [SCORE_LABEL]: score,
      [STUDENT_COUNT_LABEL]: studentCount,
    };
  };
  // populate from latest report
  if (actualReports.latestReport) {
    // populate school level performance data
    actualReports.latestReport.schoolAverages.items.map((sa) => {
      populateGradeDataForReport(
        ALL_GRADES_LABEL,
        sa.subject,
        LATEST_REPORT_LABEL,
        sa.score,
        sa.studentCount
      );
    });
    // populate grade level performance data
    actualReports.latestReport.gradeAverages.items.map((ga) => {
      populateGradeDataForReport(
        ga.grade,
        ga.subject,
        LATEST_REPORT_LABEL,
        ga.score,
        ga.studentCount
      );
    });
  }
  // populate from previous report
  if (actualReports.previousReport) {
    // populate school level performance data
    actualReports.previousReport.schoolAverages.items.map((sa) => {
      populateGradeDataForReport(
        ALL_GRADES_LABEL,
        sa.subject,
        PREVIOUS_REPORT_LABEL,
        sa.score,
        sa.studentCount
      );
    });
    // populate grade level performance data
    actualReports.previousReport.gradeAverages.items.map((ga) => {
      populateGradeDataForReport(
        ga.grade,
        ga.subject,
        PREVIOUS_REPORT_LABEL,
        ga.score,
        ga.studentCount
      );
    });
  }
  // populate from baseline report
  if (actualReports.baselineReport) {
    // populate school level performance data
    actualReports.baselineReport.schoolAverages.items.map((sa) => {
      populateGradeDataForReport(
        ALL_GRADES_LABEL,
        sa.subject,
        BASELINE_REPORT_LABEL,
        sa.score,
        sa.studentCount
      );
    });
    // populate grade level performance data
    actualReports.baselineReport.gradeAverages.items.map((ga) => {
      populateGradeDataForReport(
        ga.grade,
        ga.subject,
        BASELINE_REPORT_LABEL,
        ga.score,
        ga.studentCount
      );
    });
  }

  const displayData = {
    schoolPerformanceData: {
      label: schoolData.name,
      subLabel: schoolData.branch,
      data: reportDataByGrades[ALL_GRADES_LABEL],
    },
    gradePerformanceData: Object.keys(reportDataByGrades)
      .filter((k) => k !== ALL_GRADES_LABEL)
      .map((k) => ({ label: k, data: reportDataByGrades[k] })),
    reportPeriods: {
      [LATEST_REPORT_LABEL]: (actualReports.latestReport || {}).reportName,
      [PREVIOUS_REPORT_LABEL]: (actualReports.previousReport || {}).reportName,
      [BASELINE_REPORT_LABEL]: (actualReports.baselineReport || {}).reportName,
    },
  };

  return {
    schoolId: schoolData.id,
    actualReports,
    displayData,
  };
};

/**
 * Fetch reports for all schools.
 * Load the following from the backend for each school
 * - baseline report, prev month's report, prev-prev month's report.
 * Transform the report data, ready to be displayed on dashboard report component.
 * Push the data to redux store.
 **/
export const fetchAllSchoolReportsForDashboard = async (
  dispatch,
  SUBJECT_LIST
) => {
  try {
    dispatch({
      type: SET_DASHBOARD_REPORTS_LOADED,
      payload: { reportsLoadedFlag: false },
    });

    const {
      lastMonthStart,
      reportDateFilter,
    } = getDateFilterForLatestReports();
    let apiResponse = await API.graphql(
      graphqlOperation(
        ` query AllSchoolReports (
            $filter: ModelMonthlyReportFilterInput
          ) {
          listSchools {
            items {
              id
              name
              branch
              baselineReport {
                id
                reportName
                reportPeriodEnd
                reportPeriodStart
                createdAt
                schoolAverages {
                  items {
                    id
                    score
                    studentCount
                    subject
                  }
                }
                gradeAverages {
                  items {
                    id
                    grade
                    score
                    studentCount
                    subject
                  }
                }
              }
              id
              monthlyReports(filter: $filter, sortDirection: DESC, limit: 2) {
                items {
                  id
                  reportName
                  reportPeriodEnd
                  reportPeriodStart
                  createdAt
                  schoolAverages {
                    items {
                      score
                      id
                      studentCount
                      subject
                    }
                  }
                  gradeAverages {
                    items {
                      id
                      grade
                      score
                      studentCount
                      subject
                    }
                  }
                }
              }
            }
          }
        }`,
        { filter: reportDateFilter }
      )
    );

    // transform report data and set in redux store
    apiResponse.data.listSchools.items.map((school) => {
      dispatch({
        type: SET_DASHBOARD_REPORT,
        payload: {
          schoolId: school.id,
          reportData: transformSchoolReportsForDashboard(
            school,
            lastMonthStart,
            SUBJECT_LIST
          ),
        },
      });
    });

    dispatch({
      type: SET_DASHBOARD_REPORTS_LOADED,
      payload: { reportsLoadedFlag: true },
    });

    return {
      success: true,
      message: 'Reports fetched successfully',
    };
  } catch (err) {
    console.log(`Error fetching reports for all schools: `, err);
    return {
      success: false,
      baselineReport: null,
      message: 'Failed to fetch reports',
    };
  }
};

/**
 * Fetch reports for the specified school.
 * Load the following from the backend for the school
 * - baseline report, prev month's report, prev-prev month's report.
 * Transform the report data, ready to be displayed on dashboard report component.
 * Push the data to redux store.
 **/
export const fetchSchoolReportsForDashboard = async (
  dispatch,
  schoolId,
  SUBJECT_LIST
) => {
  try {
    dispatch({
      type: SET_DASHBOARD_REPORTS_LOADED,
      payload: { reportsLoadedFlag: false },
    });

    const {
      lastMonthStart,
      reportDateFilter,
    } = getDateFilterForLatestReports();
    let apiResponse = await API.graphql(
      graphqlOperation(
        ` query SchoolReports (
          $schoolId: ID!
          $filter: ModelMonthlyReportFilterInput
        ) {
          getSchool(id: $schoolId) {
            id
            name
            branch
            baselineReport {
              id
              reportName
              reportPeriodEnd
              reportPeriodStart
              createdAt
              schoolAverages {
                items {
                  id
                  score
                  studentCount
                  subject
                }
              }
              gradeAverages {
                items {
                  id
                  grade
                  score
                  studentCount
                  subject
                }
              }
            }
            id
            monthlyReports(filter: $filter, sortDirection: DESC, limit: 2) {
              items {
                id
                reportName
                reportPeriodEnd
                reportPeriodStart
                createdAt
                schoolAverages {
                  items {
                    score
                    id
                    studentCount
                    subject
                  }
                }
                gradeAverages {
                  items {
                    id
                    grade
                    score
                    studentCount
                    subject
                  }
                }
              }
            }
          }
        }`,
        { schoolId, filter: reportDateFilter }
      )
    );

    // transform data and set in redux store
    dispatch({
      type: SET_DASHBOARD_REPORT,
      payload: {
        schoolId,
        reportData: transformSchoolReportsForDashboard(
          apiResponse.data.getSchool,
          lastMonthStart,
          SUBJECT_LIST
        ),
      },
    });

    dispatch({
      type: SET_DASHBOARD_REPORTS_LOADED,
      payload: { reportsLoadedFlag: true },
    });

    return {
      success: true,
      message: 'Reports fetched successfully',
    };
  } catch (err) {
    console.log(`Error fetching reports for school: ${schoolId}`, err);
    return {
      success: false,
      message: 'Failed to fetch reports',
    };
  }
};

/**
 * Generate monthly report for a school from the summary data of specified month.
 */
const generateMonthlyReport = async (schoolId, monthStart) => {
  try {
    // load summary data for the report period
    const dateFilterStart = new Date(monthStart);
    dateFilterStart.setDate(1);
    dateFilterStart.setHours(0, 0, 0, 0);
    const dateFilterEnd = new Date(monthStart);
    dateFilterEnd.setMonth(monthStart.getMonth() + 1);
    const reportMonth = dateFilterStart.toLocaleString('en-us', {
      month: 'long',
      year: 'numeric',
    });
    let apiResponse = await API.graphql(
      graphqlOperation(
        `query filteredSummaryData (
        $schoolId: ID!
        $filter: ModelSummaryDataFilterInput
      ) {
        summaryDataBySchool(schoolId: $schoolId, filter: $filter) {
          items {
            createdAt
            grade
            id
            score
            section
            studentId
            subject
          }
        }
      }`,
        {
          schoolId: schoolId,
          filter: {
            createdAt: { ge: `${dateFilterStart.toISOString()}` },
            and: { createdAt: { lt: `${dateFilterEnd.toISOString()}` } },
          },
        }
      )
    );

    // aggregate summary data
    const filteredSummaryData = apiResponse.data.summaryDataBySchool.items;
    if (filteredSummaryData.length <= 0)
      return {
        success: false,
        message: `Report could not be generated - no student scores available for the period ${reportMonth}`,
      };
    console.log('Summary data for report: ', filteredSummaryData);
    // get latest entry for Student/Grade/Section/Subject, in case of multiple records
    const dedupedData = filteredSummaryData.reduce(
      (accumulatedData, currentRecord) => {
        const prevIndex = accumulatedData.findIndex(
          (r) =>
            currentRecord.studentId === r.studentId &&
            currentRecord.grade === r.grade &&
            currentRecord.section === r.section &&
            currentRecord.subject === r.subject
        );
        if (prevIndex > -1) {
          // find the most recent of the 2 matching records
          const currRecordDate = new Date(currentRecord.createdAt);
          const prevRecordDate = new Date(accumulatedData[prevIndex].createdAt);
          if (currRecordDate >= prevRecordDate) {
            accumulatedData.splice(prevIndex, 1);
            return accumulatedData.concat(currentRecord);
          } else {
            return accumulatedData;
          }
        } else {
          // push the current record
          return accumulatedData.concat(currentRecord);
        }
      },
      []
    );
    console.log('De-duplicated data: ', dedupedData);
    // calculate averages across grades
    const gradeAverages = dedupedData
      .reduce((accumulatedData, currentRecord) => {
        const prevIndex = accumulatedData.findIndex(
          (r) =>
            currentRecord.grade === r.grade &&
            currentRecord.subject === r.subject
        );
        const currScore = parseInt(
          currentRecord.score.replace('LEVEL_', ''),
          10
        );
        if (prevIndex > -1) {
          accumulatedData[prevIndex] = {
            grade: currentRecord.grade,
            subject: currentRecord.subject,
            totalScore: accumulatedData[prevIndex].totalScore + currScore,
            studentCount: accumulatedData[prevIndex].studentCount + 1,
          };
          return accumulatedData;
        } else {
          return accumulatedData.concat({
            grade: currentRecord.grade,
            subject: currentRecord.subject,
            totalScore: currScore,
            studentCount: 1,
          });
        }
      }, [])
      .map((r) => ({ ...r, avgScore: r.totalScore / r.studentCount }));
    // calculate averages overall
    const schoolAverages = gradeAverages
      .reduce((accumulatedData, currentRecord) => {
        const prevIndex = accumulatedData.findIndex(
          (r) => currentRecord.subject === r.subject
        );
        if (prevIndex > -1) {
          accumulatedData[prevIndex] = {
            subject: currentRecord.subject,
            totalScore:
              accumulatedData[prevIndex].totalScore + currentRecord.totalScore,
            studentCount:
              accumulatedData[prevIndex].studentCount +
              currentRecord.studentCount,
          };
        } else {
          return accumulatedData.concat({
            subject: currentRecord.subject,
            totalScore: currentRecord.totalScore,
            studentCount: currentRecord.studentCount,
          });
        }
      }, [])
      .map((r) => ({ ...r, avgScore: r.totalScore / r.studentCount }));
    console.log('School averages: ', schoolAverages);
    console.log('Grade Avergaes: ', gradeAverages);

    // check and create monthly report for school
    dateFilterEnd.setDate(dateFilterEnd.getDate() - 1);
    apiResponse = await API.graphql(
      graphqlOperation(
        ` query SchoolReports (
          $schoolId: ID!
          $filter: ModelMonthlyReportFilterInput
        ) {
          getSchool(id: $schoolId) {
            id
            baselineReport {
              id
            }
            monthlyReports(filter: $filter) {
              items {
                id
              }
            }
          }
        }`,
        {
          schoolId,
          filter: {
            reportPeriodStart: {
              eq: moment(dateFilterStart).format(REPORT_PERIOD_DATE_FORMAT),
            },
            reportPeriodEnd: {
              eq: moment(dateFilterEnd).format(REPORT_PERIOD_DATE_FORMAT),
            },
          },
        }
      )
    );
    const school = apiResponse.data.getSchool;
    let reportId = null;
    if (school.monthlyReports.items.length <= 0) {
      apiResponse = await API.graphql(
        graphqlOperation(createMonthlyReport, {
          input: {
            reportName: reportMonth,
            reportPeriodStart: moment(dateFilterStart).format(
              REPORT_PERIOD_DATE_FORMAT
            ),
            reportPeriodEnd: moment(dateFilterEnd).format(
              REPORT_PERIOD_DATE_FORMAT
            ),
            schoolId,
          },
        })
      );
      reportId = apiResponse.data.createMonthlyReport.id;
    } else {
      reportId = school.monthlyReports.items[0].id;
      //TODO: delete existing attached averages for the report
    }
    // attach averages to the report
    const schoolAvgsMutations = schoolAverages
      .map(
        (i, index) =>
          `sam${index}: createSchoolAvgScore(input: {reportId: "${reportId}", subject: ${i.subject}, score: ${i.avgScore}, studentCount: ${i.studentCount}}) { id }`
      )
      .join(' ');
    const gradeAvgMutations = gradeAverages
      .map(
        (i, index) =>
          `gam${index}: createGradeAvgScore(input: {reportId: "${reportId}", grade: ${i.grade}, subject: ${i.subject}, score: ${i.avgScore}, studentCount: ${i.studentCount}}) { id }`
      )
      .join(' ');
    const avgScoreMutation = `mutation attachScoreAverages { ${schoolAvgsMutations} ${gradeAvgMutations} }`;
    await API.graphql(graphqlOperation(avgScoreMutation));

    // attach baseline report if it does not exist for the school
    if (!school.baselineReport) {
      await API.graphql(
        graphqlOperation(updateSchool, {
          input: { id: schoolId, baselineReportId: reportId },
        })
      );
    }

    return { success: true, message: 'Report generated' };
  } catch (err) {
    console.log(
      `Error trying to generate report for ${monthStart.toLocaleString()}`,
      err
    );
    return {
      success: false,
      message: 'Report generation failed, try again or contact ADMIN',
    };
  }
};

/**
 * Generate monthly report for a school from the summary data of latest (last) month.
 */
export const generateLatestReport = (schoolId, SUBJECT_LIST) => {
  return async (dispatch) => {
    // Month start and end dates for report generation
    const currMonthStart = new Date();
    currMonthStart.setDate(1);
    const lastMonthStart = new Date(currMonthStart);
    lastMonthStart.setMonth(currMonthStart.getMonth() - 1);
    const { success, message } = await generateMonthlyReport(
      schoolId,
      lastMonthStart
    );
    if (success) {
      // force load report data to the redux store (async - in the background)
      fetchSchoolReportsForDashboard(dispatch, schoolId, SUBJECT_LIST);
    }
    return {
      success,
      message,
    };
  };
};
