import { API, graphqlOperation } from 'aws-amplify';

import { listAlokitUsers } from 'GraphQL/queries';
import {
  createAlokitUser,
  createUserLogin,
  updateAlokitUser,
  modifyUserAccess,
} from 'GraphQL/mutations';
import {
  getCredentialDeliveryFlags,
  ENABLE_USER_ACTION,
  DISABLE_USER_ACTION,
} from './index';
export const fetchAlokitUsers = async () => {
  try {
    const apiResponse = await API.graphql(graphqlOperation(listAlokitUsers));
    return apiResponse.data.listAlokitUsers.items;
  } catch (error) {
    console.log('Error fetching alokit users from API: ', error);
    return [];
  }
};

export const addAlokitUser = async (
  firstName,
  lastName,
  phoneNumber,
  email,
  role,
  deliveryMedium
  // eslint-disable-next-line max-params
) => {
  try {
    const deliveryMediumFlags = getCredentialDeliveryFlags(deliveryMedium);
    const userLoginResponse = await API.graphql(
      graphqlOperation(createUserLogin, {
        firstName: firstName,
        lastName: lastName,
        email: email,
        phoneNumber: phoneNumber,
        sendEmail: deliveryMediumFlags.sendEmail,
        sendSMS: deliveryMediumFlags.sendSMS,
      })
    );
    //check if user login creation failed (handles case of using already registered email or phone number)
    if (!userLoginResponse.data.createUserLogin.created)
      return {
        user: null,
        success: false,
        message: userLoginResponse.data.createUserLogin.message,
      };

    const userResponse = await API.graphql(
      graphqlOperation(createAlokitUser, {
        input: {
          cognitoUsername: phoneNumber,
          phoneNumber: phoneNumber,
          firstName: firstName,
          lastName: lastName,
          email: email,
          role: role,
          disabled: false,
        },
      })
    );

    //return login related response message
    return {
      user: userResponse.data.createAlokitUser,
      success: true,
      message: `User details saved. ${userLoginResponse.data.createUserLogin.message}.`,
    };
  } catch (error) {
    console.log('Error adding new Alokit user: ', error);
    return { message: 'Failed to add user', success: false, user: null };
  }
};

export const editAlokitUser = async (
  cognitoUsername,
  firstName,
  lastName,
  role
) => {
  try {
    const userInput = {
      cognitoUsername,
      firstName,
      lastName,
      role,
    };
    const userResponse = await API.graphql(
      graphqlOperation(updateAlokitUser, { input: userInput })
    );

    return {
      success: true,
      user: { ...userResponse.data.updateAlokitUser },
      message: 'User details saved',
    };
  } catch (error) {
    console.log('Error updating Alokit user: ', error);
    return {
      success: false,
      user: null,
      message: 'Failed to save user details',
    };
  }
};

export const modifyAlokitUserAccess = async (phoneNumber, disableFlag) => {
  try {
    const apiResponse = await API.graphql(
      graphqlOperation(modifyUserAccess, {
        username: phoneNumber,
        action: disableFlag ? DISABLE_USER_ACTION : ENABLE_USER_ACTION,
      })
    );

    //check if user login creation failed (handles case of using already registered email or phone number)
    if (!apiResponse.data.modifyUserAccess.modified)
      return {
        success: false,
        message: apiResponse.data.modifyUserAccess.message,
      };

    await API.graphql(
      graphqlOperation(updateAlokitUser, {
        input: {
          cognitoUsername: phoneNumber,
          disabled: disableFlag,
        },
      })
    );

    return {
      success: true,
      message: apiResponse.data.modifyUserAccess.message,
    };
  } catch (error) {
    console.log('Error modifying user access: ', error);
    return { message: 'Failed to modify user access', success: false };
  }
};
