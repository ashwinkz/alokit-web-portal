import { API, graphqlOperation } from 'aws-amplify';
import * as mutations from 'GraphQL/mutations';
import { summaryDataByTeacher } from 'GraphQL/queries';

export const fetchSummaries = async (schoolId, teacherId) => {
  try {
    const monthStart = new Date();
    monthStart.setDate(1);
    monthStart.setHours(0, 0, 0, 0);
    const apiResponse = await API.graphql(
      graphqlOperation(summaryDataByTeacher, {
        teacherId: teacherId,
        filter: {
          createdAt: { ge: `${monthStart.toISOString()}` },
          and: { schoolId: { eq: schoolId } },
        },
      })
    );
    return apiResponse.data.summaryDataByTeacher.items;
  } catch (error) {
    console.log('Error fetching Summaries from API: ', error);
  }
};

export const fetchStudentNames = async (schoolId, gradeSectionList) => {
  try {
    const filterByGradeSection = gradeSectionList.map(
      (gradeSection) =>
        `{grade: {eq: ${gradeSection.grade}}, section: {eq: ${gradeSection.section}}}`
    );
    const filters = `{and: {or: [${filterByGradeSection.join(
      ','
    )}]}, schoolId: {eq: "${schoolId}"}}`;
    const apiResponse = await API.graphql(
      graphqlOperation(`query MyQuery {
        listStudents(filter: ${filters}) {
          items {
            studentName
            id
            section
            grade
            updatedAt
            schoolId
            gender
            createdAt
            rollNumber
          }
        }
      }
      `)
    );
    return apiResponse.data.listStudents.items;
  } catch (error) {
    console.log('Error fetching students from API: ', error);
  }
};

export const createSummary = async (
  schoolId,
  teacherId,
  grade,
  section,
  subject,
  SummaryList
  // eslint-disable-next-line max-params
) => {
  try {
    SummaryList.forEach(async (element) => {
      if (element.score && element.comment && element.studentId) {
        const inputInfo = {
          grade: grade,
          section: section,
          subject: subject,
          score: element.score,
          comment: element.comment,
          studentId: element.studentId,
          summaryDataStudentId: element.studentId,
          teacherId: teacherId,
          schoolId: schoolId,
        };
        await API.graphql({
          query: mutations.createSummaryData,
          variables: { input: inputInfo },
        });
      }
    });
    return {
      success: true,
      summaries: SummaryList,
      message: 'Summary data saved',
    };
  } catch (error) {
    console.log('Error creating summary from API: ', error);
    return {
      success: false,
      summaries: null,
      message: 'Failed to save Summary data',
    };
  }
};
