import React from 'react';
import PropTypes from 'prop-types';
import Amplify from 'aws-amplify';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import theme from './Theme';
import { MuiThemeProvider, withStyles } from '@material-ui/core/styles';

import { cognitoConfig } from './awsConfig';
import Root from 'Containers/root/index';
import globalStyles from 'Utils/customStyling';

Amplify.configure(cognitoConfig);

const browserSupportsHistoryApi = 'pushState' in window.history;

const App = ({ store }) => (
  <Provider store={store}>
    <BrowserRouter forceRefresh={!browserSupportsHistoryApi}>
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <Root />
      </MuiThemeProvider>
    </BrowserRouter>
  </Provider>
);

App.propTypes = {
  store: PropTypes.object.isRequired,
};

export default withStyles(globalStyles, { withTheme: true })(App);
