import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { compose } from 'redux';
import { withStyles } from '@material-ui/core/styles';
import { Grid, Paper } from '@material-ui/core';
import { Switch, Route } from 'react-router-dom';

import { APP_ROUTES } from 'Utils/appShell';
import { SignInForm, SignInLanding } from 'Containers/auth/signIn';
import { ForgotPassword } from 'Containers/auth/forgotPassword';
import { SignOutLanding } from 'Containers/auth/signOut';

const styles = (theme) => ({
  limitContentWidth: {
    [theme.breakpoints.down('md')]: {
      maxWidth: 720,
    },
    [theme.breakpoints.up('lg')]: {
      maxWidth: 720,
    },
    [theme.breakpoints.up('xl')]: {
      maxWidth: 1080,
    },
  },
  contentPadding: {
    padding: theme.spacing(4),
  },
  flexOccupyItem: {
    flexGrow: 1,
  },
  flexGrowItem: {
    flexGrow: 2,
  },
});

class AuthLandingRoot extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <Grid container direction="row" spacing={0} className={'fillScreen'}>
        <Grid item md={4} />
        <Grid item md={4}>
          <Grid container direction="column" className={'fullHeight'}>
            <Grid item className={classes.flexOccupyItem} />
            <Grid item className={classes.flexGrowItem}>
              <Paper
                variant="outlined"
                elevation={5}
                className={clsx(
                  classes.limitContentWidth,
                  classes.contentPadding,
                  'fullHeight'
                )}
              >
                <Grid
                  container
                  direction="column"
                  justify="space-around"
                  spacing={3}
                  className={'fullHeight'}
                >
                  <Grid item className={'fullWidth'}>
                    <Switch>
                      <Route
                        path={APP_ROUTES.AUTH_SIGNIN}
                        exact
                        component={SignInForm}
                      />
                      <Route
                        path={APP_ROUTES.AUTH_FORGOT_PASSWORD}
                        exact
                        component={ForgotPassword}
                      />
                      <Route
                        path={APP_ROUTES.AUTH_SIGNIN_LANDING}
                        component={SignInLanding}
                      />
                      <Route
                        path={APP_ROUTES.AUTH_SIGNED_OUT}
                        component={SignOutLanding}
                      />
                    </Switch>
                  </Grid>
                </Grid>
              </Paper>
            </Grid>
            <Grid item className={classes.flexOccupyItem} />
          </Grid>
        </Grid>
        <Grid item md={4} />
      </Grid>
    );
  }
}

AuthLandingRoot.propTypes = {
  classes: PropTypes.object.isRequired,
};

AuthLandingRoot = compose(withStyles(styles, { withTheme: true }))(
  AuthLandingRoot
);

export default AuthLandingRoot;
