import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { Switch, withRouter, Redirect } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';

import teacherListing from 'Components/teacher/teacherListing';
import studentListing from 'Components/student/studentList';
import studentAdd from 'Components/student/studentAdd';

import SchoolListing from 'Components/school/schoolListing';
import SummaryListing from 'Components/summary/summaryListing';
import SummaryAdd from 'Components/summary/summaryAdd';
import { APP_ROUTES } from 'Utils/appShell';
import userListing from 'Components/users/Listusers';
import DashboardPage from 'Containers/dashboard/index';
import {
  ProgressNotification,
  ActionNotification,
} from 'Components/notify/index';
import PrivateRoute from './privateRoute';

const styles = (theme) => ({
  root: {
    padding: theme.spacing(3),
  },
});

class PageRoot extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div>
        <ActionNotification />
        <ProgressNotification />
        <div className={classes.root}>
          <Switch>
            <PrivateRoute exact path={APP_ROUTES.ROOT}>
              <Redirect to={APP_ROUTES.DASHBOARD_PAGE} />
            </PrivateRoute>
            <PrivateRoute
              path={APP_ROUTES.DASHBOARD_PAGE}
              component={DashboardPage}
            />
            <PrivateRoute
              path={APP_ROUTES.MANAGE_ALOKIT_USERS_PAGE}
              component={userListing}
            />
            <PrivateRoute
              path={APP_ROUTES.MANAGE_TEACHERS_PAGE}
              component={teacherListing}
            />
            <PrivateRoute
              path={APP_ROUTES.MANAGE_STUDENTS_PAGE}
              component={studentListing}
            />
            <PrivateRoute
              path={APP_ROUTES.ADD_STUDENTS_PAGE}
              component={studentAdd}
            />
            <PrivateRoute
              path={APP_ROUTES.MANAGE_SCHOOLS_PAGE}
              component={SchoolListing}
            />
            <PrivateRoute
              path={APP_ROUTES.VIEW_SCHOOLS_PAGE}
              component={SchoolListing}
            />
            <PrivateRoute
              path={APP_ROUTES.MANAGE_SUMMARY_PAGE}
              component={SummaryListing}
            />
            <PrivateRoute
              path={APP_ROUTES.ADD_SUMMARY_PAGE}
              component={SummaryAdd}
            />
          </Switch>
        </div>
      </div>
    );
  }
}

PageRoot.propTypes = {
  classes: PropTypes.object.isRequired,
};

PageRoot = compose(
  withRouter,
  withStyles(styles, { withTheme: true })
)(PageRoot);

export default PageRoot;
