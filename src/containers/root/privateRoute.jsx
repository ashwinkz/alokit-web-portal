import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { Route, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Typography,
} from '@material-ui/core';

import { APP_ROUTES, APP_ROUTE_RESTRICTION_BY_ROLES } from 'Utils/appShell';

const PrivateRoute = (props) => {
  const [denyAccess, setDenyAccess] = React.useState(false);
  const [warningClosed, setWarningClosed] = React.useState(true);
  React.useEffect(() => {
    if (props.userProfileLoaded) {
      if (
        // ignore ROOT as it redirects
        props.path !== APP_ROUTES.ROOT &&
        !APP_ROUTE_RESTRICTION_BY_ROLES[props.path].includes(props.userRole)
      ) {
        setDenyAccess(true);
        setWarningClosed(false);
      } else {
        setDenyAccess(false);
        setWarningClosed(true);
      }
    }
  }, [warningClosed]);

  const handleWarningClose = () => {
    setWarningClosed(true);
  };

  const { component: Component, history, ...rest } = props;

  return (
    <Route
      {...rest}
      render={() => {
        return (
          props.userProfileLoaded &&
          (denyAccess ? (
            <Dialog
              open={denyAccess && !warningClosed}
              aria-labelledby="alert-deny-access-title"
              aria-describedby="alert-deny-access-desc"
            >
              <DialogTitle id="alert-deny-access-title">
                <Typography color="error" variant="subtitle1">
                  {'Access Denied'}
                </Typography>
              </DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-deny-access-desc">
                  <Typography variant="body2" color="textPrimary">
                    You do not have permission to access this page.
                  </Typography>
                  <Typography variant="body2" color="textPrimary">
                    You will be redirected to the home page.
                  </Typography>
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button
                  onClick={() => {
                    handleWarningClose();
                    history.push(APP_ROUTES.DASHBOARD_PAGE);
                  }}
                  variant="outlined"
                  color="primary"
                >
                  Continue
                </Button>
              </DialogActions>
            </Dialog>
          ) : (
            <Component {...props} />
          ))
        );
      }}
    />
  );
};

PrivateRoute.propTypes = {
  path: PropTypes.string,
  component: PropTypes.object,
  userRole: PropTypes.string,
  userProfileLoaded: PropTypes.bool,
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
};

export default compose(
  withRouter,
  connect(
    (state) => ({
      userRole: state.authUser.userProfile.userRole,
      userProfileLoaded: state.authUser.userProfileLoaded,
    }),
    null
  )
)(PrivateRoute);
