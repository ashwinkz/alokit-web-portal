import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import {
  Dashboard,
  Apartment,
  LocalLibrary,
  SupervisorAccount,
  GroupAdd,
  AssignmentTurnedIn,
  Assignment,
} from '@material-ui/icons';
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Tooltip,
  Zoom,
} from '@material-ui/core';
import { connect } from 'react-redux';

import { APP_ROUTES, APP_ROUTE_RESTRICTION_BY_ROLES } from 'Utils/appShell';

const styles = (theme) => ({
  centeredMenuIcon: {
    justifyContent: 'center',
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
  navMenuItem: {
    color: theme.palette.text.navigation,
    paddingRight: theme.spacing(4),
  },
  navMenuItemSelected: {
    color: theme.palette.text.navigation,
    borderRightColor: theme.palette.selection.navigation,
    borderRightStyle: 'solid',
    borderRightWidth: 2,
  },
  navMenuItemDisabled: {
    opacity: 0.4,
  },
});

const APP_NAV_LINKS = [
  {
    navLabel: 'Dashboard',
    helpText: 'Dashboard',
    navIcon: <Dashboard />,
    navRoute: APP_ROUTES.DASHBOARD_PAGE,
    rolesWithAccess: APP_ROUTE_RESTRICTION_BY_ROLES[APP_ROUTES.DASHBOARD_PAGE],
  },
  {
    navLabel: 'Manage Schools',
    helpText: 'Manage Schools',
    navIcon: <Apartment />,
    navRoute: APP_ROUTES.MANAGE_SCHOOLS_PAGE,
    rolesWithAccess:
      APP_ROUTE_RESTRICTION_BY_ROLES[APP_ROUTES.MANAGE_SCHOOLS_PAGE],
  },
  {
    navLabel: 'View Schools',
    helpText: 'View Schools',
    navIcon: <Apartment />,
    navRoute: APP_ROUTES.VIEW_SCHOOLS_PAGE,
    rolesWithAccess:
      APP_ROUTE_RESTRICTION_BY_ROLES[APP_ROUTES.VIEW_SCHOOLS_PAGE],
  },
  {
    navLabel: 'Manage Alokit Staff',
    helpText: 'Manage users in the Alokit team',
    navIcon: <SupervisorAccount />,
    navRoute: APP_ROUTES.MANAGE_ALOKIT_USERS_PAGE,
    rolesWithAccess:
      APP_ROUTE_RESTRICTION_BY_ROLES[APP_ROUTES.MANAGE_ALOKIT_USERS_PAGE],
  },
  {
    navLabel: 'Manage Teachers',
    helpText: 'Manage teachers in your school',
    navIcon: <SupervisorAccount />,
    navRoute: APP_ROUTES.MANAGE_TEACHERS_PAGE,
    rolesWithAccess:
      APP_ROUTE_RESTRICTION_BY_ROLES[APP_ROUTES.MANAGE_TEACHERS_PAGE],
  },

  {
    navLabel: 'Manage Students',
    helpText: 'Manage students in your school',
    navIcon: <LocalLibrary />,
    navRoute: APP_ROUTES.MANAGE_STUDENTS_PAGE,
    rolesWithAccess:
      APP_ROUTE_RESTRICTION_BY_ROLES[APP_ROUTES.MANAGE_STUDENTS_PAGE],
  },

  {
    navLabel: 'Add Students',
    helpText: 'Add students in your school',
    navIcon: <GroupAdd />,
    navRoute: APP_ROUTES.ADD_STUDENTS_PAGE,
    rolesWithAccess:
      APP_ROUTE_RESTRICTION_BY_ROLES[APP_ROUTES.ADD_STUDENTS_PAGE],
  },

  {
    navLabel: 'View Student Score',
    helpText: 'View Student Score',
    navIcon: <Assignment />,
    navRoute: APP_ROUTES.MANAGE_SUMMARY_PAGE,
    rolesWithAccess:
      APP_ROUTE_RESTRICTION_BY_ROLES[APP_ROUTES.MANAGE_SUMMARY_PAGE],
  },
  {
    navLabel: 'Add Student Score',
    helpText: 'Add Student Score',
    navIcon: <AssignmentTurnedIn />,
    navRoute: APP_ROUTES.ADD_SUMMARY_PAGE,
    rolesWithAccess:
      APP_ROUTE_RESTRICTION_BY_ROLES[APP_ROUTES.ADD_SUMMARY_PAGE],
  },
];

class NavigationMenu extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      navItems: APP_NAV_LINKS,
    };
  }

  handleAppNavigation = (toPath) => {
    const { history, location } = this.props;
    const { navItems } = this.state;
    const selectedNavItem = navItems.find((e) => e.navRoute === toPath);
    if (selectedNavItem && selectedNavItem.navRoute !== location.pathname) {
      history.push(toPath);
    }
  };

  render() {
    const { classes, userRole, location } = this.props;
    const { navItems } = this.state;

    return (
      userRole && (
        <List>
          {navItems.map(
            (nav, index) =>
              nav.rolesWithAccess.includes(userRole) && (
                <Tooltip
                  TransitionComponent={Zoom}
                  title={nav.helpText}
                  placement="right-end"
                  key={index}
                  arrow
                >
                  <ListItem
                    disableGutters
                    button
                    key={index}
                    classes={{
                      root: classes.navMenuItem,
                      selected: classes.navMenuItemSelected,
                    }}
                    selected={location.pathname === nav.navRoute}
                    onClick={() => this.handleAppNavigation(nav.navRoute)}
                  >
                    <ListItemIcon classes={{ root: classes.centeredMenuIcon }}>
                      {nav.navIcon}
                    </ListItemIcon>
                    <ListItemText primary={nav.navLabel} />
                  </ListItem>
                </Tooltip>
              )
          )}
        </List>
      )
    );
  }
}

NavigationMenu.propTypes = {
  location: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
  userRole: PropTypes.string,
};

NavigationMenu = compose(
  withStyles(styles, { withTheme: true }),
  withRouter,
  connect(
    (state) => ({
      userRole: state.authUser.userProfile.userRole,
    }),
    null
  )
)(NavigationMenu);

export default NavigationMenu;
