import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { compose } from 'redux';
import { withStyles } from '@material-ui/core/styles';
import { Switch, Route, withRouter } from 'react-router-dom';
import {
  AppBar,
  Button,
  Drawer,
  Divider,
  Fade,
  IconButton,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
  Tooltip,
} from '@material-ui/core';
import { green } from '@material-ui/core/colors';
import {
  School as EduIcon,
  AccountBoxOutlined,
  ExitToAppOutlined,
  MoreVert as MoreVertIcon,
  OpenInNewTwoTone as OpenInNewTwoToneIcon,
} from '@material-ui/icons';
import { connect } from 'react-redux';
import { Auth } from 'aws-amplify';

import {
  setUserSignedIn,
  setUserSignedOut,
  setPostSigninRedirectPath,
} from 'Actions/authUser';
import { APP_ROUTES } from 'Utils/appShell';
import { AUTH_STATES } from 'Utils/constants';
import PageRoot from 'Containers/root/pageRoot';
import AuthLandingRoot from 'Containers/root/authRoot';
import NavigationMenu from 'Containers/root/navigation';
import { ReactComponent as AlokitLogo } from 'Images/alokit-logo.svg';

const drawerWidth = 240;

const styles = (theme) => ({
  root: {
    display: 'flex',
  },
  page: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.page,
  },
  navBackground: {
    backgroundColor: theme.palette.background.navigation,
  },
  navLink: {
    color: theme.palette.text.navigation,
    marginRight: theme.spacing(1),
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    width: drawerWidth,
  },
  drawerPaper: {
    color: theme.palette.text.navigation,
    backgroundColor: theme.palette.background.navigation,
    width: drawerWidth,
  },
  nameAvatar: {
    color: '#fff',
    backgroundColor: green[500],
  },
  schoolName: {
    display: 'inline-flex',
    position: 'absolute',
    left: '35%',
    alignItems: 'baseline',
  },
});

class Root extends React.Component {
  constructor(props) {
    super();
    this.state = {
      userMenuAnchor: null,
    };
    this.handleUserMenu = this.handleOpenUserMenu.bind(this);
    this.handleSignOut = this.handleSignOut.bind(this);
  }

  async componentDidMount() {
    const {
      setUserSignedIn,
      setPostSigninRedirectPath,
      location,
      history,
    } = this.props;

    // save current path to redirect to after sign-in & landing
    if (!location.pathname.startsWith(APP_ROUTES.AUTH_ROOT))
      setPostSigninRedirectPath(location.pathname);
    // check if the user is signed in
    try {
      await Auth.currentAuthenticatedUser();
      setUserSignedIn();
      // save current route and redirect to landing page to load essentials/metadata
      history.push(APP_ROUTES.AUTH_SIGNIN_LANDING);
    } catch (err) {
      console.error('Error validating user session: ', err);
      // redirect to sign-in
      if (!location.pathname.startsWith(APP_ROUTES.AUTH_ROOT))
        history.push(APP_ROUTES.AUTH_SIGNIN);
    }
  }

  async handleSignOut() {
    await Auth.signOut();
    const { setUserSignedOut, history } = this.props;
    setUserSignedOut();
    history.push(APP_ROUTES.AUTH_SIGNED_OUT);
  }

  handleOpenUserMenu = (e) => {
    this.setState({ userMenuAnchor: e.currentTarget });
  };

  handleCloseUserMenu = () => {
    this.setState({ userMenuAnchor: null });
  };

  render() {
    const {
      userAuthState,
      classes,
      userFirstname,
      userLastname,
      userRole,
      userSchoolBranch,
      userSchoolName,
    } = this.props;
    const displayName = (userFirstname || '') + ' ' + (userLastname || '');
    return (
      <div className={classes.root}>
        <AppBar
          position="fixed"
          className={clsx(classes.navBackground, classes.appBar)}
        >
          <Toolbar>
            <EduIcon fontSize="large" className={classes.navLink} />
            <AlokitLogo className={classes.navLink} />
            {userSchoolName && (
              <div className={classes.schoolName}>
                <Typography variant="h5" color="textPrimary">
                  {userSchoolName}
                </Typography>
                <span>&nbsp;&nbsp;</span>
                <Typography variant="h6" color="textSecondary">
                  {userSchoolBranch}
                </Typography>
              </div>
            )}
            <div className={'flexGrow1'} />
            <Tooltip placement="bottom" arrow title="Go to alokit.org">
              <Button
                variant="contained"
                disableElevation
                disableRipple
                href="https://alokit.org/"
                target="_blank"
                className={clsx('noTextTransform', classes.navLink)}
                startIcon={<OpenInNewTwoToneIcon />}
              >
                About
              </Button>
            </Tooltip>
            {userAuthState === AUTH_STATES.SIGNED_IN && (
              <>
                <Button
                  variant="contained"
                  disableElevation
                  disableRipple
                  className={clsx('noTextTransform', classes.navLink)}
                  startIcon={<AccountBoxOutlined />}
                >
                  {userFirstname}
                </Button>
                <IconButton
                  aria-label="more"
                  aria-controls="signed-in-user-menu"
                  aria-haspopup="true"
                  onClick={this.handleOpenUserMenu}
                >
                  <MoreVertIcon />
                </IconButton>
                <Menu
                  id="signed-in-user-menu"
                  anchorEl={this.state.userMenuAnchor}
                  open={Boolean(this.state.userMenuAnchor)}
                  onClose={this.handleCloseUserMenu}
                  TransitionComponent={Fade}
                >
                  <MenuItem>
                    <ListItemIcon>
                      <AccountBoxOutlined />
                    </ListItemIcon>
                    <ListItemText
                      primary={displayName}
                      secondary={
                        <Typography variant="caption" color="textSecondary">
                          {userRole}
                        </Typography>
                      }
                    />
                  </MenuItem>
                  <Divider />
                  <MenuItem
                    onClick={() => {
                      this.handleCloseUserMenu();
                      this.handleSignOut();
                    }}
                  >
                    <ListItemIcon>
                      <ExitToAppOutlined />
                    </ListItemIcon>
                    <ListItemText primary="Sign Out" />
                  </MenuItem>
                </Menu>
              </>
            )}
          </Toolbar>
        </AppBar>
        {userAuthState === AUTH_STATES.SIGNED_IN && (
          <Drawer
            className={classes.drawer}
            variant="permanent"
            open={false}
            classes={{ paper: classes.drawerPaper }}
          >
            <Toolbar />
            <NavigationMenu />
          </Drawer>
        )}
        <main className={classes.page}>
          <Toolbar />
          <Switch>
            <Route path={APP_ROUTES.AUTH_ROOT} component={AuthLandingRoot} />
            <Route component={PageRoot} />
          </Switch>
        </main>
      </div>
    );
  }
}

Root.propTypes = {
  classes: PropTypes.object.isRequired,
  userAuthState: PropTypes.string.isRequired,
  userFirstname: PropTypes.string,
  userLastname: PropTypes.string,
  userRole: PropTypes.string,
  userSchoolName: PropTypes.string,
  userSchoolBranch: PropTypes.string,
  setUserSignedIn: PropTypes.func.isRequired,
  setUserSignedOut: PropTypes.func.isRequired,
  setPostSigninRedirectPath: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
};

export default compose(
  withStyles(styles, { withTheme: true }),
  withRouter,
  connect(
    (state) => ({
      userAuthState: state.authUser.userAuthState,
      userFirstname: state.authUser.userProfile.firstName,
      userLastname: state.authUser.userProfile.lastName,
      userRole: state.authUser.userProfile.userRole,
      userSchoolName: state.authUser.userProfile.school.name,
      userSchoolBranch: state.authUser.userProfile.school.branch,
    }),
    {
      setUserSignedIn,
      setUserSignedOut,
      setPostSigninRedirectPath,
    }
  )
)(Root);
