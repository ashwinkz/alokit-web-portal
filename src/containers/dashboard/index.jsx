import React from 'react';
import { connect } from 'react-redux';
import { Paper } from '@material-ui/core';

import AlokitDashBoard from 'Components/dashboard/alokitDashBoard';
import SchoolDashBoard from 'Components/dashboard/schoolDashBoard';
import { AUTH_ROLES } from 'Utils/constants';

function DashboardPage(props) {
  return (
    <Paper elevation={0}>
      {props.currentUserProfileLoaded &&
        ([AUTH_ROLES.SCHOOL_ADMIN, AUTH_ROLES.SCHOOL_TEACHER].includes(
          props.currentUserRole
        ) ? (
          <SchoolDashBoard
            schoolId={props.currentUserSchoolId}
            SUBJECT_LIST={props.SUBJECT_LIST}
            dashboardReports={props.dashboardReports}
            dashboardReportsLoaded={props.dashboardReportsLoaded}
          />
        ) : (
          <AlokitDashBoard
            SUBJECT_LIST={props.SUBJECT_LIST}
            dashboardReports={props.dashboardReports}
            dashboardReportsLoaded={props.dashboardReportsLoaded}
          />
        ))}
    </Paper>
  );
}

function mapStateToProps(state) {
  return {
    currentUserSchoolId: state.authUser.userProfile.school.id,
    currentUserRole: state.authUser.userProfile.userRole,
    currentUserProfileLoaded: state.authUser.userProfileLoaded,
    SUBJECT_LIST: state.authUser.SUBJECT_LIST,
    dashboardReportsLoaded: state.authUser.dashboardReportsLoaded,
    dashboardReports: state.authUser.dashboardReports,
  };
}

export default connect(mapStateToProps, null)(DashboardPage);
