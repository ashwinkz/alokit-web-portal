import React from 'react';
import PropTypes from 'prop-types';
import {
  Typography,
  Button,
  Grid,
  Chip,
  LinearProgress,
  InputAdornment,
} from '@material-ui/core';
import { compose } from 'redux';
import { withStyles } from '@material-ui/styles';
import { withRouter } from 'react-router-dom';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import { Auth } from 'aws-amplify';
import { connect } from 'react-redux';

import { NOTIFICATION_SEVERITY } from 'Components/notify/index';
import { showNotification } from 'Actions/appShell';
import {
  MOBILE_NUMBER_PREFIX,
  validatePhoneNumber,
  APP_ROUTES,
} from 'Utils/appShell';
// import { AUTH_STATES } from 'Utils/constants';

const styles = (theme) => ({
  errorChip: {
    borderRadius: 4,
  },
  cursorPointer: {
    cursor: 'pointer',
  },
});

class ForgotPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mobileNum: '',
      forgotPasswordError: null,
      setNewPassword: false,
      verificationCode: '',
      cognitoUser: null,
      newPassword: '',
      repeatPassword: '',
      actionInProgress: false,
    };
    this.handleValueChange = this.handleValueChange.bind(this);
    this.handleForgotPassword = this.handleForgotPassword.bind(this);
    this.handleNewPassword = this.handleNewPassword.bind(this);
  }

  componentDidMount() {
    //custom rule for password matching in (re)set password form
    ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
      if (value !== this.state.newPassword) {
        return false;
      }
      return true;
    });
    //custom rule to validate username (should be a valid phone number)
    ValidatorForm.addValidationRule('isPhoneNumber', (value) =>
      validatePhoneNumber(value)
    );
  }

  componentWillUnmount() {
    // remove custom rule from validator
    ValidatorForm.removeValidationRule('isPasswordMatch');
    ValidatorForm.removeValidationRule('isPhoneNumber');
  }

  handleValueChange = (fieldName) => (event) => {
    this.setState({
      [fieldName]: event.target.value,
    });
  };

  handleForgotPassword = async (event) => {
    const { mobileNum } = this.state;
    const uname = `${MOBILE_NUMBER_PREFIX}${mobileNum}`;
    this.setState({ forgotPasswordError: null, actionInProgress: true });
    try {
      const user = await Auth.forgotPassword(uname);
      this.setState({
        setNewPassword: true,
        forgotPasswordError: 'Sent a verification code to ' + uname,
        cognitoUser: user,
        actionInProgress: false,
      });
    } catch (err) {
      console.error('Error during forgot password flow: ', err);
      let errorToDisplay =
        'Unknown error during forgot password flow, try again';
      if (err.code === 'UserNotConfirmedException') {
        //
      } else if (err.code === 'PasswordResetRequiredException') {
        //
      } else if (
        ['NotAuthorizedException', 'UserNotFoundException'].includes(err.code)
      ) {
        errorToDisplay = err.message;
      } else {
        //FIXME: handle using error boundaries
        console.error('Unknown error during forgot password flow: ', err);
      }
      this.setState({
        forgotPasswordError: errorToDisplay,
        actionInProgress: false,
      });
    }
  };

  handleNewPassword = async (event) => {
    const { mobileNum, newPassword, verificationCode } = this.state;
    const { history } = this.props;
    const uname = `${MOBILE_NUMBER_PREFIX}${mobileNum}`;
    this.setState({ forgotPasswordError: null, actionInProgress: true });
    try {
      await Auth.forgotPasswordSubmit(uname, verificationCode, newPassword);
      this.props.showNotification({
        message:
          'Your password has been changed successfully. Please try logging in using your new password.',
        severity: NOTIFICATION_SEVERITY.SUCCESS,
      });
      history.push(APP_ROUTES.AUTH_SIGNIN);
    } catch (err) {
      console.log();
      console.error('Error setting new password: ', err);
      let errorToDisplay =
        'Unknown error during forgot password flow, try again';
      if (err.code === 'CodeMismatchException') {
        errorToDisplay = err.message;
      } else if (err.code === 'InvalidParameterException') {
        errorToDisplay = 'Invalid Password';
      }
      this.setState({
        forgotPasswordError: errorToDisplay,
        actionInProgress: false,
      });
    }
  };

  render() {
    const {
      mobileNum,
      newPassword,
      repeatPassword,
      forgotPasswordError,
      setNewPassword,
      actionInProgress,
      verificationCode,
    } = this.state;
    const { classes } = this.props;
    return setNewPassword ? (
      <ValidatorForm onSubmit={this.handleNewPassword}>
        <Grid container direction="column" spacing={2} justify="space-evenly">
          <Grid item>
            <TextValidator
              label="Verification Code"
              name="verification-code"
              type="password"
              value={verificationCode}
              onChange={this.handleValueChange('verificationCode')}
              validators={['required']}
              errorMessages={['Verification code is required']}
              autoFocus
              disabled={actionInProgress}
              variant="outlined"
              fullWidth
            />
          </Grid>
          <Grid item>
            <TextValidator
              label="New Password"
              name="new-password"
              type="password"
              value={newPassword}
              onChange={this.handleValueChange('newPassword')}
              validators={['required']}
              errorMessages={['password is required']}
              disabled={actionInProgress}
              variant="outlined"
              fullWidth
            />
          </Grid>
          <Grid item>
            <TextValidator
              label="Confirm Password"
              name="repeat-password"
              type="password"
              value={repeatPassword}
              onChange={this.handleValueChange('repeatPassword')}
              validators={['required', 'isPasswordMatch']}
              errorMessages={[
                'password is required',
                'passwords did not match',
              ]}
              disabled={actionInProgress}
              variant="outlined"
              fullWidth
            />
          </Grid>
          <Grid item>
            <Typography variant="caption" color="primary">
              Password must contain atleast one Capital letter, one numeric, one
              special characters. For eg. Abcdef@123
            </Typography>
          </Grid>
          {forgotPasswordError && (
            <Grid item>
              <Chip
                label={forgotPasswordError}
                variant="outlined"
                color="primary"
                className={classes.errorChip}
              />
            </Grid>
          )}
          <Grid item>
            {actionInProgress ? (
              <>
                <Typography variant="caption" color="primary">
                  Setting new password
                </Typography>
                <LinearProgress variant="indeterminate" color="primary" />
              </>
            ) : (
              <Button
                variant="contained"
                color="primary"
                className={'fitContentWidth'}
                type="submit"
                disabled={actionInProgress}
              >
                Submit
              </Button>
            )}
          </Grid>
        </Grid>
      </ValidatorForm>
    ) : (
      <ValidatorForm onSubmit={this.handleForgotPassword}>
        <Grid container direction="column" spacing={2} justify="space-evenly">
          <Grid item>
            <Typography variant="h5">Forgot Password?</Typography>
            <Typography>
              Enter your registered mobile number to continue
            </Typography>
          </Grid>
          <Grid item>
            <TextValidator
              label="Mobile number"
              name="username"
              value={mobileNum}
              onChange={this.handleValueChange('mobileNum')}
              validators={['required', 'isPhoneNumber']}
              errorMessages={[
                'Mobile number is required',
                'Mobile number is not valid',
              ]}
              autoFocus
              disabled={actionInProgress}
              variant="outlined"
              fullWidth
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    {MOBILE_NUMBER_PREFIX}
                  </InputAdornment>
                ),
              }}
            />
          </Grid>
          {forgotPasswordError && (
            <Grid item>
              <Chip
                label={forgotPasswordError}
                variant="outlined"
                color="primary"
                className={classes.errorChip}
              />
            </Grid>
          )}
          <Grid item>
            {actionInProgress ? (
              <>
                <Typography variant="caption" color="primary">
                  Starting forgot password flow
                </Typography>
                <LinearProgress variant="indeterminate" color="primary" />
              </>
            ) : (
              <Button
                variant="contained"
                color="primary"
                className={'fitContentWidth'}
                type="submit"
                disabled={actionInProgress}
              >
                Submit
              </Button>
            )}
          </Grid>
        </Grid>
      </ValidatorForm>
    );
  }
}

ForgotPassword.propTypes = {
  classes: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  showNotification: PropTypes.func.isRequired,
};

ForgotPassword = compose(
  withStyles(styles, { withTheme: true }),
  withRouter,
  connect(null, {
    showNotification,
  })
)(ForgotPassword);

export { ForgotPassword };
