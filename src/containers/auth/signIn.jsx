import React from 'react';
import PropTypes from 'prop-types';
import {
  Typography,
  Button,
  FormControlLabel,
  FormGroup,
  Grid,
  Chip,
  LinearProgress,
  Link,
  InputAdornment,
  Switch,
} from '@material-ui/core';
import { compose } from 'redux';
import { withStyles } from '@material-ui/styles';
import { withRouter } from 'react-router-dom';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import { Auth } from 'aws-amplify';
import { connect } from 'react-redux';

import {
  setUserSignedIn,
  setPostSigninRedirectPath,
  preloadDataFromBackend,
} from 'Actions/authUser';
import { showProgressBar, hideProgressBar } from 'Actions/appShell';
import {
  APP_ROUTES,
  MOBILE_NUMBER_PREFIX,
  validatePhoneNumber,
} from 'Utils/appShell';
import { AUTH_STATES } from 'Utils/constants';

const styles = (theme) => ({
  errorChip: {
    borderRadius: 4,
    color: theme.palette.error.main,
    borderColor: theme.palette.error.light,
  },
  cursorPointer: {
    cursor: 'pointer',
  },
});

class SignInForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mobileNum: '',
      emailId: '',
      loginWithEmail: false,
      upass: '',
      signInError: null,
      forceSetNewPassword: false,
      cognitoUser: null,
      newPassword: '',
      repeatPassword: '',
      actionInProgress: false,
    };
    this.handleValueChange = this.handleValueChange.bind(this);
    this.handleSignIn = this.handleSignIn.bind(this);
    this.handleNewPassword = this.handleNewPassword.bind(this);
    this.redirectSignedInUser = this.redirectSignedInUser.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    //custom rule for password matching in (re)set password form
    ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
      if (value !== this.state.newPassword) {
        return false;
      }
      return true;
    });
    //custom rule to validate username (should be a valid phone number)
    ValidatorForm.addValidationRule('isPhoneNumber', (value) =>
      validatePhoneNumber(value)
    );
  }

  componentWillUnmount() {
    // remove custom rules from validator
    ValidatorForm.removeValidationRule('isPasswordMatch');
    ValidatorForm.removeValidationRule('isPhoneNumber');
  }

  handleChange = (event) => {
    this.setState({ loginWithEmail: event.target.checked });
  };

  handleValueChange = (fieldName) => (event) => {
    let value = event.target.value;
    if (fieldName === 'mobileNum' && value.startsWith(MOBILE_NUMBER_PREFIX))
      value = value.replace(MOBILE_NUMBER_PREFIX, '');
    this.setState({
      [fieldName]: value,
    });
  };

  redirectSignedInUser = () => {
    const { setUserSignedIn, history } = this.props;
    setUserSignedIn();
    history.push(APP_ROUTES.AUTH_SIGNIN_LANDING);
  };

  handleSignIn = async (event) => {
    const { mobileNum, emailId, upass, loginWithEmail } = this.state;
    let uname = '';
    if (loginWithEmail) {
      uname = emailId;
    } else {
      uname = `${MOBILE_NUMBER_PREFIX}${mobileNum}`;
    }
    this.setState({ signInError: null, actionInProgress: true });
    try {
      const user = await Auth.signIn(uname, upass);
      if (
        user.challengeName &&
        user.challengeName === 'NEW_PASSWORD_REQUIRED'
      ) {
        // user need to set a password to continue
        this.setState({
          forceSetNewPassword: true,
          signInError: 'Please set a new password to continue',
          cognitoUser: user,
          actionInProgress: false,
        });
      } else {
        // user signed in successfully
        this.redirectSignedInUser();
      }
    } catch (err) {
      console.error('Error during signin: ', err);
      let errorToDisplay = 'Unknown error during sign in, try again';
      if (err.code === 'UserNotConfirmedException') {
        //
      } else if (err.code === 'PasswordResetRequiredException') {
        //
      } else if (
        ['NotAuthorizedException', 'UserNotFoundException'].includes(err.code)
      ) {
        errorToDisplay = err.message;
      } else {
        //FIXME: handle using error boundaries
        console.error('Unknown error during sign in: ', err);
      }

      this.setState({ signInError: errorToDisplay, actionInProgress: false });
    }
  };
  handleNewPassword = async (event) => {
    const { cognitoUser, newPassword } = this.state;
    this.setState({ signInError: null, actionInProgress: true });
    try {
      await Auth.completeNewPassword(cognitoUser, newPassword);
      this.redirectSignedInUser();
    } catch (err) {
      console.error('Error setting new password: ', err);
      this.setState({
        signInError: 'Something went wrong, please try again',
        actionInProgress: false,
      });
    }
  };

  render() {
    const {
      mobileNum,
      emailId,
      upass,
      newPassword,
      repeatPassword,
      signInError,
      forceSetNewPassword,
      actionInProgress,
      loginWithEmail,
    } = this.state;
    const { classes, history } = this.props;
    const triggered = { loginWithEmail };
    let comp;
    if (triggered.loginWithEmail) {
      comp = (
        <Grid item>
          <TextValidator
            label="Email ID"
            name="username"
            value={emailId}
            onChange={this.handleValueChange('emailId')}
            validators={['required', 'isEmail']}
            errorMessages={['Email ID is required', 'Email ID is not valid']}
            autoFocus
            disabled={actionInProgress}
            variant="outlined"
            fullWidth
          />
        </Grid>
      );
    } else {
      comp = (
        <Grid item>
          <TextValidator
            label="Mobile number"
            name="username"
            value={mobileNum}
            onChange={this.handleValueChange('mobileNum')}
            validators={['required', 'isPhoneNumber']}
            errorMessages={[
              'Mobile number is required',
              'Mobile number is not valid',
            ]}
            autoFocus
            disabled={actionInProgress}
            variant="outlined"
            fullWidth
            InputProps={{
              startAdornment: (
                <InputAdornment>{MOBILE_NUMBER_PREFIX}</InputAdornment>
              ),
            }}
          />
        </Grid>
      );
    }
    return forceSetNewPassword ? (
      <ValidatorForm onSubmit={this.handleNewPassword}>
        <Grid container direction="column" spacing={2} justify="space-evenly">
          <Grid item>
            <Typography variant="h5">Set your password</Typography>
            <Typography>to continue</Typography>
          </Grid>
          <Grid item>
            <TextValidator
              label="New Password"
              name="new-password"
              type="password"
              value={newPassword}
              onChange={this.handleValueChange('newPassword')}
              validators={['required']}
              errorMessages={['password is required']}
              autoFocus
              disabled={actionInProgress}
              variant="outlined"
              fullWidth
            />
          </Grid>
          <Grid item>
            <TextValidator
              label="Confirm Password"
              name="repeat-password"
              type="password"
              value={repeatPassword}
              onChange={this.handleValueChange('repeatPassword')}
              validators={['required', 'isPasswordMatch']}
              errorMessages={[
                'password is required',
                'passwords did not match',
              ]}
              disabled={actionInProgress}
              variant="outlined"
              fullWidth
            />
          </Grid>
          <Grid item>
            <Typography variant="caption" color="primary">
              Password must contain a combination of uppercase letters,
              lowercase letters and numbers. For eg. Abcdef123
            </Typography>
          </Grid>
          {signInError && (
            <Grid item>
              <Chip
                label={signInError}
                variant="outlined"
                className={classes.errorChip}
              />
            </Grid>
          )}
          <Grid item>
            {actionInProgress ? (
              <>
                <Typography variant="caption" color="primary">
                  Setting new password
                </Typography>
                <LinearProgress variant="indeterminate" color="primary" />
              </>
            ) : (
              <Button
                variant="contained"
                color="primary"
                className={'fitContentWidth'}
                type="submit"
                disabled={actionInProgress}
              >
                Submit
              </Button>
            )}
          </Grid>
        </Grid>
      </ValidatorForm>
    ) : (
      <ValidatorForm onSubmit={this.handleSignIn}>
        <Grid container direction="column" spacing={2} justify="space-evenly">
          <Grid item>
            <Typography variant="h5">Sign In</Typography>
            <Typography>to continue</Typography>
          </Grid>
          <Grid item>
            <FormGroup>
              <Typography>Choose Mobile number or Email ID to login</Typography>
            </FormGroup>
            <FormGroup>
              <Typography>
                Mobile Number&ensp;
                <FormControlLabel
                  control={
                    <Switch
                      checked={this.state.loginWithEmail}
                      name="Mobile number"
                      onChange={this.handleChange}
                    />
                  }
                  label="Email ID"
                  labelPlacement="end"
                />
              </Typography>
            </FormGroup>
          </Grid>
          {comp}
          <Grid item>
            <TextValidator
              label="Password"
              name="password"
              type="password"
              value={upass}
              onChange={this.handleValueChange('upass')}
              validators={['required']}
              errorMessages={['password is required']}
              disabled={actionInProgress}
              variant="outlined"
              fullWidth
            />
          </Grid>
          {signInError && (
            <Grid item>
              <Chip
                label={signInError}
                variant="outlined"
                color="primary"
                className={classes.errorChip}
              />
            </Grid>
          )}
          <Grid item>
            {actionInProgress ? (
              <>
                <Typography variant="caption" color="primary">
                  Signing you in
                </Typography>
                <LinearProgress variant="indeterminate" color="primary" />
              </>
            ) : (
              <Button
                variant="contained"
                color="primary"
                className={'fitContentWidth'}
                type="submit"
                disabled={actionInProgress}
              >
                Sign In
              </Button>
            )}
          </Grid>
          <Grid item>
            <Link
              component="button"
              variant="body2"
              className={classes.cursorPointer}
              onClick={() => history.push(APP_ROUTES.AUTH_FORGOT_PASSWORD)}
            >
              Forgot Password
            </Link>
          </Grid>
        </Grid>
      </ValidatorForm>
    );
  }
}

SignInForm.propTypes = {
  classes: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  setUserSignedIn: PropTypes.func.isRequired,
  setPostSigninRedirectPath: PropTypes.func.isRequired,
  redirectPathPostSignIn: PropTypes.string.isRequired,
};

const mapStateToProps = (state) => ({
  redirectPathPostSignIn: state.authUser.redirectPathPostSignIn,
  userAuthState: state.authUser.userAuthState,
});

SignInForm = compose(
  withStyles(styles, { withTheme: true }),
  withRouter,
  connect(mapStateToProps, {
    setUserSignedIn,
    setPostSigninRedirectPath,
  })
)(SignInForm);

class SignInLanding extends React.Component {
  constructor(props) {
    super(props);
    this.state = { authFailed: false };
  }

  async componentDidMount() {
    const {
      userAuthState,
      redirectPathPostSignIn,
      setPostSigninRedirectPath,
      preloadDataFromBackend,
      showProgressBar,
      hideProgressBar,
      history,
    } = this.props;

    if (userAuthState === AUTH_STATES.SIGNED_IN) {
      showProgressBar();
      // handle redirection from sign-in form
      const { attributes } = await Auth.currentAuthenticatedUser();
      await preloadDataFromBackend(attributes.phone_number);

      // reset redirect path
      setPostSigninRedirectPath();
      // redirect to target path
      history.push(redirectPathPostSignIn);
      hideProgressBar();
    }
  }

  render() {
    const { classes } = this.props;
    const { authFailed } = this.state;

    return authFailed ? (
      <Typography variant="subtitle1">
        You are not signed in.&nbsp;Click here to&nbsp;
        <Link
          component="button"
          variant="body2"
          className={classes.cursorPointer}
          onClick={() => history.push(APP_ROUTES.AUTH_SIGNIN)}
        >
          Sign In
        </Link>
        &nbsp;again.
      </Typography>
    ) : (
      <>
        <Typography variant="subtitle1">
          You are now signed in. Redirecting to the portal in a moment....
        </Typography>
        <LinearProgress variant="query" color="secondary" />
      </>
    );
  }
}

SignInLanding.propTypes = {
  classes: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  setPostSigninRedirectPath: PropTypes.func.isRequired,
  setUserSignedIn: PropTypes.func.isRequired,
  preloadDataFromBackend: PropTypes.func.isRequired,
  redirectPathPostSignIn: PropTypes.string.isRequired,
  showProgressBar: PropTypes.func.isRequired,
  hideProgressBar: PropTypes.func.isRequired,
  userAuthState: PropTypes.string.isRequired,
};

SignInLanding = compose(
  withStyles(styles, { withTheme: true }),
  withRouter,
  connect(mapStateToProps, {
    setPostSigninRedirectPath,
    setUserSignedIn,
    preloadDataFromBackend,
    showProgressBar,
    hideProgressBar,
  })
)(SignInLanding);

export { SignInForm, SignInLanding };
