import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';
import { Link, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/styles';

import { APP_ROUTES } from 'Utils/appShell';

const styles = (theme) => ({
  cursorPointer: {
    cursor: 'pointer',
  },
});
class SignOutLanding extends React.Component {
  render() {
    // FIXME: add a check on the user auth state and redirect accordingly
    const { classes, history } = this.props;
    return (
      <Typography variant="subtitle1">
        You are now signed out.&nbsp;
        <Link
          component="button"
          variant="body2"
          className={classes.cursorPointer}
          onClick={() => history.push(APP_ROUTES.AUTH_SIGNIN)}
        >
          Sign In here.
        </Link>
      </Typography>
    );
  }
}

SignOutLanding.propTypes = {
  classes: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

SignOutLanding = compose(
  withStyles(styles, { withTheme: true }),
  withRouter
)(SignOutLanding);

export { SignOutLanding };
