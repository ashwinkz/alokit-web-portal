import { APP_SHELL_ACTIONS } from 'Actions/index';

const initialState = {
  showProgress: false,
  showNotification: false,
  notification: {
    message: '',
    severity: 'info',
  },
};

const {
  SET_SHOW_PROGRESS,
  SET_HIDE_PROGRESS,
  SET_SHOW_NOTIFICATION,
  SET_HIDE_NOTIFICATION,
} = APP_SHELL_ACTIONS;

const appShell = (state = initialState, action) => {
  switch (action.type) {
    case SET_SHOW_PROGRESS:
      return {
        ...state,
        showProgress: true,
      };
    case SET_HIDE_PROGRESS:
      return {
        ...state,
        showProgress: false,
      };
    case SET_SHOW_NOTIFICATION:
      return {
        ...state,
        showNotification: true,
        notification: {
          message: action.payload.message,
          severity: action.payload.severity,
        },
      };
    case SET_HIDE_NOTIFICATION:
      return {
        ...state,
        showNotification: false,
      };
    default:
      return state;
  }
};

export default appShell;
