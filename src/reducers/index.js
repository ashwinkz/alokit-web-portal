import { combineReducers } from 'redux';
import appShell from 'Reducers/appShell';
import authUser from 'Reducers/authUser';

const reducersList = combineReducers({ appShell, authUser });
export default reducersList;
