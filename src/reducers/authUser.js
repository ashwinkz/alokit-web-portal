import { AUTH_USER_ACTIONS } from 'Actions/index';
import { APP_ROUTES } from 'Utils/appShell';
import { AUTH_STATES } from 'Utils/constants';

const blankUserProfile = {
  username: null,
  firstName: null,
  lastName: null,
  userRole: null,
  school: {
    id: null,
    name: null,
    branch: null,
  },
  userGradeSectionList: [],
};

const initialState = {
  userAuthState: AUTH_STATES.SIGNED_OUT,
  redirectPathPostSignIn: APP_ROUTES.DASHBOARD_PAGE,
  userProfile: blankUserProfile,
  userProfileLoaded: false,
  ALOKIT_USER_ROLE_LIST: [],
  SCHOOL_USER_ROLE_LIST: [],
  GENDER_LIST: [],
  GRADE_LIST: [],
  SECTION_LIST: [],
  SUBJECT_LIST: [],
  SUMMARY_SCORE_LIST: [],
  dashboardReportsLoaded: false,
  dashboardReports: {},
};

const {
  SET_REDIRECT_PATH,
  SET_AUTH_STATE,
  SET_LOGGED_IN_USER_DETAILS,
  CLEAR_LOGGED_IN_USER_DETAILS,
  SET_ENUM_VALUES,
  SET_DASHBOARD_REPORTS_LOADED,
  SET_DASHBOARD_REPORT,
} = AUTH_USER_ACTIONS;

const authUser = (state = initialState, action) => {
  switch (action.type) {
    case SET_REDIRECT_PATH:
      const redirectPath =
        action.payload.redirectPath || initialState.redirectPathPostSignIn;
      return {
        ...state,
        redirectPathPostSignIn: redirectPath,
      };
    case SET_AUTH_STATE:
      return {
        ...state,
        userAuthState: action.payload.authState,
      };
    case SET_LOGGED_IN_USER_DETAILS:
      return {
        ...state,
        userProfile: {
          username: action.payload.username,
          firstName: action.payload.firstName,
          lastName: action.payload.lastName,
          userRole: action.payload.userRole,
          school: {
            id: action.payload.schoolID,
            name: action.payload.schoolName,
            branch: action.payload.schoolBranch,
          },
          userGradeSectionList: action.payload.userGradeSectionList,
        },
        userProfileLoaded: true,
      };
    case CLEAR_LOGGED_IN_USER_DETAILS:
      return {
        ...state,
        redirectPathPostSignIn: APP_ROUTES.DASHBOARD_PAGE,
        userProfile: blankUserProfile,
        userProfileLoaded: false,
        dashboardReportsLoaded: false,
        dashboardReports: {},
      };
    case SET_ENUM_VALUES:
      return {
        ...state,
        ALOKIT_USER_ROLE_LIST: action.payload.alokitUserRoleList,
        SCHOOL_USER_ROLE_LIST: action.payload.schoolUserRoleList,
        GENDER_LIST: action.payload.genderList,
        GRADE_LIST: action.payload.gradeList,
        SECTION_LIST: action.payload.sectionList,
        SUBJECT_LIST: action.payload.subjectList,
        SUMMARY_SCORE_LIST: action.payload.summaryScoreList,
      };
    case SET_DASHBOARD_REPORTS_LOADED:
      return {
        ...state,
        dashboardReportsLoaded: action.payload.reportsLoadedFlag,
      };
    case SET_DASHBOARD_REPORT:
      return {
        ...state,
        dashboardReports: {
          ...state.dashboardReports,
          [action.payload.schoolId]: action.payload.reportData,
        },
      };
    default:
      return state;
  }
};

export default authUser;
