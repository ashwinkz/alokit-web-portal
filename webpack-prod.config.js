const path = require('path');
const Dotenv = require('dotenv-webpack');
const { merge } = require('webpack-merge');
const commonConfig = require('./webpack-base.config.js');

const prodConfig = {
  mode: 'production',
  module: {
    rules: [
      {
        test: /\.(pdf|jpg|jpeg|png|gif|svg|ico|mp3)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[path][name]-[hash:8].[ext]',
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new Dotenv({
      path: path.resolve(__dirname, '.env'),
    }),
  ],
};

module.exports = commonConfig.map((conf) => merge(conf, prodConfig));
