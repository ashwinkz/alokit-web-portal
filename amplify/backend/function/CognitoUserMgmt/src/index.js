/* Amplify Params - DO NOT EDIT
	AUTH_ALOKITWEBAUTH_USERPOOLID
	ENV
	REGION
Amplify Params - DO NOT EDIT */

const { CognitoIdentityServiceProvider } = require('aws-sdk');
const cognitoIdentityServiceProvider = new CognitoIdentityServiceProvider();

const COGNITO_SMS_DELIVERY = 'SMS';
const COGNITO_EMAIL_DELIVERY = 'EMAIL';

const DISABLE_USER_ACTION = 'DISABLE';
const ENABLE_USER_ACTION = 'ENABLE';

const getDeliveryMediumsList = (sendEmail, sendSMS) => {
  let dmList = [];
  if (sendSMS) {
    dmList.push(COGNITO_SMS_DELIVERY);
  }
  if (sendEmail || !dmList.length) {
    dmList.push(COGNITO_EMAIL_DELIVERY);
  }
  return dmList;
};

/**
 * Get user pool information from environment variables.
 */
const COGNITO_USERPOOL_ID = process.env.AUTH_ALOKITWEBAUTH_USERPOOLID;
if (!COGNITO_USERPOOL_ID) {
  throw new Error(
    `Function requires environment variable: 'AUTH_ALOKITWEBAUTH_USERPOOLID'`
  );
}

/**
 * Using this as the entry point, you can use a single function to handle many resolvers.
 */
const resolvers = {
  Mutation: {
    createUserLogin: async (ctx) => {
      //check if user already exists with the given phone number or email
      let params = {
        UserPoolId: COGNITO_USERPOOL_ID,
        AttributesToGet: [],
        Limit: 1,
      };

      try {
        params['Filter'] = `phone_number = \"${ctx.arguments.phoneNumber}\"`;
        const usersByPhoneNumber = await cognitoIdentityServiceProvider
          .listUsers(params)
          .promise();
        params['Filter'] = `email = \"${ctx.arguments.email}\"`;
        const usersByEmail = await cognitoIdentityServiceProvider
          .listUsers(params)
          .promise();

        if (
          usersByPhoneNumber.Users.length > 0 ||
          usersByEmail.Users.length > 0
        ) {
          return {
            created: false,
            message:
              'Another user is already registered with this email/phone number',
          };
        }
      } catch (e) {
        console.log(
          'Failed searching for existing users by email/phone number',
          e
        );
        return {
          created: false,
          message:
            'Failed checking if email or phone number is already registered',
        };
      }

      //proceed to add user to the cognito pool
      const deliveryMediums = getDeliveryMediumsList(
        ctx.arguments.sendEmail,
        ctx.arguments.sendSMS
      );
      params = {
        UserPoolId: COGNITO_USERPOOL_ID,
        Username: ctx.arguments.phoneNumber,
        DesiredDeliveryMediums: deliveryMediums,
        UserAttributes: [
          {
            Name: 'given_name',
            Value: ctx.arguments.firstName,
          },
          {
            Name: 'family_name',
            Value: ctx.arguments.lastName,
          },
          {
            Name: 'email',
            Value: ctx.arguments.email,
          },
          {
            Name: 'phone_number',
            Value: ctx.arguments.phoneNumber,
          },
          {
            Name: 'email_verified',
            Value: 'true',
          },
          {
            Name: 'phone_number_verified',
            Value: 'true',
          },
        ],
      };
      try {
        const createdUser = await cognitoIdentityServiceProvider
          .adminCreateUser(params)
          .promise();
        console.log('User creation succeeded in Cognito');
        return {
          created: true,
          message: `Login credentials sent via ${deliveryMediums.join(' & ')}`,
        };
      } catch (e) {
        console.log(
          'Failed creating user in Cognito for given input',
          params,
          e
        );
        return {
          created: false,
          message: 'Failed creating user login for given input',
        };
      }
    },
    modifyUserAccess: async (ctx) => {
      if (
        ![ENABLE_USER_ACTION, DISABLE_USER_ACTION].includes(
          ctx.arguments.action
        )
      )
        throw new Error('Invalid value provided for "action" parameter');

      if (!ctx.arguments.username)
        throw new Error('Invalid value provided for "username" parameter');

      let params = {
        UserPoolId: COGNITO_USERPOOL_ID,
        Username: ctx.arguments.username,
      };
      try {
        if (ctx.arguments.action === ENABLE_USER_ACTION) {
          await cognitoIdentityServiceProvider
            .adminEnableUser(params)
            .promise();
          console.log('User successfully enabled in Cognito');
        } else if (ctx.arguments.action === DISABLE_USER_ACTION) {
          await cognitoIdentityServiceProvider
            .adminDisableUser(params)
            .promise();
          await cognitoIdentityServiceProvider
            .adminUserGlobalSignOut(params)
            .promise();
          console.log('User successfully disabled in Cognito');
        }

        return {
          modified: true,
          message: 'User access modified successfully',
        };
      } catch (e) {
        console.log('Failed modifying user access for params: ', params, e);
        return {
          created: false,
          message: 'Failed to modify user access',
        };
      }
    },
  },
};

// event
// {
//   "typeName": "Query", /* Filled dynamically based on @function usage location */
//   "fieldName": "me", /* Filled dynamically based on @function usage location */
//   "arguments": { /* GraphQL field arguments via $ctx.arguments */ },
//   "identity": { /* AppSync identity object via $ctx.identity */ },
//   "source": { /* The object returned by the parent resolver. E.G. if resolving field 'Post.comments', the source is the Post object. */ },
//   "request": { /* AppSync request object. Contains things like headers. */ },
//   "prev": { /* If using the built-in pipeline resolver support, this contains the object returned by the previous function. */ },
// }
exports.handler = async (event) => {
  const typeHandler = resolvers[event.typeName];
  if (typeHandler) {
    const resolver = typeHandler[event.fieldName];
    if (resolver) {
      return await resolver(event);
    }
  }
  throw new Error('Resolver not found.');
};
